LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_JAVA_LIBRARIES := bouncycastle core-oj telephony-common ims-common
LOCAL_STATIC_JAVA_LIBRARIES := \
    android-support-v4 \
    android-support-v13 \
    android-support-v7-recyclerview \
    android-support-v7-preference \
    android-support-v7-appcompat \
    android-support-v14-preference \
    jsr305

LOCAL_MODULE_TAGS := optional

src_dirs := src
res_dirs := res

ifneq ($(strip $(wildcard $(LOCAL_PATH)/../../../vendor/sprd/platform/packages/apps/AudioProfile)),)
audioprofile_dir := ./../../../vendor/sprd/platform/packages/apps/AudioProfile

src_dirs += \
    $(audioprofile_dir)/src \

res_dirs += \
    $(audioprofile_dir)/res \

LOCAL_AAPT_FLAGS := \
    --auto-add-overlay \
    --extra-packages com.sprd.audioprofile \

endif

LOCAL_SRC_FILES := \
        $(call all-java-files-under, $(src_dirs)) \
        src/com/android/settings/EventLogTags.logtags

LOCAL_RESOURCE_DIR := $(addprefix $(LOCAL_PATH)/, $(res_dirs)) \
    frameworks/support/v7/preference/res \
    frameworks/support/v14/preference/res \
    frameworks/support/v7/appcompat/res \
    frameworks/support/v7/recyclerview/res

LOCAL_PACKAGE_NAME := Settings
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

# Force building PowerGuru UI
# For normalize, put HeartbeatSyncSettings into PRODUCT_PACKAGES board config
# if current product needed
LOCAL_REQUIRED_MODULES += HeartbeatSyncSettings


LOCAL_PROGUARD_FLAG_FILES := proguard.flags

LOCAL_AAPT_FLAGS += --auto-add-overlay \
    --extra-packages android.support.v7.preference:android.support.v14.preference:android.support.v17.preference:android.support.v7.appcompat:android.support.v7.recyclerview

ifneq ($(INCREMENTAL_BUILDS),)
    LOCAL_PROGUARD_ENABLED := disabled
    LOCAL_JACK_ENABLED := incremental
    LOCAL_DX_FLAGS := --multi-dex
    LOCAL_JACK_FLAGS := --multi-dex native
endif

include frameworks/opt/setupwizard/library/common-full-support.mk
include frameworks/base/packages/SettingsLib/common.mk

include $(BUILD_PACKAGE)

# Use the following include to make our test apk.
ifeq (,$(ONE_SHOT_MAKEFILE))
include $(call all-makefiles-under,$(LOCAL_PATH))
endif
