package com.sprd.settings;

import android.content.Context;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.settings.R;
import com.android.settings.sim.SimDialogActivity;

import android.app.AddonManager;

public class SettingsOperatorHelper {
    private static final String TAG = "SettingsOperatorHelper";
    static SettingsOperatorHelper sInstance;

    public static SettingsOperatorHelper getInstance(Context context) {
        if (sInstance != null) {
            return sInstance;
        }
        AddonManager addonManager = new AddonManager(context);
        sInstance = (SettingsOperatorHelper) addonManager.getAddon(
                R.string.feature_settings_operator_plugin_feature, SettingsOperatorHelper.class);
        Log.i(TAG, "getInstance [" + sInstance + "]");
        return sInstance;
    }

    public SettingsOperatorHelper() {
    }

    public boolean isNeedSetDataEnable(Context context) {return false;}

    public boolean removeDataSwitch() {return false;}

    public boolean isSimSlotForPrimaryCard(SubscriptionInfo subInfoRecord){
        int defaultDataSubId = SubscriptionManager.getDefaultDataSubscriptionId();
        return defaultDataSubId == subInfoRecord.getSubscriptionId();
    }
}
