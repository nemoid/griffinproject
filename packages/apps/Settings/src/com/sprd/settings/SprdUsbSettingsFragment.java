/** Created by Spreadst */
package com.sprd.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.UserManager;
import android.preference.PreferenceFragment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.provider.Settings;
import android.util.Log;
/*import android.support.v7.preference.PreferenceScreen;*/

import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.Utils;

public class SprdUsbSettingsFragment extends SettingsPreferenceFragment
        implements RadioButtonPreference.OnClickListener {

    private final String LOG_TAG = "SprdUsbSettings";
    private final static boolean DBG = true;

    private static final String KEY_CHARGE_ONLY = "usb_charge_only";
    private static final String KEY_MTP = "usb_mtp";
    private static final String KEY_PTP = "usb_ptp";
    private static final String KEY_CDROM = "usb_virtual_drive";
    private static final String KEY_MIDI = "usb_midi";
    private static final String KEY_UMS = "usb_storage";

    private RadioButtonPreference mUsbChargeOnly;
    private RadioButtonPreference mMtp;
    private RadioButtonPreference mPtp;
    private RadioButtonPreference mMidi;
    private RadioButtonPreference mCdrom;
    private RadioButtonPreference mUms;

    private UsbManager mUsbManager = null;
    private BroadcastReceiver mPowerDisconnectReceiver = null;

    private final static boolean SUPPORT_UMS = SystemProperties.getBoolean("persist.sys.usb.support_ums", false);

    private boolean SUPPORT_CTA = SystemProperties.getBoolean("ro.usb.support_cta", false);
    private boolean mIsUnlocked = false;

    @SuppressWarnings("deprecation")
    public void onActivityCreated(Bundle savedInstanceState) {
        addPreferencesFromResource(R.xml.sprd_usb_settings);
        Intent i = getContext().registerReceiver(null, new IntentFilter(UsbManager.ACTION_USB_STATE));
        mIsUnlocked = i.getBooleanExtra(UsbManager.USB_DATA_UNLOCKED, false);

        if (DBG) Log.d(LOG_TAG, "on Create");

        mUsbChargeOnly = (RadioButtonPreference) findPreference(KEY_CHARGE_ONLY);
        mMtp = (RadioButtonPreference) findPreference(KEY_MTP);
        mPtp = (RadioButtonPreference) findPreference(KEY_PTP);
        mCdrom = (RadioButtonPreference) findPreference(KEY_CDROM);
        mMidi = (RadioButtonPreference) findPreference(KEY_MIDI);
        mUms = (RadioButtonPreference) findPreference(KEY_UMS);

        mUsbChargeOnly.setOnClickListener(this);
        mMtp.setOnClickListener(this);
        mPtp.setOnClickListener(this);
        mCdrom.setOnClickListener(this);
        mMidi.setOnClickListener(this);
        mUms.setOnClickListener(this);
        if (!SUPPORT_UMS) {
            getPreferenceScreen().removePreference(mUms);
        }
        mUsbManager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);

        boolean isFileTransferRestricted = ((UserManager) getActivity().getSystemService(Context.USER_SERVICE))
                .hasUserRestriction(UserManager.DISALLOW_USB_FILE_TRANSFER);
        if (isFileTransferRestricted) {
            getPreferenceScreen().removePreference(mMtp);
            getPreferenceScreen().removePreference(mPtp);
        }

        mPowerDisconnectReceiver = new PowerDisconnectReceiver();
        getActivity().registerReceiver(mPowerDisconnectReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (DBG) Log.d(LOG_TAG, "on onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DBG) Log.d(LOG_TAG, "on Resume");
        updateUI();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mPowerDisconnectReceiver);
    }

    @Override
    public void onRadioButtonClicked(RadioButtonPreference preference) {

        Log.d(LOG_TAG ,"onRadioButtonClicked");
        if (Utils.isMonkeyRunning()) {
            return;
        }

        if (preference == mUsbChargeOnly) {
            if (mUsbChargeOnly.isChecked()) return;
            if (SUPPORT_CTA) {
                mUsbManager.setCurrentFunction(UsbManager.USB_FUNCTION_NONE);
            } else {
                //SPRD: Bug#594225 Set usb function error led to usb debug notification disappear-->
                mUsbManager.setCurrentFunction(null);
                //<-- Set usb function error led to usb debug notification disappear
                mUsbManager.setUsbDataUnlocked(false);
                getActivity().finish();
                return;
            }
        } else if (preference == mMtp) {
            if (mMtp.isChecked()) return;
            mUsbManager.setCurrentFunction(UsbManager.USB_FUNCTION_MTP);
        } else if (preference == mPtp) {
            if (mPtp.isChecked()) return;
            mUsbManager.setCurrentFunction(UsbManager.USB_FUNCTION_PTP);
        } else if (preference == mMidi) {
            if (mMidi.isChecked()) return;
            mUsbManager.setCurrentFunction(UsbManager.USB_FUNCTION_MIDI);
        } else if (preference == mCdrom) {
            if (mCdrom.isChecked()) return;
            mUsbManager.setCurrentFunction(UsbManager.USB_FUNCTION_CDROM);
        } else if (preference == mUms) {
            if (mUms.isChecked()) return;
            mUsbManager.setCurrentFunction(UsbManager.USB_FUNCTION_MASS_STORAGE);
        }
        mUsbManager.setUsbDataUnlocked(true);
        getActivity().finish();
        return;
    }

    private void updateUI() {
        String mCurrentfunction = getCurrentFunction();
        Log.i(LOG_TAG, "mCurrentfunction = " + mCurrentfunction);

        if (DBG) Log.d(LOG_TAG, "SUPPORT_CTA" + SUPPORT_CTA + "; mIsUnlocked = " + mIsUnlocked);
        uncheckAllUI();

        if (SUPPORT_CTA) {
            if (UsbManager.USB_FUNCTION_NONE.equals(mCurrentfunction)) {
                mUsbChargeOnly.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_MTP.equals(mCurrentfunction)) {
                mMtp.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_PTP.equals(mCurrentfunction)) {
                mPtp.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_MIDI.equals(mCurrentfunction)) {
                mMidi.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_CDROM.equals(mCurrentfunction)) {
                mCdrom.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_MASS_STORAGE.equals(mCurrentfunction)) {
                mUms.setChecked(true);
            }
        } else {
            if (!mIsUnlocked) {
                mUsbChargeOnly.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_CDROM.equals(mCurrentfunction)) {
                mCdrom.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_MASS_STORAGE.equals(mCurrentfunction)) {
                mUms.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_MTP.equals(mCurrentfunction)) {
                mMtp.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_PTP.equals(mCurrentfunction)) {
                mPtp.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_MIDI.equals(mCurrentfunction)) {
                mMidi.setChecked(true);
            } else if (UsbManager.USB_FUNCTION_NONE.equals(mCurrentfunction)) {
                mUsbChargeOnly.setChecked(true);
            }
        }
    }

    private void uncheckAllUI() {
        mUsbChargeOnly.setChecked(false);
        mMtp.setChecked(false);
        mPtp.setChecked(false);
        mMidi.setChecked(false);
        mCdrom.setChecked(false);
        mUms.setChecked(false);
    }

    public String getCurrentFunction() {
        String functions = SystemProperties.get("sys.usb.config", "");
        if (DBG) Log.d(LOG_TAG, "getCurrentFunction (sys.usb.config) = " + functions);
        int commaIndex = functions.indexOf(',');
        if (commaIndex > 0) {
            return functions.substring(0, commaIndex);
        } else {
            if (functions.equals(UsbManager.USB_FUNCTION_ADB)) {
                return UsbManager.USB_FUNCTION_NONE;
            } else {
                return functions;
            }
        }
    }

    private class PowerDisconnectReceiver extends BroadcastReceiver {
        public void onReceive(Context content, Intent intent) {
            int plugType = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
            if (DBG) Log.d(LOG_TAG, "plugType = " + plugType);
            if (plugType == 0) {
                getActivity().finish();
            }
        }
    }

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return 1;
    }

}
