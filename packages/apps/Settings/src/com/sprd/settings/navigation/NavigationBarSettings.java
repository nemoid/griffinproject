package com.sprd.settings.navigation;

import android.app.ActionBar;
import java.util.ArrayList;
import android.content.Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.os.SystemProperties;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.provider.Settings;

import com.android.settings.AppWidgetLoader.ItemConstructor;
import com.android.settings.R;
import com.android.settings.location.RadioButtonPreference;
import com.android.settings.location.RadioButtonPreference.OnClickListener;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.search.SearchIndexableRaw;
import com.android.settings.search.Indexable.SearchIndexProvider;

public class NavigationBarSettings extends PreferenceActivity 
            implements OnItemClickListener, OnPreferenceChangeListener ,Indexable{
    private int mCurrentSelected;
    private String[] mNavigationBarModes = {"RIGHT", "LEFT","RIGHT_NOTI","LEFT_NOTI"};
    private ListView mNavigationBarModeListView;
    private CustomizedNavAdapter mAdapter;
    private SwitchPreference mHideNotiBar;

    private int[] keyImage = { R.drawable.navigation_bar_style_layout1,
            R.drawable.navigation_bar_style_layout2,
            R.drawable.navigation_bar_style_layout3,
            R.drawable.navigation_bar_style_layout4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar =  getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP, ActionBar.DISPLAY_HOME_AS_UP);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        addPreferencesFromResource(R.xml.hide_navigationbar_preference);
        setContentView(R.layout.navigation_bar_settings);
        mHideNotiBar = (SwitchPreference) findPreference("hide_navigation_bar");
        mHideNotiBar.setOnPreferenceChangeListener(this);
        int lastConfig =  Settings.System.getInt(getContentResolver(), "navigationbar_config", 0);
        mHideNotiBar.setChecked((lastConfig & 0x10) != 0);
        mNavigationBarModeListView = (ListView) findViewById(R.id.softkey_list);
        mNavigationBarModeListView.setOnItemClickListener(this);

        mCurrentSelected = lastConfig & 0x0F;
        mAdapter = new CustomizedNavAdapter(this, getListItems(), keyImage);
        mNavigationBarModeListView.setAdapter(mAdapter);
        setListViewHeightBasedOnChildren(mNavigationBarModeListView);
    }

    private List<Map<String, Object>> getListItems() {
        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        int index = 0;
        for (String t: mNavigationBarModes) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("text", t);
            if (index == mCurrentSelected) {
                map.put("checked", true);
            } else {
                map.put("checked", false);
            }
            index++;
            listItems.add(map);
        }
        return listItems;
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        if (position == mNavigationBarModes.length) {
             return;
        }
        mCurrentSelected = position;
        mAdapter.setListItems(getListItems());
        mAdapter.notifyDataSetChanged();

        int lastConfig = Settings.System.getInt(getContentResolver(), "navigationbar_config", 0);
        Settings.System.putInt(getContentResolver(), "navigationbar_config", (0x10&lastConfig) + mCurrentSelected);
        int newConfig = Settings.System.getInt(getContentResolver(), "navigationbar_config", 0);

        Log.d("NavigationBarSettings", "Style...lastConfig: "+lastConfig + "; newConfig: "+newConfig+"; mCurrentSelected: "+mCurrentSelected);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        // TODO Auto-generated method stub
        if(preference == mHideNotiBar){
            final boolean flag = (Boolean) newValue;
            int lastConfig = Settings.System.getInt(getContentResolver(), "navigationbar_config", 0);
            if(flag){
                Settings.System.putInt(getContentResolver(), "navigationbar_config", 0x10 | lastConfig);
            }else{
                Settings.System.putInt(getContentResolver(), "navigationbar_config", 0x0F & lastConfig);
            }
            int newConfig = Settings.System.getInt(getContentResolver(), "navigationbar_config", 0);
            Log.d("NavigationBarSettings", "lastConfig: "+lastConfig + "; newConfig: "+newConfig);
            return true;
        }else{
            return false;
        }
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
            new SprdNavigationSearchIndexProvider();

    private static class SprdNavigationSearchIndexProvider extends BaseSearchIndexProvider {

        @Override
        public List<SearchIndexableRaw> getRawDataToIndex(Context context, boolean enabled) {
            final List<SearchIndexableRaw> result = new ArrayList<SearchIndexableRaw>();
            if (!"0".equals(android.os.SystemProperties.get("qemu.hw.mainkeys"))
                    || !DynaNavigationBarPluginsUtils.getInstance(context).isSupportDynaNaviBar()) {
                return result;
            }
            SearchIndexableRaw data = new SearchIndexableRaw(context);
            final String screenTitle =  context.getResources().getString(R.string.navigation_bar_title);
            data.title = screenTitle;
            data.screenTitle = screenTitle;
            data.intentAction = "android.intent.action.MAIN";
            data.intentTargetPackage = "com.android.settings";
            data.intentTargetClass = "com.sprd.settings.navigation.NavigationBarSettings";
            result.add(data);
            return result;
        }
    }
}