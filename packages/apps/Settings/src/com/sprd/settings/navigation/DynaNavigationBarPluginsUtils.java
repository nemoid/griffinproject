package com.sprd.settings.navigation;

import android.app.AddonManager;
import android.content.Context;
import com.android.settings.R;

public class DynaNavigationBarPluginsUtils {

    static DynaNavigationBarPluginsUtils mInstance;

    public DynaNavigationBarPluginsUtils() {
    }

    public static DynaNavigationBarPluginsUtils getInstance(Context context) {
        if (mInstance != null)
            return mInstance;
        mInstance = (DynaNavigationBarPluginsUtils) new AddonManager(context).getAddon(
                R.string.feature_settings_for_dyna_navigationbar, DynaNavigationBarPluginsUtils.class);
        return mInstance;
    }

    public boolean isSupportDynaNaviBar() {
        return false;
    }

}
