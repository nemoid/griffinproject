package com.sprd.settings.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.util.Log;
import android.os.Handler;
import android.os.Message;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.database.ContentObserver;

public class WifiSoftApSleepPolicy extends BroadcastReceiver{

    private static final String TAG = "WifiSoftApSleepPolicy";

    public static final int WIFI_SOFT_AP_SLEEP_POLICY_NEVER = 0;
    public static final int WIFI_SOFT_AP_SLEEP_POLICY_5_MINS = 1;
    public static final int WIFI_SOFT_AP_SLEEP_POLICY_10_MINS = 2;
    public static final String WIFI_SOFT_AP_SLEEP_POLICY = "wifi_soft_ap_sleep_policy_key";

    public static final int FIVE_MINS = 300000;
    public static final int TEN_MINS = 600000;

    private static WifiManager mWifiManager = null;
    private static SoftApConnHandler mHandler;
    private static boolean isSoftApRunning = false;
    private static Context mContext = null;

    private static class SoftApConnHandler extends Handler {

        @Override
        public void handleMessage(Message message) {
            Log.e(TAG,"handlemsg:" + message.what + "-" + message);
            mWifiManager.setWifiApEnabled(null, false);
        }

        void cancel() {
            removeMessages(0);
        }

        void start(long when) {
            removeMessages(0);
            sendEmptyMessageDelayed(0, when);
        }

        boolean hasMsg(){
            return hasMessages(0);
        }
    }

    public static ContentObserver mObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.e(TAG,"change policy!");
            if (!isSoftApRunning)
                return;
            Log.e(TAG,"change policy and soft ap running");
            mHandler.cancel();
            onSoftApConnChanged();
        }
    };

    public static ContentObserver getSoftApChangeObserver(){
        return mObserver;
    }

    public static void init(Context context) {
        mWifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        if (mHandler == null) {
            Log.e(TAG,"first");
            mHandler = new SoftApConnHandler();
            context.getContentResolver().registerContentObserver(
                    Settings.System.getUriFor(WIFI_SOFT_AP_SLEEP_POLICY), true, mObserver);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        String action = intent.getAction();
        if (mHandler == null) {
            Log.e(TAG,"receive before init");
            mWifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
            mHandler = new SoftApConnHandler();
            context.getContentResolver().registerContentObserver(
                    Settings.System.getUriFor(WIFI_SOFT_AP_SLEEP_POLICY), true, mObserver);
        }

        if (WifiManager.WIFI_AP_STATE_CHANGED_ACTION.equals(action)) {
            isSoftApRunning = true;
            int hotspotState = intent.getIntExtra(
                        WifiManager.EXTRA_WIFI_AP_STATE,
                        WifiManager.WIFI_AP_STATE_FAILED);
            if (hotspotState == WifiManager.WIFI_AP_STATE_ENABLED) {
                Log.e(TAG,"open");
                checkAndSendMsg();
            } else if (hotspotState == WifiManager.WIFI_AP_STATE_DISABLED) {
                //SPRD: Bug #590722 The switch status of softAp/BluetoothTether shows exception BEG-->
                if (mHandler != null) {
                    mHandler.cancel();
                }
                //<-- The switch status of softAp/BluetoothTether shows exception END
                Log.e(TAG,"close");
                isSoftApRunning = false;
            }
        }else if (WifiManager.WIFI_AP_CONNECTION_CHANGED_ACTION.equals(action) || HotspotSettings.STATIONS_STATE_CHANGED_ACTION.equals(action)) {
            onSoftApConnChanged();
        }
    }

    private static void  onSoftApConnChanged() {
        String mConnectedStationsStr = mWifiManager.softApGetConnectedStations();
        Log.e(TAG, "mConnectedStationsStr = " + mConnectedStationsStr);
        if (mConnectedStationsStr == null || mConnectedStationsStr.length() == 0) {
            Log.e(TAG,"no ap connected");
            if(mHandler.hasMsg())
                return;
            checkAndSendMsg();
        } else {
            mHandler.cancel();
            Log.e(TAG,"coming sta");
        }
    }

    private static boolean checkAndSendMsg(){
        int value = Settings.System.getInt(mContext.getContentResolver(),WIFI_SOFT_AP_SLEEP_POLICY,
                          WifiSoftApSleepPolicy.WIFI_SOFT_AP_SLEEP_POLICY_NEVER);
        if (value == WIFI_SOFT_AP_SLEEP_POLICY_NEVER) {
            Log.e(TAG,"always! and return");
            return false;
        }
        if (value == WIFI_SOFT_AP_SLEEP_POLICY_5_MINS)
            mHandler.start(FIVE_MINS);
        else if (value == WIFI_SOFT_AP_SLEEP_POLICY_10_MINS)
            mHandler.start(TEN_MINS);
        return true;
    }
}
