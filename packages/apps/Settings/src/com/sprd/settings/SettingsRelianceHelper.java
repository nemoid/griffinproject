package com.sprd.settings;

import android.content.Context;
import android.telephony.SubscriptionInfo;
import android.util.Log;

import com.android.settings.R;
import com.android.settings.sim.SimDialogActivity;

import android.app.AddonManager;

public class SettingsRelianceHelper {
    private static final String TAG = "SettingsRelianceHelper";
    static SettingsRelianceHelper sInstance;

    public static SettingsRelianceHelper getInstance(Context context) {
        if (sInstance != null) {
            return sInstance;
        }
        AddonManager addonManager = new AddonManager(context);
        sInstance = (SettingsRelianceHelper) addonManager.getAddon(
                R.string.settings_reliance_plugin, SettingsRelianceHelper.class);
        Log.i(TAG, "getInstance [" + sInstance + "]");
        return sInstance;
    }

    public SettingsRelianceHelper() {
    }

    /* SPRD: new featrue:Smart Dual SIM @{ */
    public boolean shouldShowSmartDualSimSOption() {
    Log.i(TAG , "Smart Dual Sim is not support");
    return false;
    }
    /* @} */

    public boolean displayConfirmDataDialog(final Context context,
            final SubscriptionInfo subscriptionInfo,final int currentDataPhoneId,final SimDialogActivity simDialogActivity) {
        return false;
    }
}
