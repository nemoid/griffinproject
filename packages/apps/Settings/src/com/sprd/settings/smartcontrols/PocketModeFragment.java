package com.sprd.settings.smartcontrols;

import com.android.settings.SettingsActivity;
import com.android.settings.Utils;
import com.android.settings.widget.SwitchBar;
import com.android.settings.SettingsPreferenceFragment;
import android.widget.Switch;
import com.android.settings.R;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceScreen;
import com.android.internal.logging.MetricsLogger;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.content.res.Resources;
import static android.provider.Settings.Global.POCKET_MODE_ENABLED;
import static android.provider.Settings.Global.TOUCH_DISABLE;
import static android.provider.Settings.Global.SMART_BELL;
import static android.provider.Settings.Global.POWER_SAVING;
import static android.provider.Settings.Global.TOUCH_DISABLE_SWITCH;
import static android.provider.Settings.Global.SMART_BELL_SWITCH;
import static android.provider.Settings.Global.POWER_SAVING_SWITCH;
import java.util.ArrayList;
import android.hardware.Sensor;
import android.hardware.SprdSensor;

public class PocketModeFragment extends SettingsPreferenceFragment implements
        SwitchBar.OnSwitchChangeListener, Preference.OnPreferenceChangeListener {

    private static final String KEY_TOUCH_DISABLE = "touch_disable";
    private static final String KEY_SMART_BELL = "smart_bell";
    private static final String KEY_POWER_SAVING = "power_saving";
    private static final int DEFAULT_ENABLED = 0;
    private int mControlSwitchBar = 0;

    private SwitchBar mSwitchBar;
    private boolean mValidListener = false;
    private SwitchPreference mTouchDisablePreference;
    private SwitchPreference mSmartBellPreference;
    private SwitchPreference mPowerSavingPreference;
    private ArrayList<String> POCKETMODE_CONTROLS = new ArrayList<String>();
    private ArrayList<String> POCKETMODE_SWITCH_CONTROLS = new ArrayList<String>();
    private ArrayList<SwitchPreference> POCKETMODEPREFERENCE_CONTROLS = new ArrayList<SwitchPreference>();

    @Override
    protected int getMetricsCategory() {
        return MetricsLogger.POCKET_MODE;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    addPreferencesFromResource(R.xml.pocket_mode);
    initializeAllPreferences();
    /* SPRD: Bug 569813 when close main switch, close sub switch function but save its status @{ */
    POCKETMODE_CONTROLS.add(TOUCH_DISABLE);
    POCKETMODE_CONTROLS.add(SMART_BELL);
    POCKETMODE_CONTROLS.add(POWER_SAVING);

    POCKETMODE_SWITCH_CONTROLS.add(TOUCH_DISABLE_SWITCH);
    POCKETMODE_SWITCH_CONTROLS.add(SMART_BELL_SWITCH);
    POCKETMODE_SWITCH_CONTROLS.add(POWER_SAVING_SWITCH);

    POCKETMODEPREFERENCE_CONTROLS.add(mTouchDisablePreference);
    POCKETMODEPREFERENCE_CONTROLS.add(mSmartBellPreference);
    POCKETMODEPREFERENCE_CONTROLS.add(mPowerSavingPreference);
    /* @} */
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final SettingsActivity activity = (SettingsActivity) getActivity();
        mSwitchBar = activity.getSwitchBar();
        mSwitchBar.show();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!mValidListener) {
            mSwitchBar.addOnSwitchChangeListener(this);
            mValidListener = true;
        }
        updateState(isPocketModeEnabled());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mValidListener) {
            mSwitchBar.removeOnSwitchChangeListener(this);
            mValidListener = false;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSwitchBar.hide();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object objValue) {
        if (preference == mTouchDisablePreference) {
            boolean checked = (Boolean) objValue;
            Settings.Global.putInt(getContentResolver(), TOUCH_DISABLE, checked ? 1 : 0);
            mTouchDisablePreference.setChecked(checked);
        }

        if (preference == mSmartBellPreference) {
            boolean checked = (Boolean) objValue;
            Settings.Global.putInt(getContentResolver(), SMART_BELL, checked ? 1 : 0);
            mSmartBellPreference.setChecked(checked);
        }

        if (preference == mPowerSavingPreference) {
            boolean checked = (Boolean) objValue;
            Settings.Global.putInt(getContentResolver(), POWER_SAVING, checked ? 1 : 0);
            mPowerSavingPreference.setChecked(checked);
        }

        return false;
    }

    @Override
    public void onSwitchChanged(Switch switchView, boolean isChecked) {
        Settings.Global.putInt(getContentResolver(), POCKET_MODE_ENABLED, isChecked ? 1 : 0);
        getPreferenceScreen().setEnabled(isChecked);
        if (!isChecked) {
            resetPreference();
        } else {
            for (int i = 0; i < POCKETMODE_SWITCH_CONTROLS.size(); i++) {
                if (Settings.Global.getInt(getContentResolver(), POCKETMODE_SWITCH_CONTROLS.get(i), 0) == 1) {
                    Settings.Global.putInt(getContentResolver(), POCKETMODE_CONTROLS.get(i), 1);
                    Settings.Global.putInt(getContentResolver(), POCKETMODE_SWITCH_CONTROLS.get(i), 0);
                }
            }
        }
    }

    private boolean isTouchDisableAvailable(Resources res) {
        return res.getBoolean(com.android.internal.R.bool.config_supportTouchDisable)
                && Utils.isSupportSensor(getActivity(), SprdSensor.TYPE_SPRDHUB_POCKET_MODE);
    }

    private boolean isSmartBellAvailable(Resources res) {
        return res.getBoolean(com.android.internal.R.bool.config_supportSmartBell)
                && Utils.isSupportSensor(getActivity(), SprdSensor.TYPE_SPRDHUB_POCKET_MODE);
    }

    private boolean isPowerSavingAvailable(Resources res) {
        return res.getBoolean(com.android.internal.R.bool.config_supportPowerSaving)
                && Utils.isSupportSensor(getActivity(), SprdSensor.TYPE_SPRDHUB_POCKET_MODE);
    }

    public final boolean isPocketModeEnabled() {
        return Settings.Global.getInt(getContentResolver(),
                Settings.Global.POCKET_MODE_ENABLED, DEFAULT_ENABLED) == 1;
    }

    /* SPRD: Bug 539959,569813 remove Easy clear memory in smart Controls @{ */
    private void resetPreference() {
        for (int i = 0; i < POCKETMODE_CONTROLS.size(); i++) {
            if (POCKETMODEPREFERENCE_CONTROLS.get(i) != null
                    && POCKETMODEPREFERENCE_CONTROLS.get(i).isChecked()) {
                Settings.Global.putInt(getContentResolver(), POCKETMODE_SWITCH_CONTROLS.get(i), 1);
            }
            Settings.Global.putInt(getContentResolver(), POCKETMODE_CONTROLS.get(i), 0);
        }
    }
    /* @} */

    private void updateState(boolean isChecked) {
        mSwitchBar.setChecked(isChecked);
        getPreferenceScreen().setEnabled(isChecked);

        /* SPRD: Bug 567104,569813 add "memory" function in Smart Control @{ */
        if (isChecked) {
            if (mTouchDisablePreference != null) {
                int value = Settings.Global.getInt(getContentResolver(), TOUCH_DISABLE, 0);
                mTouchDisablePreference.setChecked(value != 0);
            }

            if (mSmartBellPreference != null) {
                int value = Settings.Global.getInt(getContentResolver(), SMART_BELL, 0);
                mSmartBellPreference.setChecked(value != 0);
            }

            if (mPowerSavingPreference != null) {
                int value = Settings.Global.getInt(getContentResolver(), POWER_SAVING, 0);
                mPowerSavingPreference.setChecked(value != 0);
            }
        } else {
            for (int i = 0; i < POCKETMODE_SWITCH_CONTROLS.size(); i++) {
                if (Settings.Global.getInt(getContentResolver(), POCKETMODE_SWITCH_CONTROLS.get(i), 0) == 1
                        && POCKETMODEPREFERENCE_CONTROLS.get(i) != null) {
                    POCKETMODEPREFERENCE_CONTROLS.get(i).setChecked(true);
                }
            }
        }
        /* @} */
    }

    private void updateSwitchBar() {
        if (mSwitchBar != null) {
            int preferenceCount = getPreferenceScreen().getPreferenceCount();
            for (int i = 0; i < preferenceCount; ++i) {
                Preference pref = getPreferenceScreen().getPreference(i);
                if (pref instanceof SwitchPreference) {
                    mControlSwitchBar += ((SwitchPreference)pref).isChecked() ? 1:0;
                }
            }
            mSwitchBar.setChecked(mControlSwitchBar != 0);
            mControlSwitchBar = 0;
        }
    }

    private void initializeAllPreferences() {
        if (isTouchDisableAvailable(getResources())) {
            mTouchDisablePreference = (SwitchPreference) findPreference(KEY_TOUCH_DISABLE);
            mTouchDisablePreference.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_TOUCH_DISABLE);
        }

        if (isSmartBellAvailable(getResources())) {
            mSmartBellPreference = (SwitchPreference) findPreference(KEY_SMART_BELL);
            mSmartBellPreference.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_SMART_BELL);
        }

        if (isPowerSavingAvailable(getResources())) {
            mPowerSavingPreference = (SwitchPreference) findPreference(KEY_POWER_SAVING);
            mPowerSavingPreference.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_POWER_SAVING);
        }
    }
}
