
package com.sprd.settings;

import android.app.AddonManager;

public class GPSHelper {

    private static final String FEATURE_LOCATION_GPS = "plugin.sprd.location.GPSAddon";

    static GPSHelper sInstance;

    public GPSHelper() {
    }

    public static GPSHelper getInstance() {
        if (sInstance == null) {
            sInstance = (GPSHelper) AddonManager.getDefault().getAddon(FEATURE_LOCATION_GPS,
                    GPSHelper.class);
        }
        return sInstance;
    }

    public boolean isSupportCmcc() {
        return false;
    }

}
