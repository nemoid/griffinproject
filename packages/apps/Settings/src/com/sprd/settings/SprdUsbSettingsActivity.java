/** Created by Spreadst */
package com.sprd.settings;

import android.app.Activity;
import android.os.Bundle;
import com.android.settings.R;

public final class SprdUsbSettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sprd_usb_screen);
    }
}

