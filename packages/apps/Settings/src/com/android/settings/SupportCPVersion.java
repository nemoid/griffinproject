package com.android.settings;

import android.app.AddonManager;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;

import java.io.OutputStream;
import java.io.InputStream;

import android.text.TextUtils;
import android.content.SharedPreferences;
import android.support.v7.preference.Preference;
import android.preference.PreferenceManager;

import java.nio.charset.StandardCharsets;

import android.os.Handler;
import android.os.Message;
import android.content.Context;

public class SupportCPVersion {
    static SupportCPVersion mInstanceCpVersion;
    private Runnable mBasedVerRunnable;
    private Handler mHandler;
    private static final int MSG_UPDATE_BASED_VERSION_SUMMARY = 1;

    public static SupportCPVersion getInstance () {
        if (mInstanceCpVersion == null) {
            mInstanceCpVersion = (SupportCPVersion) AddonManager.getDefault().getAddon(
                    R.string.feature_support_cp2_version_addon,
                    SupportCPVersion.class);
        }
        return mInstanceCpVersion;
    }

    public boolean isSupport(){
        return false;
    }

    private void getBasedSummary(String property) {
    }

    public static String getCp2Version() {
        return null;
    }

    public void startRunnable() {
        //return mBasedVerRunnable;
    }

    public void initPreference(Context context,Preference mPreference) {
    }
}