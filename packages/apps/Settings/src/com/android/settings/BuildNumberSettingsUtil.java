/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.AddonManager;
import android.util.Log;
import com.android.settings.R;

public class BuildNumberSettingsUtil {
    static BuildNumberSettingsUtil mInstance;
    public static String TAG = "BuildNumberSettingsUtil";
    public static boolean DEBUG = true;

    public static BuildNumberSettingsUtil getInstance() {
        if (mInstance == null) {
            mInstance = (BuildNumberSettingsUtil) AddonManager.getDefault().getAddon(
                    R.string.feature_build_number_settings_addon, BuildNumberSettingsUtil.class);
        }
        return mInstance;
    }

    public boolean operatorVersion() {
        if (DEBUG) {
            Log.i(TAG, "operatorVersion");
        }
        return false;
    }

    public String getSystemProperties() {
        if (DEBUG) {
            Log.i(TAG, "getSystemProperties");
        }
        return null;
    }
}
