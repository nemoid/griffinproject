/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothPan;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.UserManager;
import android.os.SystemProperties;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;

import android.util.Log;
import android.widget.Toast;
import android.provider.Settings;
import android.os.SystemProperties;


import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.datausage.DataSaverBackend;
import com.android.settings.wifi.WifiApDialog;
import com.android.settings.wifi.WifiApEnabler;
import com.android.settingslib.TetherUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

import static android.net.ConnectivityManager.TETHERING_BLUETOOTH;
import static android.net.ConnectivityManager.TETHERING_USB;
import static android.net.ConnectivityManager.TETHERING_WIFI;

//SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
import com.sprd.settings.SprdChooseOS;
import android.os.SystemProperties;
//<-- Add for SPRD USB Feature END

//SPRD: Bug #590722 The switch status of softAp/BluetoothTether shows exception BEG-->
import com.android.settingslib.bluetooth.LocalBluetoothManager;
//<-- The switch status of softAp/BluetoothTether shows exception END

/*
 * Displays preferences for Tethering.
 */
public class TetherSettings extends RestrictedSettingsFragment
        implements DialogInterface.OnClickListener, Preference.OnPreferenceChangeListener,
        DataSaverBackend.Listener {

    private static final String USB_TETHER_SETTINGS = "usb_tether_settings";
    private static final String ENABLE_WIFI_AP = "enable_wifi_ap";
    private static final String ENABLE_BLUETOOTH_TETHERING = "enable_bluetooth_tethering";
    private static final String TETHER_CHOICE = "TETHER_TYPE";
    private static final String DATA_SAVER_FOOTER = "disabled_on_data_saver";
    public static final int TETHERING_INVALID   = -1;

    private static final int DIALOG_AP_SETTINGS = 1;

    private static final String TAG = "TetheringSettings";

    private SwitchPreference mUsbTether;

    private WifiApEnabler mWifiApEnabler;
    private SwitchPreference mEnableWifiAp;

    private SwitchPreference mBluetoothTether;

    private BroadcastReceiver mTetherChangeReceiver;

    private String[] mUsbRegexs;

    private String[] mWifiRegexs;

    private String[] mBluetoothRegexs;

    //SPRD: Bug #590722 Porting AOSP Bug (When the softAP/BluetoothTether is opened, The tips will be show every time) to AndroidN BEG-->
    //private AtomicReference<BluetoothPan> mBluetoothPan = new AtomicReference<BluetoothPan>();
    //<-- The switch status of softAp/BluetoothTether shows exception END

    private Handler mHandler = new Handler();
    private OnStartTetheringCallback mStartTetheringCallback;

    private static final String WIFI_AP_SSID_AND_SECURITY = "wifi_ap_ssid_and_security";
    //NOTE: Bug #474462 Add For SoftAp advance Feature BEG-->
    private static final String HOTSPOT_SETTINGS = "hotspot_settings";
    //<-- Add for SoftAp Advance Feature END
    private static final int CONFIG_SUBTEXT = R.string.wifi_tether_configure_subtext;

    private String[] mSecurityType;
    private Preference mCreateNetwork;

    private WifiApDialog mDialog;
    private WifiManager mWifiManager;
    private WifiConfiguration mWifiConfig = null;
    private ConnectivityManager mCm;

    private boolean mRestartWifiApAfterConfigChange;

    private boolean mUsbConnected;
    private boolean mMassStorageActive;

    private boolean mBluetoothEnableForTether;

    /* Stores the package name and the class name of the provisioning app */
    private String[] mProvisionApp;
    private static final int PROVISION_REQUEST = 0;

    private boolean mUnavailable;

    private DataSaverBackend mDataSaverBackend;
    private boolean mDataSaverEnabled;
    private Preference mDataSaverFooter;

    //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
    private static final String USB_PC_SHARE_SETTINGS = "usb_pc_share_settings";
    private static final boolean SUPPORT_USB_REVERSE_TETHER = SystemProperties.getBoolean("persist.sys.usb-pc.tethering",true);
    private SwitchPreference mUsbPcShare;
    private ConnectivityManager mConnectivityManager = null;
    private static final int PROVISION_REQUEST_USB_PC_TETHER = 1;
    //<-- Add for SPRD USB Feature END

    private boolean mSupportBtWifiSoftApCoexist = true;
    private boolean mHasHandle;

    private int mTetherChoice = TETHERING_INVALID;
    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.TETHER;
    }

    public TetherSettings() {
        super(UserManager.DISALLOW_CONFIG_TETHERING);
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.tether_prefs);

        //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
        if (mConnectivityManager == null) {
            mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        if (mWifiManager == null) {
            mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        }
        //<-- Add for SPRD USB Feature END

        mDataSaverBackend = new DataSaverBackend(getContext());
        mDataSaverEnabled = mDataSaverBackend.isDataSaverEnabled();
        mDataSaverFooter = findPreference(DATA_SAVER_FOOTER);

        setIfOnlyAvailableForAdmins(true);
        if (isUiRestricted()) {
            mUnavailable = true;
            setPreferenceScreen(new PreferenceScreen(getPrefContext(), null));
            return;
        }

        final Activity activity = getActivity();
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter != null) {
            adapter.getProfileProxy(activity.getApplicationContext(), mProfileServiceListener,
                    BluetoothProfile.PAN);
        }

        mEnableWifiAp =
                (SwitchPreference) findPreference(ENABLE_WIFI_AP);
        Preference wifiApSettings = findPreference(WIFI_AP_SSID_AND_SECURITY);
        mUsbTether = (SwitchPreference) findPreference(USB_TETHER_SETTINGS);
        //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
        mUsbPcShare = (SwitchPreference) findPreference(USB_PC_SHARE_SETTINGS);
        //<-- Add for SPRD USB Feature END
        mBluetoothTether = (SwitchPreference) findPreference(ENABLE_BLUETOOTH_TETHERING);

        mCm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        mUsbRegexs = mCm.getTetherableUsbRegexs();
        mWifiRegexs = mCm.getTetherableWifiRegexs();
        mBluetoothRegexs = mCm.getTetherableBluetoothRegexs();

        mDataSaverBackend.addListener(this);

        final boolean usbAvailable = mUsbRegexs.length != 0;
        final boolean wifiAvailable = mWifiRegexs.length != 0;
        final boolean bluetoothAvailable = mBluetoothRegexs.length != 0;

        if (!usbAvailable || Utils.isMonkeyRunning()) {
            getPreferenceScreen().removePreference(mUsbTether);
            //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
            getPreferenceScreen().removePreference(mUsbPcShare);
            //<-- Add for SPRD USB Feature END
        }

        //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
        if (!SUPPORT_USB_REVERSE_TETHER && mUsbPcShare != null){
            getPreferenceScreen().removePreference(mUsbPcShare);
        }
        //<-- Add for SPRD USB Feature END

        if (wifiAvailable && !Utils.isMonkeyRunning() &&  !Utils.WCN_DISABLED) {
            mWifiApEnabler = new WifiApEnabler(activity, mDataSaverBackend, mEnableWifiAp);
            //NOTE: Bug #474462 Add For SoftAp advance Feature BEG-->
            //initWifiTethering();
            if (SystemProperties.getInt("ro.hotspot.enabled", 1) == 0) {
                initWifiTethering();
                getPreferenceScreen().removePreference(findPreference(HOTSPOT_SETTINGS));
            } else {
                getPreferenceScreen().removePreference(wifiApSettings);
            }
            //<-- Add for SoftAp Advance Feature END
        } else {
            getPreferenceScreen().removePreference(mEnableWifiAp);
            getPreferenceScreen().removePreference(wifiApSettings);
            //NOTE: Bug #474462 Add For SoftAp advance Feature BEG-->
            getPreferenceScreen().removePreference(findPreference(HOTSPOT_SETTINGS));
            //<-- Add for SoftAp Advance Feature END
        }

        //SPRD: Bug #590722 The switch status of softAp/BluetoothTether shows exception BEG-->
        manager = LocalBluetoothManager.getInstance(getActivity());
        if (!bluetoothAvailable || Utils.WCN_DISABLED) {
            getPreferenceScreen().removePreference(mBluetoothTether);
        } else {
            //BluetoothPan pan = mBluetoothPan.get();
            //if (pan != null && pan.isTetheringOn()) {
            if (manager != null && manager.isTetheringOn()) {
        //<-- The switch status of softAp/BluetoothTether shows exception END
                mBluetoothTether.setChecked(true);
            } else {
                mBluetoothTether.setChecked(false);
            }
        }
        // Set initial state based on Data Saver mode.
        onDataSaverChanged(mDataSaverBackend.isDataSaverEnabled());
        //SPRD: Bug #474425 Add for SoftAP/BT coexist feature BEG-->
        if (!SystemProperties.getBoolean("ro.btwifisoftap.coexist", true)) {
            mSupportBtWifiSoftApCoexist = false;
        }
        //<-- Add for SoftAP/BT coexist feature END
    }

    @Override
    public void onDestroy() {
        mDataSaverBackend.remListener(this);
        // SPRD: in case of OOM issue when do pressure test
        if (mBluetoothPan.get() != null) {
            try {
                Log.d(TAG, "onDestroy closeProfileProxy()");
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(
                        BluetoothProfile.PAN, mBluetoothPan.get());
                mBluetoothPan.set(null);
            } catch (Throwable t) {
                Log.w(TAG, "Error cleaning up PAN proxy", t);
            }
        }
        super.onDestroy();
    }

    @Override
    public void onDataSaverChanged(boolean isDataSaving) {
        mDataSaverEnabled = isDataSaving;
        mEnableWifiAp.setEnabled(!mDataSaverEnabled);
        mUsbTether.setEnabled(!mDataSaverEnabled && mUsbConnected && !mUsbPcShare.isChecked());
        if (mDataSaverEnabled) {
            mTetherChoice = TETHERING_INVALID;
            updateState();
        }
        mBluetoothTether.setEnabled(!mDataSaverEnabled);
        mDataSaverFooter.setVisible(mDataSaverEnabled);
    }

    @Override
    public void onWhitelistStatusChanged(int uid, boolean isWhitelisted) {
    }

    @Override
    public void onBlacklistStatusChanged(int uid, boolean isBlacklisted)  {
    }

    private void initWifiTethering() {
        final Activity activity = getActivity();
        mWifiConfig = mWifiManager.getWifiApConfiguration();
        mSecurityType = getResources().getStringArray(R.array.wifi_ap_security);

        mCreateNetwork = findPreference(WIFI_AP_SSID_AND_SECURITY);

        mRestartWifiApAfterConfigChange = false;

        if (mWifiConfig == null) {
            final String s = activity.getString(
                    com.android.internal.R.string.wifi_tether_configure_ssid_default);
            mCreateNetwork.setSummary(String.format(activity.getString(CONFIG_SUBTEXT),
                    s, mSecurityType[WifiApDialog.OPEN_INDEX]));
        } else {
            int index = WifiApDialog.getSecurityTypeIndex(mWifiConfig);
            mCreateNetwork.setSummary(String.format(activity.getString(CONFIG_SUBTEXT),
                    mWifiConfig.SSID,
                    mSecurityType[index]));
        }
    }

    @Override
    public Dialog onCreateDialog(int id) {
        if (id == DIALOG_AP_SETTINGS) {
            final Activity activity = getActivity();
            mDialog = new WifiApDialog(activity, this, mWifiConfig);
            return mDialog;
        }

        return null;
    }

    private class TetherChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context content, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.ACTION_TETHER_STATE_CHANGED)) {
                // TODO - this should understand the interface types
                ArrayList<String> available = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_AVAILABLE_TETHER);
                ArrayList<String> active = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_ACTIVE_TETHER);
                ArrayList<String> errored = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_ERRORED_TETHER);
                updateState(available.toArray(new String[available.size()]),
                        active.toArray(new String[active.size()]),
                        errored.toArray(new String[errored.size()]));
                if (mWifiManager.getWifiApState() == WifiManager.WIFI_AP_STATE_DISABLED
                        && mRestartWifiApAfterConfigChange) {
                    mRestartWifiApAfterConfigChange = false;
                    Log.d(TAG, "Restarting WifiAp due to prior config change.");
                    startTethering(TETHERING_WIFI);
                }
            } else if (action.equals(WifiManager.WIFI_AP_STATE_CHANGED_ACTION)) {
                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_AP_STATE, 0);
                if (state == WifiManager.WIFI_AP_STATE_DISABLED
                        && mRestartWifiApAfterConfigChange) {
                    mRestartWifiApAfterConfigChange = false;
                    Log.d(TAG, "Restarting WifiAp due to prior config change.");
                    startTethering(TETHERING_WIFI);
                }
            } else if (action.equals(Intent.ACTION_MEDIA_SHARED)) {
                mMassStorageActive = true;
                updateState();
            } else if (action.equals(Intent.ACTION_MEDIA_UNSHARED)) {
                mMassStorageActive = false;
                updateState();
            } else if (action.equals(UsbManager.ACTION_USB_STATE)) {
                mUsbConnected = intent.getBooleanExtra(UsbManager.USB_CONNECTED, false);
                updateState();
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                if (mBluetoothEnableForTether) {
                    switch (intent
                            .getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                        case BluetoothAdapter.STATE_ON:
                            startTethering(TETHERING_BLUETOOTH);
                            mBluetoothEnableForTether = false;
                            break;

                        case BluetoothAdapter.STATE_OFF:
                        case BluetoothAdapter.ERROR:
                            mBluetoothEnableForTether = false;
                            break;

                        default:
                            // ignore transition states
                    }
                }
                //SPRD: Bug#607179 Add for SoftAP/BT tethering  not coexist BEG-->
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR) == BluetoothAdapter.STATE_OFF &&
                        mTetherChoice == TETHERING_BLUETOOTH) {
                        mTetherChoice = TETHERING_INVALID;
                }
               //<-- Add for SoftAP/BT tethering  not coexist
                updateState();
            }
            //SPRD: Bug #474425 Add for SoftAP/BT coexist feature BEG-->
            else if (action.equals(BluetoothPan.ACTION_CONNECTION_STATE_CHANGED)) {
                int localRole = intent.getIntExtra(BluetoothPan.EXTRA_LOCAL_ROLE, 0);
                if(localRole == BluetoothPan.LOCAL_NAP_ROLE) {
                    updateState();
                }
            } else if (action.equals(WifiManager.WIFI_AP_STATE_CHANGED_ACTION)) {
                int wifiApState = intent.getIntExtra(WifiManager.EXTRA_WIFI_AP_STATE, WifiManager.WIFI_AP_STATE_FAILED);
                if (wifiApState == WifiManager.WIFI_AP_STATE_DISABLED && mTetherChoice == TETHERING_WIFI) {
                    mTetherChoice = TETHERING_INVALID;
                }
            }
            //<-- Add for SoftAP/BT coexist feature END
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mUnavailable) {
            if (!isUiRestrictedByOnlyAdmin()) {
                getEmptyTextView().setText(R.string.tethering_settings_not_available);
            }
            getPreferenceScreen().removeAll();
            return;
        }

        final Activity activity = getActivity();

        mStartTetheringCallback = new OnStartTetheringCallback(this);

        mMassStorageActive = Environment.MEDIA_SHARED.equals(Environment.getExternalStorageState());
        mTetherChangeReceiver = new TetherChangeReceiver();
        IntentFilter filter = new IntentFilter(ConnectivityManager.ACTION_TETHER_STATE_CHANGED);
        filter.addAction(WifiManager.WIFI_AP_STATE_CHANGED_ACTION);
        Intent intent = activity.registerReceiver(mTetherChangeReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_STATE);
        activity.registerReceiver(mTetherChangeReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_SHARED);
        filter.addAction(Intent.ACTION_MEDIA_UNSHARED);
        filter.addDataScheme("file");
        activity.registerReceiver(mTetherChangeReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        //SPRD: Bug #474425 Add for SoftAP/BT coexist feature BEG-->.
        filter.addAction(BluetoothPan.ACTION_CONNECTION_STATE_CHANGED);
        filter.addAction(WifiManager.WIFI_AP_STATE_CHANGED_ACTION);
        //<-- Add for SoftAP/BT coexist feature END
        activity.registerReceiver(mTetherChangeReceiver, filter);

        if (intent != null) mTetherChangeReceiver.onReceive(activity, intent);
        if (mWifiApEnabler != null) {
            mEnableWifiAp.setOnPreferenceChangeListener(this);
            mWifiApEnabler.resume();
        }
        //SPRD: Bug #474425 Add for SoftAP/BT coexist feature BEG-->.
        if (mBluetoothTether != null) {
            mBluetoothTether.setOnPreferenceChangeListener(this);
        }
        //<-- Add for SoftAP/BT coexist feature END

        updateState();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mUnavailable) {
            return;
        }
        getActivity().unregisterReceiver(mTetherChangeReceiver);
        mTetherChangeReceiver = null;
        mStartTetheringCallback = null;
        if (mWifiApEnabler != null) {
            mEnableWifiAp.setOnPreferenceChangeListener(null);
            mWifiApEnabler.pause();
        }
    }

    private void updateState() {
        String[] available = mCm.getTetherableIfaces();
        String[] tethered = mCm.getTetheredIfaces();
        String[] errored = mCm.getTetheringErroredIfaces();
        updateState(available, tethered, errored);
    }

    private void updateState(String[] available, String[] tethered,
            String[] errored) {
        updateUsbState(available, tethered, errored);
        updateBluetoothState(available, tethered, errored);
    }


    private void updateUsbState(String[] available, String[] tethered,
            String[] errored) {
        //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
        /*ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);*/
        //<-- Add for SPRD USB Feature END
        boolean usbAvailable = mUsbConnected && !mMassStorageActive;
        int usbError = ConnectivityManager.TETHER_ERROR_NO_ERROR;
        for (String s : available) {
            for (String regex : mUsbRegexs) {
                if (s.matches(regex)) {
                    if (usbError == ConnectivityManager.TETHER_ERROR_NO_ERROR) {
                        usbError = mCm.getLastTetherError(s);
                    }
                }
            }
        }
        boolean usbTethered = false;
        for (String s : tethered) {
            for (String regex : mUsbRegexs) {
                if (s.matches(regex)) usbTethered = true;
            }
        }
        boolean usbErrored = false;
        for (String s: errored) {
            for (String regex : mUsbRegexs) {
                if (s.matches(regex)) usbErrored = true;
            }
        }
        //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
        if (mCm.getPCNetTether()) {
            mUsbPcShare.setSummary(R.string.usb_pc_internet_share_active_subtext);
            mUsbTether.setEnabled(false);
            mUsbPcShare.setEnabled(true);
            mUsbPcShare.setChecked(true);
        } else if (usbTethered) {
            mUsbTether.setSummary(R.string.usb_tethering_active_subtext);
            mUsbTether.setEnabled(!mDataSaverEnabled);
            mUsbTether.setChecked(true);
            mUsbPcShare.setEnabled(false);
            mUsbPcShare.setChecked(false);
        } else if (usbAvailable) {
            if (usbError == ConnectivityManager.TETHER_ERROR_NO_ERROR) {
                mUsbPcShare.setSummary(R.string.usb_pc_internet_share_available_subtext);
                mUsbTether.setSummary(R.string.usb_tethering_available_subtext);
            } else {
                mUsbTether.setSummary(R.string.usb_tethering_errored_subtext);
            }
            mUsbTether.setEnabled(!mDataSaverEnabled);
            mUsbTether.setChecked(false);
            mUsbPcShare.setEnabled(mTetherChoice!=TETHERING_USB);
            mUsbPcShare.setChecked(false);
        } else if (usbErrored) {
            mTetherChoice = TETHERING_INVALID;
            mUsbTether.setSummary(R.string.usb_tethering_errored_subtext);
            mUsbTether.setEnabled(false);
            mUsbTether.setChecked(false);
            mUsbPcShare.setEnabled(false);
            mUsbPcShare.setChecked(false);
        } else if (mMassStorageActive) {
            mUsbTether.setSummary(R.string.usb_tethering_storage_active_subtext);
            mUsbTether.setEnabled(false);
            mUsbTether.setChecked(false);
            mUsbPcShare.setEnabled(false);
            mUsbPcShare.setChecked(false);
        } else {
            mUsbTether.setSummary(R.string.usb_tethering_unavailable_subtext);
            mUsbTether.setEnabled(false);
            mUsbTether.setChecked(false);
            mUsbPcShare.setEnabled(false);
            mUsbPcShare.setChecked(false);
        //<-- Add for SPRD USB Feature END
        }
    }

    private void updateBluetoothState(String[] available, String[] tethered,
            String[] errored) {
        boolean bluetoothErrored = false;
        for (String s: errored) {
            for (String regex : mBluetoothRegexs) {
                if (s.matches(regex)) bluetoothErrored = true;
            }
        }

        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {
            return;
        }
        int btState = adapter.getState();
        if (btState == BluetoothAdapter.STATE_TURNING_OFF) {
            mBluetoothTether.setEnabled(false);
            mBluetoothTether.setSummary(R.string.bluetooth_turning_off);
        } else if (btState == BluetoothAdapter.STATE_TURNING_ON) {
            mBluetoothTether.setEnabled(false);
            mBluetoothTether.setSummary(R.string.bluetooth_turning_on);
        } else {
            BluetoothPan bluetoothPan = mBluetoothPan.get();
            if (btState == BluetoothAdapter.STATE_ON && bluetoothPan != null
                    && bluetoothPan.isTetheringOn()) {
                mBluetoothTether.setChecked(!mDataSaverEnabled);
                mBluetoothTether.setEnabled(!mDataSaverEnabled);
                int bluetoothTethered = bluetoothPan.getConnectedDevices().size();
                if (bluetoothTethered > 1) {
                    String summary = getString(
                            R.string.bluetooth_tethering_devices_connected_subtext,
                            bluetoothTethered);
                    mBluetoothTether.setSummary(summary);
                } else if (bluetoothTethered == 1) {
                    mBluetoothTether.setSummary(
                            R.string.bluetooth_tethering_device_connected_subtext);
                } else if (bluetoothErrored) {
                    mBluetoothTether.setSummary(R.string.bluetooth_tethering_errored_subtext);
                } else {
                    mBluetoothTether.setSummary(R.string.bluetooth_tethering_available_subtext);
                }
                if(mDataSaverEnabled){
                    stopBluetoothTethering();
                }
            } else {
                mBluetoothTether.setEnabled(!mDataSaverEnabled);
                //SPRD: Bug #590722 The switch status of softAp/BluetoothTether shows exception BEG-->
                if (manager != null && manager.isTetheringOn()) {
                    mBluetoothTether.setChecked(true);
                    mBluetoothTether.setSummary(R.string.bluetooth_tethering_available_subtext);
                } else {
                    mBluetoothTether.setChecked(false);
                    mBluetoothTether.setSummary(R.string.bluetooth_tethering_off_subtext);
                }
                //<-- The switch status of softAp/BluetoothTether shows exception END
            }
        }
    }
    private void stopBluetoothTethering(){
        if (TetherUtil.isProvisioningNeeded(getActivity())) {
            TetherService.cancelRecheckAlarmIfNecessary(getActivity(), TETHERING_BLUETOOTH);
        }
        boolean errored = false;
        String [] tethered = mCm.getTetheredIfaces();
        String bluetoothIface = findIface(tethered, mBluetoothRegexs);
        if (bluetoothIface != null &&
                mCm.untether(bluetoothIface) != ConnectivityManager.TETHER_ERROR_NO_ERROR) {
            errored = true;
        }
        BluetoothPan bluetoothPan = mBluetoothPan.get();
        if (bluetoothPan != null) bluetoothPan.setBluetoothTethering(false);
        if (errored) {
            mBluetoothTether.setSummary(R.string.bluetooth_tethering_errored_subtext);
        } else {
            mBluetoothTether.setSummary(R.string.bluetooth_tethering_off_subtext);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        boolean enable = (Boolean) value;
        //SPRD: Bug #474425 Add for SoftAP/BT coexist feature BEG-->
        if (ENABLE_WIFI_AP.equals(preference.getKey())) {
            if (enable) {
               if (!mSupportBtWifiSoftApCoexist) {
                    if (mTetherChoice == TETHERING_BLUETOOTH) {
                           Toast.makeText(this.getActivity(), R.string.softap_bt_cannot_coexist, Toast.LENGTH_SHORT).show();
                           mHasHandle = true;
                           return false;
                    }
                    BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                    int btState = adapter.getState();
                    if(enable && (btState != BluetoothAdapter.STATE_OFF) ) {
                        Toast.makeText(this.getActivity(), R.string.softap_bt_cannot_coexist, Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
                startTethering(TETHERING_WIFI);
            } else {
                mCm.stopTethering(TETHERING_WIFI);
                mTetherChoice = TETHERING_INVALID;
            }
        } else if (ENABLE_BLUETOOTH_TETHERING.equals(preference.getKey())) {
            if (enable) {
                if (!mSupportBtWifiSoftApCoexist) {
                    if (mTetherChoice == TETHERING_WIFI) {
                        Toast.makeText(this.getActivity(),R.string.bt_softap_cannot_coexist,Toast.LENGTH_SHORT).show();
                        mHasHandle = true;
                        return false;
                    }
                    if (mWifiManager == null)
                        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                    if (enable && mWifiManager.isSoftapEnablingOrEnabled()) {
                        Toast.makeText(this.getActivity(),R.string.bt_softap_cannot_coexist,Toast.LENGTH_SHORT).show();
                        mHasHandle = true;
                        return false;
                    }
                }
            }
            return true;
        }
        //<-- Add for SoftAP/BT coexist feature END
        return false;
    }

    public static boolean isProvisioningNeededButUnavailable(Context context) {
        return (TetherUtil.isProvisioningNeeded(context)
                && !isIntentAvailable(context));
    }

    private static boolean isIntentAvailable(Context context) {
        String[] provisionApp = context.getResources().getStringArray(
                com.android.internal.R.array.config_mobile_hotspot_provision_app);
        final PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClassName(provisionApp[0], provisionApp[1]);

        return (packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY).size() > 0);
    }

    private void startTethering(int choice) {
        //SPRD: Bug#607179 Add for SoftAP/BT tethering  not coexist BEG -->
        if (choice == TETHERING_WIFI || choice == TETHERING_BLUETOOTH) {
            mTetherChoice =choice;
        }
        //<-- Add for SoftAP/BT tethering  not coexist
        if (choice == TETHERING_BLUETOOTH) {
            // Turn on Bluetooth first.
            BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            if (adapter.getState() == BluetoothAdapter.STATE_OFF) {
                mBluetoothEnableForTether = true;
                adapter.enable();
                mBluetoothTether.setSummary(R.string.bluetooth_turning_on);
                mBluetoothTether.setEnabled(false);
                return;
            }
        }

        mCm.startTethering(choice, true, mStartTetheringCallback, mHandler);
    }

    @Override







    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference == mUsbTether) {
            if (mUsbTether.isChecked()) {
                //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
                mUsbPcShare.setEnabled(false);
                //<-- Add for SPRD USB Feature END
                mTetherChoice = TETHERING_USB;
                startTethering(TETHERING_USB);
            } else {
                mTetherChoice = TETHERING_INVALID;
                mCm.stopTethering(TETHERING_USB);
            }
        //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
        } else if (preference == mUsbPcShare) {
            if (!mUsbPcShare.isChecked()) {
                setInternetShare(false, null);
            } else {
                mUsbPcShare.setEnabled(false);
                Intent chooseOSIntent = new Intent(getActivity(),SprdChooseOS.class);
                startActivityForResult(chooseOSIntent, PROVISION_REQUEST_USB_PC_TETHER);
            }
        //<-- Add for SPRD USB Feature END
        } else if (preference == mBluetoothTether) {
            //SPRD: Bug #474425 Add for SoftAP/BT coexist feature BEG-->
            if (!mHasHandle) {
                if (mBluetoothTether.isChecked()) {
                    startTethering(TETHERING_BLUETOOTH);
                } else {
                    stopBluetoothTethering();
                }
            }
            //<-- Add for SoftAP/BT coexist feature END
        } else if (preference == mCreateNetwork) {
            showDialog(DIALOG_AP_SETTINGS);
        }
        mHasHandle = false;

        return super.onPreferenceTreeClick(preference);
    }

    public void onClick(DialogInterface dialogInterface, int button) {
        if (button == DialogInterface.BUTTON_POSITIVE) {
            mWifiConfig = mDialog.getConfig();
            if (mWifiConfig != null) {
                /**
                 * if soft AP is stopped, bring up
                 * else restart with new config
                 * TODO: update config on a running access point when framework support is added
                 */
                if (mWifiManager.getWifiApState() == WifiManager.WIFI_AP_STATE_ENABLED) {
                    Log.d("TetheringSettings",
                            "Wifi AP config changed while enabled, stop and restart");
                    mRestartWifiApAfterConfigChange = true;
                    mCm.stopTethering(TETHERING_WIFI);
                }
                mWifiManager.setWifiApConfiguration(mWifiConfig);
                int index = WifiApDialog.getSecurityTypeIndex(mWifiConfig);
                mCreateNetwork.setSummary(String.format(getActivity().getString(CONFIG_SUBTEXT),
                        mWifiConfig.SSID,
                        mSecurityType[index]));
            }
        }
    }

    @Override
    public int getHelpResource() {
        return R.string.help_url_tether;
    }

    private BluetoothProfile.ServiceListener mProfileServiceListener =
            new BluetoothProfile.ServiceListener() {
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            mBluetoothPan.set((BluetoothPan) proxy);
        }
        public void onServiceDisconnected(int profile) {
            mBluetoothPan.set(null);
        }
    };

    private static final class OnStartTetheringCallback extends
            ConnectivityManager.OnStartTetheringCallback {
        final WeakReference<TetherSettings> mTetherSettings;

        OnStartTetheringCallback(TetherSettings settings) {
            mTetherSettings = new WeakReference<TetherSettings>(settings);
        }

        @Override
        public void onTetheringStarted() {
            update();
        }

        @Override
        public void onTetheringFailed() {
            TetherSettings settings = mTetherSettings.get();
            if(TETHERING_USB == settings.mTetherChoice){
                settings.mTetherChoice = TETHERING_INVALID;
            }
            update();
        }

        private void update() {
            TetherSettings settings = mTetherSettings.get();
            if (settings != null) {
                settings.updateState();
            }
        }
    }

    //=============================================================================
    // add by sprd start
    //=============================================================================
    //SPRD: Bug#474460 Add for SPRD USB Feature BEG-->
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Log.d(TAG, "onActivityResult requestCode = " + requestCode + ", resultCode = " + resultCode);
        if (requestCode == PROVISION_REQUEST_USB_PC_TETHER) {
            if (resultCode == Activity.RESULT_OK) {
                setInternetShare(true, intent.getStringExtra("usb_rndis_ip_address"));
            }
        }
    }

    private void setInternetShare(boolean isShared, String ip) {
        int result = ConnectivityManager.TETHER_ERROR_NO_ERROR;
        Log.d(TAG, "setInternetShare(" + isShared +"," + " " + ip + ")");
        if(isShared) {
            mWifiManager.setWifiEnabled(false);
            result = mConnectivityManager.enableTetherPCInternet(ip);
        } else {
            result = mConnectivityManager.disableTetherPCInternet();
        }
    }
    //<-- Add for SPRD USB Feature END


    //SPRD: Bug #474425 Add for SoftAP/BT coexist feature BEG-->
    private static String findIface(String[] ifaces, String[] regexes) {
        for (String iface : ifaces) {
            for (String regex : regexes) {
                if (iface.matches(regex)) {
                    return iface;
                }
            }
        }
        return null;
    }
    //<-- Add for SoftAP/BT coexist feature END

    //SPRD: Bug #590722 The switch status of softAp/BluetoothTether shows exception BEG-->
    private static AtomicReference<BluetoothPan> mBluetoothPan = new AtomicReference<BluetoothPan>();
    private LocalBluetoothManager manager;
    //<-- The switch status of softAp/BluetoothTether shows exception END
}
