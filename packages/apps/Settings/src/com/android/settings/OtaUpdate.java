package com.android.settings;

import android.app.AddonManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.UserHandle;

import android.support.v7.preference.PreferenceScreen;

import com.android.internal.logging.MetricsProto.MetricsEvent;

public class OtaUpdate extends SettingsPreferenceFragment{
    static OtaUpdate mInstanceOtaUpdate;
    private Context mContext;
    private BroadcastReceiver mBatteryLevelRcvr;
    private IntentFilter mBatteryLevelFilter;
    private int mLevelPower;

    public static OtaUpdate getInstance(Context context) {
        if (mInstanceOtaUpdate == null) {
            AddonManager addonManager = new AddonManager(context);
            mInstanceOtaUpdate = (OtaUpdate) addonManager.getAddon(
                    R.string.feature_support_ota_update,OtaUpdate.class);

        }
        return mInstanceOtaUpdate;
    }
    public boolean  isSupport() {
        return false;
    }
    /* SPRD BUG: 616438  */
    public void initRecoverySystemUpdatePreference(Context context, PreferenceScreen preferenceScreen,Context appContext) {
    }

    public void monitorBatteryState() {
    }

    public void unregisterBatteryReceiver() {
    }

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return MetricsEvent.NOTIFICATION_ZEN_MODE;
    }
}
