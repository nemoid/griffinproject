/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings;
import android.content.Context;
import android.content.res.Resources;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import android.content.Intent;
import android.content.ComponentName;
import android.content.Context;  

/**
 * Settings activity for Gesturer. Currently implements the following setting:
 */
public class GesturerSettings extends SettingsPreferenceFragment  {
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(Intent.ACTION_MAIN); 
				intent.addCategory(Intent.CATEGORY_LAUNCHER);             
				ComponentName cn = new ComponentName("com.android.systemui", "com.zyt.close_gesture_sttings.MainActivity");             
				intent.setComponent(cn); 
				startActivity(intent);
        
    }
     @Override
    protected int getMetricsCategory() {
       return MetricsEvent.USER;
    }
    @Override
    public void onResume() {
        super.onResume();
        getActivity().onBackPressed();
    }
}
