/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.search;

import android.provider.SearchIndexableResource;
import com.android.settings.DateTimeSettings;
import com.android.settings.DevelopmentSettings;
import com.android.settings.DeviceInfoSettings;
import com.android.settings.DisplaySettings;
import com.android.settings.LegalSettings;
import com.android.settings.PressBrightness;
import com.android.settings.PrivacySettings;
import com.android.settings.R;
import com.android.settings.ScreenPinningSettings;
import com.android.settings.SecuritySettings;
import com.android.settings.Utils;
import com.android.settings.WallpaperTypeSettings;
import com.android.settings.WirelessSettings;
import com.android.settings.accessibility.AccessibilitySettings;
import com.android.settings.accounts.AccountSettings;
import com.android.settings.applications.AdvancedAppSettings;
import com.android.settings.applications.App3rdAutoRunControlerFragment;
import com.android.settings.applications.AppAsLaunchFragment;
import com.android.settings.applications.NotificationApps;
import com.android.settings.applications.SpecialAccessSettings;
import com.android.settings.bluetooth.BluetoothSettings;
import com.android.settings.datausage.DataUsageMeteredSettings;
import com.android.settings.datausage.DataUsageSummary;
import com.android.settings.deviceinfo.StorageSettings;
import com.android.settings.display.ScreenZoomSettings;
import com.android.settings.fuelgauge.BatterySaverSettings;
import com.android.settings.fuelgauge.PowerUsageSummary;
import com.android.settings.inputmethod.InputMethodAndLanguageSettings;
import com.android.settings.location.LocationSettings;
import com.android.settings.location.ScanningSettings;
import com.android.settings.notification.ConfigureNotificationSettings;
import com.android.settings.notification.OtherSoundSettings;
import com.android.settings.notification.SoundSettings;
import com.android.settings.notification.ZenModePrioritySettings;
import com.android.settings.notification.ZenModeSettings;
import com.android.settings.notification.ZenModeVisualInterruptionSettings;
import com.android.settings.print.PrintSettingsFragment;
import com.android.settings.sim.SimSettings;
import com.android.settings.users.UserSettings;
import com.android.settings.wifi.AdvancedWifiSettings;
import com.android.settings.wifi.SavedAccessPointsWifiSettings;
import com.android.settings.wifi.WifiSettings;
import com.android.settings.applications.ProcessStatsSummary;
import com.sprd.settings.smartcontrols.SmartControlsSettings;

//SPRD: Bug#474680  support AGPS settings BEG -->
import android.location.LocationManager;
import com.sprd.settings.GPSHelper;
import com.sprd.settings.LocationAgpsEnableConfig;
//<-- support AGPS settings END

import java.util.Collection;
import java.util.HashMap;
import com.android.settings.DisableDataUsage;
import com.android.settings.FingerSettings;

import com.sprd.settings.navigation.NavigationBarSettings;
import com.sprd.settings.timerpower.AlarmClock;
public final class SearchIndexableResources {

    public static int NO_DATA_RES_ID = 0;

    private static HashMap<String, SearchIndexableResource> sResMap =
            new HashMap<String, SearchIndexableResource>();

    static {
        sResMap.put(WifiSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(WifiSettings.class.getName()),
                        NO_DATA_RES_ID,
                        WifiSettings.class.getName(),
                        R.drawable.ic_settings_wireless));

        sResMap.put(AdvancedWifiSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(AdvancedWifiSettings.class.getName()),
                        R.xml.wifi_advanced_settings,
                        AdvancedWifiSettings.class.getName(),
                        R.drawable.ic_settings_wireless));

        sResMap.put(SavedAccessPointsWifiSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(SavedAccessPointsWifiSettings.class.getName()),
                        R.xml.wifi_display_saved_access_points,
                        SavedAccessPointsWifiSettings.class.getName(),
                        R.drawable.ic_settings_wireless));

        sResMap.put(BluetoothSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(BluetoothSettings.class.getName()),
                        NO_DATA_RES_ID,
                        BluetoothSettings.class.getName(),
                        R.drawable.ic_settings_bluetooth));

        /* SPRD:modify for Bug 647253 wifi data usage can be searched in CMCC version. @{ */
        boolean isDataUsageEnabled = true;
        if (!Utils.isBandwidthControlEnabled() || DisableDataUsage.getInstance().isCmcc()) {
            isDataUsageEnabled = false;
        }
        if (isDataUsageEnabled) {
        /* @} */
            sResMap.put(DataUsageSummary.class.getName(),
                    new SearchIndexableResource(
                            Ranking.getRankForClassName(DataUsageSummary.class.getName()),
                            NO_DATA_RES_ID,
                            DataUsageSummary.class.getName(),
                            R.drawable.ic_settings_data_usage));
        }

        sResMap.put(SimSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(SimSettings.class.getName()),
                        NO_DATA_RES_ID,
                        SimSettings.class.getName(),
                        R.drawable.ic_sim_sd));
       /* SPRD BUG 645235 dont allow to serach datausages when it;s cmcc version*/
       /* sResMap.put(DataUsageSummary.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(DataUsageSummary.class.getName()),
                        NO_DATA_RES_ID,
                        DataUsageSummary.class.getName(),
                        R.drawable.ic_settings_data_usage));*/

        sResMap.put(DataUsageMeteredSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(DataUsageMeteredSettings.class.getName()),
                        NO_DATA_RES_ID,
                        DataUsageMeteredSettings.class.getName(),
                        R.drawable.ic_settings_data_usage));

        sResMap.put(WirelessSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(WirelessSettings.class.getName()),
                        NO_DATA_RES_ID,
                        WirelessSettings.class.getName(),
                        R.drawable.ic_settings_more));

        sResMap.put(ScreenZoomSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(ScreenZoomSettings.class.getName()),
                        NO_DATA_RES_ID,
                        ScreenZoomSettings.class.getName(),
                        R.drawable.ic_settings_display));
        /* SPRD:add for bug 601661. @{ */
        if(PressBrightness.getInstance().isSupport()){
            sResMap.put(DisplaySettings.class.getName(),
                    new SearchIndexableResource(
                            Ranking.getRankForClassName(DisplaySettings.class.getName()),
                            NO_DATA_RES_ID,
                            DisplaySettings.class.getName(),
                            R.drawable.ic_settings_display));
        }
        /* @} */
        sResMap.put(WallpaperTypeSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(WallpaperTypeSettings.class.getName()),
                        NO_DATA_RES_ID,
                        WallpaperTypeSettings.class.getName(),
                        R.drawable.ic_settings_display));

        sResMap.put(ConfigureNotificationSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(ConfigureNotificationSettings.class.getName()),
                        R.xml.configure_notification_settings,
                        ConfigureNotificationSettings.class.getName(),
                        R.drawable.ic_settings_notifications));

        sResMap.put(SoundSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(SoundSettings.class.getName()),
                        NO_DATA_RES_ID,
                        SoundSettings.class.getName(),
                        R.drawable.ic_settings_sound));

        sResMap.put(OtherSoundSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(OtherSoundSettings.class.getName()),
                        NO_DATA_RES_ID,
                        OtherSoundSettings.class.getName(),
                        R.drawable.ic_settings_sound));

        //SPRD: Modified for bug 600120, Add zen_mode settings access
        sResMap.put(ZenModeSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(ZenModeSettings.class.getName()),
                        R.xml.zen_mode_settings,
                        ZenModeSettings.class.getName(),
                        R.drawable.ic_settings_sound));

        sResMap.put(ZenModePrioritySettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(ZenModePrioritySettings.class.getName()),
                        R.xml.zen_mode_priority_settings,
                        ZenModePrioritySettings.class.getName(),
                        R.drawable.ic_settings_notifications));

        sResMap.put(StorageSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(StorageSettings.class.getName()),
                        NO_DATA_RES_ID,
                        StorageSettings.class.getName(),
                        R.drawable.ic_settings_storage));

        sResMap.put(PowerUsageSummary.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(PowerUsageSummary.class.getName()),
                        R.xml.power_usage_summary,
                        PowerUsageSummary.class.getName(),
                        R.drawable.ic_settings_battery));

        sResMap.put(BatterySaverSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(BatterySaverSettings.class.getName()),
                        R.xml.battery_saver_settings,
                        BatterySaverSettings.class.getName(),
                        R.drawable.ic_settings_battery));

        sResMap.put(AdvancedAppSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(AdvancedAppSettings.class.getName()),
                        NO_DATA_RES_ID,
                        AdvancedAppSettings.class.getName(),
                        R.drawable.ic_settings_applications));

        sResMap.put(SpecialAccessSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(SpecialAccessSettings.class.getName()),
                        R.xml.special_access,
                        SpecialAccessSettings.class.getName(),
                        R.drawable.ic_settings_applications));

        sResMap.put(UserSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(UserSettings.class.getName()),
                        NO_DATA_RES_ID,
                        UserSettings.class.getName(),
                        R.drawable.ic_settings_multiuser));

        if (!Utils.WCN_DISABLED) {
            sResMap.put(LocationSettings.class.getName(),
                    new SearchIndexableResource(
                            Ranking.getRankForClassName(LocationSettings.class.getName()),
                            R.xml.location_settings,
                            LocationSettings.class.getName(),
                            R.drawable.ic_settings_location));

            sResMap.put(ScanningSettings.class.getName(),
                    new SearchIndexableResource(
                            Ranking.getRankForClassName(ScanningSettings.class.getName()),
                            R.xml.location_scanning,
                            ScanningSettings.class.getName(),
                            R.drawable.ic_settings_location));
        }

        sResMap.put(SecuritySettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(SecuritySettings.class.getName()),
                        NO_DATA_RES_ID,
                        SecuritySettings.class.getName(),
                        R.drawable.ic_settings_security));

        sResMap.put(ScreenPinningSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(ScreenPinningSettings.class.getName()),
                        NO_DATA_RES_ID,
                        ScreenPinningSettings.class.getName(),
                        R.drawable.ic_settings_security));

        sResMap.put(AccountSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(AccountSettings.class.getName()),
                        NO_DATA_RES_ID,
                        AccountSettings.class.getName(),
                        R.drawable.ic_settings_accounts));

        sResMap.put(InputMethodAndLanguageSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(InputMethodAndLanguageSettings.class.getName()),
                        NO_DATA_RES_ID,
                        InputMethodAndLanguageSettings.class.getName(),
                        R.drawable.ic_settings_language));

        sResMap.put(PrivacySettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(PrivacySettings.class.getName()),
                        NO_DATA_RES_ID,
                        PrivacySettings.class.getName(),
                        R.drawable.ic_settings_backup));

        sResMap.put(DateTimeSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(DateTimeSettings.class.getName()),
                        R.xml.date_time_prefs,
                        DateTimeSettings.class.getName(),
                        R.drawable.ic_settings_date_time));

        sResMap.put(AccessibilitySettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(AccessibilitySettings.class.getName()),
                        NO_DATA_RES_ID,
                        AccessibilitySettings.class.getName(),
                        R.drawable.ic_settings_accessibility));

        sResMap.put(PrintSettingsFragment.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(PrintSettingsFragment.class.getName()),
                        NO_DATA_RES_ID,
                        PrintSettingsFragment.class.getName(),
                        R.drawable.ic_settings_print));

        sResMap.put(DevelopmentSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(DevelopmentSettings.class.getName()),
                        NO_DATA_RES_ID,
                        DevelopmentSettings.class.getName(),
                        R.drawable.ic_settings_development));

        sResMap.put(DeviceInfoSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(DeviceInfoSettings.class.getName()),
                        NO_DATA_RES_ID,
                        DeviceInfoSettings.class.getName(),
                        R.drawable.ic_settings_about));

        sResMap.put(LegalSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(LegalSettings.class.getName()),
                        NO_DATA_RES_ID,
                        LegalSettings.class.getName(),
                        R.drawable.ic_settings_about));

        /*sResMap.put(ZenModeVisualInterruptionSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(
                                ZenModeVisualInterruptionSettings.class.getName()),
                        R.xml.zen_mode_visual_interruptions_settings,
                        ZenModeVisualInterruptionSettings.class.getName(),
                        R.drawable.ic_settings_notifications));*/

        /*SPRD:Bug 555355 Support 3rd app auto-run control.*/
        final SearchIndexableResource app3rdAutoRunResource = new SearchIndexableResource(
                Ranking.getRankForClassName(App3rdAutoRunControlerFragment.class.getName()),
                NO_DATA_RES_ID, App3rdAutoRunControlerFragment.class.getName(),
                R.drawable.ic_settings_3rdapps);
        sResMap.put(App3rdAutoRunControlerFragment.class.getName(), app3rdAutoRunResource);
        final SearchIndexableResource appAsLaunchResource = new SearchIndexableResource(
                Ranking.getRankForClassName(AppAsLaunchFragment.class.getName()),
                NO_DATA_RES_ID, AppAsLaunchFragment.class.getName(),
                R.drawable.ic_settings_appaslaunch);
        sResMap.put(AppAsLaunchFragment.class.getName(), appAsLaunchResource);
        /* @} */
       //SPRD: Bug#474680  support AGPS settings BEG -->
        if (GPSHelper.getInstance().isSupportCmcc() && !Utils.WCN_DISABLED && !Utils.GPS_DISABLED) {
            sResMap.put(LocationAgpsEnableConfig.class.getName(),
                    new SearchIndexableResource(
                            Ranking.getRankForClassName(LocationAgpsEnableConfig.class.getName()),
                            R.xml.agps_enable_config,
                            LocationAgpsEnableConfig.class.getName(),
                            R.drawable.ic_settings_location));
        }
        //<-- support AGPS settings END

        /* SPRD 535100 ,add the search for dynamic navigationbar @{ */
        final SearchIndexableResource navigationResource = new SearchIndexableResource(
            Ranking.getRankForClassName(NavigationBarSettings.class.getName()),
            NO_DATA_RES_ID, NavigationBarSettings.class.getName(),
            R.drawable.ic_settings_navigation_bar);
        navigationResource.intentAction = "android.intent.action.MAIN";
        navigationResource.intentTargetPackage = "com.android.settings";
        navigationResource.intentTargetClass = "com.sprd.settings.navigation.NavigationBarSettings";
        sResMap.put(NavigationBarSettings.class.getName(), navigationResource);
        /* SPRD: Bug 511228 Smart Controls in Settings @{ */
        sResMap.put(SmartControlsSettings.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(SmartControlsSettings.class.getName()),
                        NO_DATA_RES_ID,
                        SmartControlsSettings.class.getName(),
                        R.drawable.ic_settings_smart_controls));
        /* @} */
        /* SPRD: Bug 596066 Timer power cannot be searched @{ */
        final SearchIndexableResource alarmClockResource = new SearchIndexableResource(
                Ranking.getRankForClassName(AlarmClock.class.getName()),
                NO_DATA_RES_ID, AlarmClock.class.getName(),
                R.drawable.ic_settings_power_off);
                alarmClockResource.intentAction = "android.intent.action.MAIN";
                alarmClockResource.intentTargetPackage = "com.android.settings";
                alarmClockResource.intentTargetClass = "com.sprd.settings.timerpower.AlarmClock";
                sResMap.put(AlarmClock.class.getName(), alarmClockResource);
         /* @} */
				
        final SearchIndexableResource fingerPrintResource = new SearchIndexableResource(
				        Ranking.getRankForClassName(FingerSettings.class.getName()),
				        NO_DATA_RES_ID, FingerSettings.class.getName(),
				        R.drawable.ic_settings_fingerprint);
				        fingerPrintResource.intentAction = "android.intent.action.MAIN";
				        fingerPrintResource.intentTargetPackage = "com.android.settings";
				        fingerPrintResource.intentTargetClass = "com.android.settings.FingerSettings";
				        sResMap.put(FingerSettings.class.getName(), fingerPrintResource);

        /* SPRD: Bug 596063 Add Memory search in Settings @{ */
        sResMap.put(ProcessStatsSummary.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(ProcessStatsSummary.class.getName()),
                        NO_DATA_RES_ID,
                        ProcessStatsSummary.class.getName(),
                        R.drawable.ic_settings_memory));
        /* @} */

        /* SPRD:modify for Bug 641248 can not search prompt & notification in Chinese. @{ */
        sResMap.put(NotificationApps.class.getName(),
                new SearchIndexableResource(
                        Ranking.getRankForClassName(NotificationApps.class.getName()),
                        NO_DATA_RES_ID,
                        NotificationApps.class.getName(),
                        R.drawable.ic_settings_situation));
        /* @} */
    }

    private SearchIndexableResources() {
    }

    public static int size() {
        return sResMap.size();
    }

    public static SearchIndexableResource getResourceByName(String className) {
        return sResMap.get(className);
    }

    public static Collection<SearchIndexableResource> values() {
        return sResMap.values();
    }
}
