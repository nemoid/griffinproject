/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.sim;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.ComponentName;
import android.os.Bundle;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.TelephonyManagerEx;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.settings.R;
import com.sprd.settings.SettingsRelianceHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SimDialogActivity extends Activity {
    private static String TAG = "SimDialogActivity";

    public static String PREFERRED_SIM = "preferred_sim";
    public static String DIALOG_TYPE_KEY = "dialog_type";
    public static final int INVALID_PICK = -1;
    public static final int DATA_PICK = 0;
    public static final int CALLS_PICK = 1;
    public static final int SMS_PICK = 2;
    public static final int PREFERRED_PICK = 3;
    //SPRD: Feature for SIM Languages
    public static final int LANGUAGE_PICK = 4;
    private SubscriptionManager mSubscriptionManager;

    // SPRD: modify by add radioButton on set defult sub id
    private Dialog mSimChooseDialog = null;
    private int mDialogType = INVALID_PICK;
    private StateChangeReciever mStateChangeReciever = new StateChangeReciever();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        this.getBaseContext().registerReceiver(mStateChangeReciever, intentFilter);
        processIntent();
    }

    private void processIntent() {
        final Bundle extras = getIntent().getExtras();
        /** Fix bug 600521 Running intentfuzz-activity crashed  {@ **/
        if(extras == null){
            return;
        }
        /** @} **/
        final int dialogType = extras.getInt(DIALOG_TYPE_KEY, INVALID_PICK);

        switch (dialogType) {
            case DATA_PICK:
            case CALLS_PICK:
            case SMS_PICK:
                mSimChooseDialog = createDialog(this, dialogType);
                mSimChooseDialog.show();
                mDialogType=dialogType;
                break;
            case PREFERRED_PICK:
                displayPreferredDialog(extras.getInt(PREFERRED_SIM));
                break;
                //SPRD: Feature for SIM Languages
            case LANGUAGE_PICK:
                createDialog(this, dialogType).show();
                break;
            default:
                throw new IllegalArgumentException("Invalid dialog type " + dialogType + " sent.");
        }

    }

    /* SPRD: MODIFY FOR BUG 589530:Hot swap, Settings crash @{ */
    @Override
    protected void onResume() {
        super.onResume();
        mSubscriptionManager = SubscriptionManager.from(this);
        mSubscriptionManager.addOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
    }

    private final SubscriptionManager.OnSubscriptionsChangedListener mOnSubscriptionsChangeListener = new SubscriptionManager.OnSubscriptionsChangedListener() {
        @Override
        public void onSubscriptionsChanged() {
            List<SubscriptionInfo> availableSubInfoList = SubscriptionManager
                    .from(getApplicationContext()).getActiveSubscriptionInfoList();
            if (availableSubInfoList == null || availableSubInfoList.size() < 2) {
                finish();
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        mSubscriptionManager.removeOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
    }
    /* @} */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getBaseContext().unregisterReceiver(mStateChangeReciever);
    }

    private class StateChangeReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(mDialogType==DATA_PICK && mSimChooseDialog!=null && mSimChooseDialog.isShowing()){
                finish();
            }
        }
    }

    private void displayPreferredDialog(final int slotId) {
        final Resources res = getResources();
        final Context context = getApplicationContext();
        final SubscriptionInfo sir = SubscriptionManager.from(context)
                .getActiveSubscriptionInfoForSimSlotIndex(slotId);

        if (sir != null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.sim_preferred_title);
            alertDialogBuilder.setMessage(res.getString(
                        R.string.sim_preferred_message, sir.getDisplayName()));

            alertDialogBuilder.setPositiveButton(R.string.yes, new
                    DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    final int subId = sir.getSubscriptionId();
                    PhoneAccountHandle phoneAccountHandle =
                            subscriptionIdToPhoneAccountHandle(subId);
                    setDefaultDataSubId(context, subId);
                    setDefaultSmsSubId(context, subId);
                    setUserSelectedOutgoingPhoneAccount(phoneAccountHandle);
                    finish();
                }
            });
            alertDialogBuilder.setNegativeButton(R.string.no, new
                    DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog,int id) {
                    finish();
                }
            });

            alertDialogBuilder.create().show();
        } else {
            finish();
        }
    }

    private static void setDefaultDataSubId(final Context context, final int subId) {
        /* SPRD: Add interface set default data subId for CMCC case SK_0013 @{*/
        // final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        //subscriptionManager.setDefaultDataSubId(subId);
        final TelephonyManagerEx tm = TelephonyManagerEx.from(context);
        tm.setDefaultDataSubId(subId);
        /* @} */
        Toast.makeText(context, R.string.data_switch_started, Toast.LENGTH_LONG).show();
    }

    /**
     * SPRD: add new feature for data switch on/off
     */
    private void disableDataForOtherSubscriptions(Context context, int subId) {
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        TelephonyManager teleMgr = TelephonyManager.from(context);
        List<SubscriptionInfo> subInfoList = subscriptionManager.getActiveSubscriptionInfoList();
        if (subInfoList != null) {
            for (SubscriptionInfo subInfo : subInfoList) {
                if (subInfo.getSubscriptionId() != subId) {
                    teleMgr.setDataEnabled(subInfo.getSubscriptionId(), false);
                }
            }
        }
    }

    private static void setDefaultSmsSubId(final Context context, final int subId) {
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        subscriptionManager.setDefaultSmsSubId(subId);
    }

    private void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle phoneAccount) {
        final TelecomManager telecomManager = TelecomManager.from(this);
        telecomManager.setUserSelectedOutgoingPhoneAccount(phoneAccount);
    }

    private PhoneAccountHandle subscriptionIdToPhoneAccountHandle(final int subId) {
        final TelecomManager telecomManager = TelecomManager.from(this);
        final TelephonyManager telephonyManager = TelephonyManager.from(this);
        final Iterator<PhoneAccountHandle> phoneAccounts =
                telecomManager.getCallCapablePhoneAccounts().listIterator();

        while (phoneAccounts.hasNext()) {
            final PhoneAccountHandle phoneAccountHandle = phoneAccounts.next();
            final PhoneAccount phoneAccount = telecomManager.getPhoneAccount(phoneAccountHandle);
            if (subId == telephonyManager.getSubIdForPhoneAccount(phoneAccount)) {
                return phoneAccountHandle;
            }
        }

        return null;
    }

    /* SPRD: add for Reliance feature of display data confirm dialog @{ */
    private boolean setDefaultDataReliace(Context context,SubscriptionInfo sir) {
        SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        int currentDataPhoneId = subscriptionManager.getDefaultDataPhoneId();
        if (SettingsRelianceHelper.getInstance(context).displayConfirmDataDialog(context, sir, currentDataPhoneId, this)) {
            return true;
        }
        return false;
    }
    /* @} */

    public Dialog createDialog(final Context context, final int id) {
        dismissSimChooseDialog();
        final ArrayList<String> list = new ArrayList<String>();
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        final List<SubscriptionInfo> subInfoList =
            subscriptionManager.getActiveSubscriptionInfoList();
        final int selectableSubInfoLength = subInfoList == null ? 0 : subInfoList.size();

        Dialog.OnKeyListener keyListener = new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                    KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finish();
                    }
                    return true;
                }
            };

        ArrayList<SubscriptionInfo> callsSubInfoList = new ArrayList<SubscriptionInfo>();
        if (id == CALLS_PICK) {
            final TelecomManager telecomManager = TelecomManager.from(context);
            final TelephonyManager telephonyManager = TelephonyManager.from(context);
            final Iterator<PhoneAccountHandle> phoneAccounts =
                    telecomManager.getCallCapablePhoneAccounts().listIterator();

            list.add(getResources().getString(R.string.sim_calls_ask_first_prefs_title));
            callsSubInfoList.add(null);
            while (phoneAccounts.hasNext()) {
                final PhoneAccount phoneAccount =
                        telecomManager.getPhoneAccount(phoneAccounts.next());
                list.add((String)phoneAccount.getLabel());
                int subId = telephonyManager.getSubIdForPhoneAccount(phoneAccount);
                if (subId != SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
                    final SubscriptionInfo sir = SubscriptionManager.from(context)
                            .getActiveSubscriptionInfo(subId);
                    callsSubInfoList.add(sir);
                } else {
                    callsSubInfoList.add(null);
                }
            }
        } else {
            for (int i = 0; i < selectableSubInfoLength; ++i) {
                final SubscriptionInfo sir = subInfoList.get(i);
                CharSequence displayName = sir.getDisplayName();
                if (displayName == null) {
                    displayName = "";
                }
                list.add(displayName.toString());
            }
        }

        String[] arr = list.toArray(new String[0]);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        ListAdapter adapter = new SelectAccountListAdapter(
                id == CALLS_PICK ? callsSubInfoList : subInfoList,
                builder.getContext(),
                R.layout.select_account_list_item,
                arr, id);

        switch (id) {
            case DATA_PICK:
                builder.setTitle(R.string.select_sim_for_data);
                break;
            case CALLS_PICK:
                builder.setTitle(R.string.select_sim_for_calls);
                break;
            case SMS_PICK:
                builder.setTitle(R.string.sim_card_select_title);
                break;
                //SPRD: Feature for SIM Languages
            case LANGUAGE_PICK:
                builder.setTitle(R.string.sim_card_select_title);
                break;
            default:
                throw new IllegalArgumentException("Invalid dialog type "
                        + id + " in SIM dialog.");
        }

        Dialog dialog = builder.setAdapter(adapter, null).create();
        dialog.setOnKeyListener(keyListener);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
            }
        });

        return dialog;

    }

    /**
     * SPRD: add for set default SMS/Voice/Data sub id by dialog id
     */
    private void setDefaltSubIdByDialogId(
            final Context context, int dialogId, int chooseId, List<SubscriptionInfo> subInfoList) {
        final SubscriptionInfo sir;

        switch (dialogId) {
            case DATA_PICK:
                sir = subInfoList.get(chooseId);
                // SPRD: MODIFY FOR BUG 600094
                if (sir.getSubscriptionId() != mSubscriptionManager
                        .getDefaultDataSubscriptionId()) {
                    if (setDefaultDataReliace(context, sir)) return;
                    setDefaultDataSubId(context, sir.getSubscriptionId());
                }
                break;
            case CALLS_PICK:
                final TelecomManager telecomManager = TelecomManager.from(context);
                final List<PhoneAccountHandle> phoneAccountsList = telecomManager
                        .getCallCapablePhoneAccounts();
                setUserSelectedOutgoingPhoneAccount(
                        chooseId < 1 ? null : phoneAccountsList.get(chooseId - 1));
                break;
            case SMS_PICK:
                sir = subInfoList.get(chooseId);
                setDefaultSmsSubId(context, sir.getSubscriptionId());
                break;
            // SPRD: Feature for SIM Languages
            case LANGUAGE_PICK:
                sir = subInfoList.get(chooseId);
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setComponent(new ComponentName("com.sprd.simlanguages",
                        "com.sprd.simlanguages.SimLangListActivity"));
                int phoneId = sir.getSimSlotIndex();
                intent.putExtra("sub_id", (SubscriptionManager.getSubId(phoneId))[0]);
                context.startActivity(intent);
                break;
            default:
                throw new IllegalArgumentException("Invalid dialog type "
                        + dialogId + " in SIM dialog.");
        }

        finish();

    }

    private void dismissSimChooseDialog() {
        if (mSimChooseDialog != null && mSimChooseDialog.isShowing()) {
            mSimChooseDialog.dismiss();
        }
    }

    private class SelectAccountListAdapter extends ArrayAdapter<String> {
        private Context mContext;
        private int mResId;
        private int mDialogId;
        private final float OPACITY = 0.54f;
        private List<SubscriptionInfo> mSubInfoList;

        public SelectAccountListAdapter(List<SubscriptionInfo> subInfoList,
                Context context, int resource, String[] arr, int dialogId) {
            super(context, resource, arr);
            mContext = context;
            mResId = resource;
            mDialogId = dialogId;
            mSubInfoList = subInfoList;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView;
            final ViewHolder holder;
            SubscriptionManager subscriptionManager = SubscriptionManager.from(mContext);
            TelecomManager telecomManager = TelecomManager.from(mContext);

            if (convertView == null) {
                // Cache views for faster scrolling
                rowView = inflater.inflate(mResId, null);
                holder = new ViewHolder();
                holder.title = (TextView) rowView.findViewById(R.id.title);
                holder.summary = (TextView) rowView.findViewById(R.id.summary);
                holder.icon = (ImageView) rowView.findViewById(R.id.icon);
                holder.defaultSubscription = (RadioButton) rowView
                        .findViewById(R.id.default_subscription_off);
                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (ViewHolder) rowView.getTag();
            }

            final SubscriptionInfo sir = mSubInfoList.get(position);
            PhoneAccountHandle phoneAccount = telecomManager.getUserSelectedOutgoingPhoneAccount();
            if (sir == null) {
                holder.title.setText(getItem(position));
                holder.summary.setText("");
                holder.icon.setImageDrawable(getResources()
                        .getDrawable(R.drawable.ic_live_help));
                holder.icon.setAlpha(OPACITY);
                /* SPRD: modify by add radioButton on set defult sub id @{ */
                holder.defaultSubscription.setChecked(false);
                switch (mDialogId) {
                    case DATA_PICK:
                        break;
                    case CALLS_PICK:
                        holder.defaultSubscription.setChecked(0 == position
                                &&  phoneAccount == null);
                        break;
                    case SMS_PICK:
                        holder.defaultSubscription.setChecked(0 == position
                                && subscriptionManager.getDefaultSmsSubscriptionId()
                                == SubscriptionManager.MAX_SUBSCRIPTION_ID_VALUE);
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid dialog type "
                        + mDialogId + " in SIM dialog.");
                        }
                        /* @} */
            } else {
                holder.title.setText(sir.getDisplayName());
                holder.summary.setText(sir.getNumber());
                holder.icon.setImageBitmap(sir.createIconBitmap(mContext));
                /* SPRD: modify by add radioButton on set defult sub id @{ */
               switch (mDialogId) {
                   case DATA_PICK:
                       holder.defaultSubscription.setChecked(
                               subscriptionManager.getDefaultDataSubscriptionId() == sir.getSubscriptionId());
                       break;
                   case CALLS_PICK:
                       holder.defaultSubscription.setChecked(phoneAccount != null
                               && subscriptionManager.getDefaultVoiceSubscriptionId() == sir.getSubscriptionId());
                       break;
                   case SMS_PICK:
                       holder.defaultSubscription.setChecked(
                               subscriptionManager.getDefaultSmsSubscriptionId() == sir.getSubscriptionId());
                       break;
                   default:
                       throw new IllegalArgumentException("Invalid dialog type " + mDialogId + " in SIM dialog.");
               }
           }
           final boolean isSubIdChecked = holder.defaultSubscription.isChecked();
           rowView.setOnClickListener(new OnClickListener() {

               @Override
               public void onClick(View v) {
                   Log.d(TAG, "onCheckedChanged isSubIdChecked = " + isSubIdChecked);
                   if (!isSubIdChecked) {
                       setDefaltSubIdByDialogId(mContext, mDialogId, position, mSubInfoList);
                   } else {
                       finish();
                   }
               }
           });
            holder.defaultSubscription.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onCheckedChanged isSubIdChecked = " + isSubIdChecked);
                    if (!isSubIdChecked) {
                        setDefaltSubIdByDialogId(mContext, mDialogId, position, mSubInfoList);
                    } else {
                        finish();
                    }
                }
            });
           /* @} */
            return rowView;
        }

        private class ViewHolder {
            TextView title;
            TextView summary;
            ImageView icon;
            // SPRD: modify by add radioButton on set defult sub id
            RadioButton defaultSubscription;
        }
    }
}
