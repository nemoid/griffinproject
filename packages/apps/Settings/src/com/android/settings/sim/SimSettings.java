/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.sim;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.BitmapDrawable;
import android.os.BaseBundle;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.SearchIndexableResource;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.provider.Settings.System;
import android.provider.SettingsEx;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceViewHolder;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.TelephonyManagerEx;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.internal.telephony.TelephonyProperties;
import com.android.settings.R;
import com.android.settings.RestrictedSettingsFragment;
import com.android.settings.Utils;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settingslib.WirelessUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sprd.settings.SettingsOperatorHelper;
import com.sprd.settings.SettingsRelianceHelper;

public class SimSettings extends RestrictedSettingsFragment implements Indexable {
    private static final String TAG = "SimSettings";
    private static final boolean DBG = false;

    private static final String DISALLOW_CONFIG_SIM = "no_config_sim";
    private static final String SIM_CARD_CATEGORY = "sim_cards";
    private static final String KEY_CELLULAR_DATA = "sim_cellular_data";
    private static final String KEY_CALLS = "sim_calls";
    private static final String KEY_SMS = "sim_sms";
    public static final String EXTRA_SLOT_ID = "slot_id";
    /* SPRD: new featrue:Smart Dual SIM @{ */
    private static final String KEY_ACTIVITIES = "sim_activities";
    private static final String KEY_SMART_DUAL_SIM = "smart_dual_sim";
    /* @} */
    private static final String STANDBY_DIALOG_TAG = "standby_dialog";

    /**
     * By UX design we use only one Subscription Information(SubInfo) record per SIM slot.
     * mAvalableSubInfos is the list of SubInfos we present to the user.
     * mSubInfoList is the list of all SubInfos.
     * mSelectableSubInfos is the list of SubInfos that a user can select for data, calls, and SMS.
     */
    // private List<SubscriptionInfo> mAvailableSubInfos = null;
    // SPRD: modify for bug497338
    private List<SubscriptionInfo> mAvailableSubInfoList = null;
    private List<SubscriptionInfo> mSubInfoList = null;
    private List<SubscriptionInfo> mSelectableSubInfos = null;
    private PreferenceScreen mSimCards = null;
    // SPRD: add new feature for data switch on/off
    private DataPreference mDataPreference;
    private SubscriptionManager mSubscriptionManager;
    private int mNumSlots;
    private Context mContext;
    private TelephonyManager mTelephonyManager = null;

    private static int mPhoneCount = TelephonyManager.getDefault().getPhoneCount();
    private int[] mCallState = new int[mPhoneCount];
    private PhoneStateListener[] mPhoneStateListener = new PhoneStateListener[mPhoneCount];
    /* SPRD: new featrue:Smart Dual SIM @{ */
    private  PreferenceCategory mSimPreferenceCatergory;
    private Preference mSmartDualSim = null;
    /* @} */
    private boolean[] mIsChecked = new boolean[mPhoneCount];
    private static boolean[] mSwitchHasChanged = new boolean[mPhoneCount];
    // SPRD: Modify for bug 603907
    private DialogFragment mAlertDialogFragment = null;
    private static TelephonyManagerEx mTmEx;
    private static String PROPERTY_CMCC_CARD_PREFER = "persist.radio.network.unable";

    public SimSettings() {
        super(DISALLOW_CONFIG_SIM);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.SIM;
    }

    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        mContext = getActivity();

        mSubscriptionManager = SubscriptionManager.from(getActivity());
        mTmEx = TelephonyManagerEx.from(mContext);

        mTelephonyManager =
                (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        addPreferencesFromResource(R.xml.sim_settings);

        mNumSlots = mTelephonyManager.getSimCount();
        mSimCards = (PreferenceScreen)findPreference(SIM_CARD_CATEGORY);
        // mAvailableSubInfos = new ArrayList<SubscriptionInfo>(mNumSlots);
        // SPRD: modify for bug497338
        mAvailableSubInfoList = getActiveSubInfoList();
        mSelectableSubInfos = new ArrayList<SubscriptionInfo>();

        /* SPRD: add new feature for data switch on/off @{ */
        PreferenceCategory simPreferenceCatergory = (PreferenceCategory) findPreference(
                KEY_ACTIVITIES);
        mDataPreference = (DataPreference) new DataPreference(getActivity());
        mDataPreference.setOrder(0);
        mDataPreference.setKey(KEY_CELLULAR_DATA);
        /* @} */

        SimSelectNotification.cancelNotification(getActivity());
        /* SPRD: new featrue:Smart Dual SIM @{ */
        mSimPreferenceCatergory = (PreferenceCategory) findPreference(KEY_ACTIVITIES);
        mSmartDualSim = (Preference)findPreference(KEY_SMART_DUAL_SIM);
        mSimPreferenceCatergory.removePreference(mSmartDualSim);
        refreshSmartDualSimStatus();
        /* @} */
    }

    private boolean isRadioBusy() {
        return Settings.Global.getInt(getContentResolver(), SettingsEx.GlobalEx.RADIO_BUSY, 0) == 1;
    }

    private ContentObserver mRadioBusyObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            if (getActivity() != null) {
                updateAllOptions();
            }
        }
    };

    private ContentObserver mAirplaneModeObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            getPreferenceScreen().setEnabled(!WirelessUtils.isAirplaneModeOn(mContext));
            // SPRD: Modify for bug 603907,Click to disable the SIM card, the pop-up "confirm" POP box, screen switch, POP box disappeared
            if (WirelessUtils.isAirplaneModeOn(mContext)) {
                if (mAlertDialogFragment != null) {
                    mAlertDialogFragment.dismissAllowingStateLoss();
                    mAlertDialogFragment = null;
                }
            }
        }
    };

    private final SubscriptionManager.OnSubscriptionsChangedListener mOnSubscriptionsChangeListener
            = new SubscriptionManager.OnSubscriptionsChangedListener() {
        @Override
        public void onSubscriptionsChanged() {
            if (DBG) log("onSubscriptionsChanged:");
            mAvailableSubInfoList = getActiveSubInfoList();
            updateSubscriptions();
        }
    };

    private void updateSubscriptions() {
        mSubInfoList = mSubscriptionManager.getActiveSubscriptionInfoList();
        for (int i = 0; i < mNumSlots; ++i) {
            Preference pref = mSimCards.findPreference("sim" + i);
            if (pref instanceof SimPreference) {
                mSimCards.removePreference(pref);
            }
        }
        // SPRD: modify for bug497338
        // mAvailableSubInfos.clear();
        mSelectableSubInfos.clear();

        for (int i = 0; i < mNumSlots; ++i) {
            final SubscriptionInfo sir = mSubscriptionManager
                    .getActiveSubscriptionInfoForSimSlotIndex(i);
            SimPreference simPreference = new SimPreference(getPrefContext(), sir, i);
            simPreference.setOrder(i-mNumSlots);
            mSimCards.addPreference(simPreference);
//            mAvailableSubInfos.add(sir);
            if (sir != null) {
                mSelectableSubInfos.add(sir);
            }
        }
        updateAllOptions();
    }

    private void updateAllOptions() {
        updateSimSlotValues();
        updateActivitesCategory();
    }

    private void updateSimSlotValues() {
        final int prefSize = mSimCards.getPreferenceCount();
        for (int i = 0; i < prefSize; ++i) {
            Preference pref = mSimCards.getPreference(i);
            if (pref instanceof SimPreference) {
                ((SimPreference)pref).update();
            }
        }
    }

    private void updateActivitesCategory() {
        updateCellularDataValues();
        updateCallValues();
        updateSmsValues();
    }

    private boolean isPhoneInCall() {
        for (int i = 0; i < mPhoneCount; i++) {
            if (TelephonyManager.from(mContext)
                    .getCallStateForSlot(i) != TelephonyManager.CALL_STATE_IDLE) {
                return true;
            }
        }
        return false;
    }

    private void updateSmsValues() {
        final Preference simPref = findPreference(KEY_SMS);
        final SubscriptionInfo sir = mSubscriptionManager.getDefaultSmsSubscriptionInfo();
        simPref.setTitle(R.string.sms_messages_title);
        if (DBG) log("[updateSmsValues] mSubInfoList=" + mSubInfoList);

        if (sir != null) {
            simPref.setSummary(sir.getDisplayName());
        } else if (sir == null) {
            simPref.setSummary(R.string.sim_selection_required_pref);
        }
        simPref.setEnabled(mAvailableSubInfoList.size() > 1);
    }

    /* SPRD: add new feature for data switch on/off @{ */
    private void updateCellularDataValues() {
        PreferenceCategory simPreferenceCatergory = (PreferenceCategory) findPreference(
                KEY_ACTIVITIES);
        if (SettingsOperatorHelper.getInstance(mContext).isNeedSetDataEnable(mContext)) {
            simPreferenceCatergory.removePreference(mDataPreference);
        } else {
            simPreferenceCatergory.addPreference(mDataPreference);
            mDataPreference.update();
        }
    }
    /* @} */

    private void updateCallValues() {
        final Preference simPref = findPreference(KEY_CALLS);
        final TelecomManager telecomManager = TelecomManager.from(mContext);
        final PhoneAccountHandle phoneAccount =
            telecomManager.getUserSelectedOutgoingPhoneAccount();
        final List<PhoneAccountHandle> allPhoneAccounts =
            telecomManager.getCallCapablePhoneAccounts();

        //SPRD: modify for bug588658
        PhoneAccount pa = telecomManager.getPhoneAccount(phoneAccount);
        /* SPRD: modify for bug494106 @{ */
        final boolean isPhoneAccountAvialable = (phoneAccount != null) && (pa != null);
        simPref.setTitle(R.string.calls_title);
        simPref.setSummary(!isPhoneAccountAvialable
                ? mContext.getResources().getString(R.string.sim_calls_ask_first_prefs_title)
                : (String) pa.getLabel());
        simPref.setEnabled(mAvailableSubInfoList.size() > 1);
        // SPRD: new featrue:Smart Dual SIM
        refreshSmartDualSimStatus();
    }

    @Override
    public void onResume() {
        super.onResume();
        getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(SettingsEx.GlobalEx.RADIO_BUSY), true,
                mRadioBusyObserver, UserHandle.USER_OWNER);
        /*
         *SPRD: add new feature for data switch on/off
         *AndroidM is to modify the original Google code, the data switch to save the state to MOBILE_DATA
         *AndroidN data switch status will be stored in MOBILE_DATA+MAX_SUBSCRIPTION_ID_VALUE.
         */
        getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(Settings.Global.MOBILE_DATA +SubscriptionManager.MAX_SUBSCRIPTION_ID_VALUE), true,
                mMobileDataObserver,UserHandle.USER_OWNER);
        mSubscriptionManager.addOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
        // SPRD: new featrue:Smart Dual SIM
        refreshSmartDualSimStatus();
        // SPRD: MODIFY FOR BUG 607226
        updateSubscriptions();
        final TelephonyManager tm =
                (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (mSelectableSubInfos.size() > 0) {
            Log.d(TAG, "Register for call state change");
            for (int i = 0; i < mSelectableSubInfos.size(); i++) {
                int subId = mSelectableSubInfos.get(i).getSubscriptionId();
                tm.listen(getPhoneStateListener(i, subId),
                        PhoneStateListener.LISTEN_CALL_STATE);
            }
        }
        updatePreferencesState();
        mContext.getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(Settings.Global.AIRPLANE_MODE_ON), true,
                mAirplaneModeObserver);
    }

    @Override
    public void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(mRadioBusyObserver);
        // SPRD: add new feature for data switch on/off
        getContentResolver().unregisterContentObserver(mMobileDataObserver);
        mSubscriptionManager.removeOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
        final TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        for (int i = 0; i < mPhoneCount; i++) {
            if (mPhoneStateListener[i] != null) {
                tm.listen(mPhoneStateListener[i], PhoneStateListener.LISTEN_NONE);
                mPhoneStateListener[i] = null;
            }
        }
        mContext.getContentResolver().unregisterContentObserver(mAirplaneModeObserver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private PhoneStateListener getPhoneStateListener(int phoneId, int subId) {
        // Disable Sim selection for Data when voice call is going on as changing the default data
        // sim causes a modem reset currently and call gets disconnected
        // ToDo : Add subtext on disabled preference to let user know that default data sim cannot
        // be changed while call is going on
        final int i = phoneId;
        mPhoneStateListener[phoneId]  = new PhoneStateListener(subId) {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (DBG) log("PhoneStateListener.onCallStateChanged: state=" + state);
                updateCellularDataValues();
                updateSimSlotValues();
            }
        };
        return mPhoneStateListener[phoneId];
    }

    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        final Context context = mContext;
        Intent intent = new Intent(context, SimDialogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        if (preference instanceof SimPreference) {
         // SPRD: modify for Re edit the SIM card interface
            SimFragmentDialog.show(SimSettings.this, ((SimPreference) preference).getSlotId());
        } else if (preference instanceof DataPreference) {
            intent.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, SimDialogActivity.DATA_PICK);
            context.startActivity(intent);
        } else if (findPreference(KEY_CALLS) == preference) {
            intent.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, SimDialogActivity.CALLS_PICK);
            context.startActivity(intent);
        } else if (findPreference(KEY_SMS) == preference) {
            intent.putExtra(SimDialogActivity.DIALOG_TYPE_KEY, SimDialogActivity.SMS_PICK);
            context.startActivity(intent);
        /* SPRD: new featrue:Smart Dual SIM @{ */
        } else if (findPreference(KEY_SMART_DUAL_SIM) == preference) {
            return false;
        }
        /* @} */

        return true;
    }

    /* SPRD: add new feature for data switch on/off @{ */
    public class DataPreference extends Preference {
        Context mContext;
        Switch mDataSwitch;

        public DataPreference(Context context) {
            super(context);
            mContext = context;
            setLayoutResource(R.layout.sim_preference_ex);
            update();
        }

        @Override
        public void onBindViewHolder(PreferenceViewHolder holder) {
            super.onBindViewHolder(holder);
            mDataSwitch = (Switch) holder.findViewById(R.id.universal_switch);
            mDataSwitch.setVisibility(View.VISIBLE);
            final int dataSubId = SubscriptionManager.getDefaultDataSubscriptionId();
            mDataSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    TelephonyManager tm = TelephonyManager.from(mContext);
                    boolean isDataEnable = mTelephonyManager.getDataEnabled(dataSubId);
                    if (isDataEnable != isChecked) {
                        mTelephonyManager.setDataEnabled(dataSubId, isChecked);
                    }
                }
            });
            updateDataSwitch(dataSubId);
        }

        public void updateDataSwitch(int subId) {
            Log.d(TAG, "mDataSwitch updateDataSwitch subId" + subId);
            if (mDataSwitch != null) {
                boolean isDataEnable = mTelephonyManager.getDataEnabled(subId);
                int phoneId = SubscriptionManager.getPhoneId(subId);
                mDataSwitch.setChecked(isDataEnable);
                boolean canSetDataEnable = (SubscriptionManager
                        .getSimStateForSlotIdx(phoneId) == TelephonyManager.SIM_STATE_READY)
                        && !WirelessUtils.isAirplaneModeOn(mContext) && !isPhoneInCall();
                mDataSwitch.setEnabled(canSetDataEnable);
                if (SettingsOperatorHelper.getInstance(mContext).removeDataSwitch()) {
                    mDataSwitch.setVisibility(View.GONE);
                }
            }
        }

        public void update() {
            final SubscriptionInfo sir = mSubscriptionManager.getDefaultDataSubscriptionInfo();
            final boolean dataEabled = mTelephonyManager
                    .getDataEnabled(mSubscriptionManager.getDefaultDataSubscriptionId());
            setTitle(R.string.cellular_data_title);
            if (DBG)
                log("[update DataPreference] mSubInfoList=" + mSubInfoList);

            if (sir != null) {
                setSummary(sir.getDisplayName().toString().trim().isEmpty()
                        ? "SIM" + (sir.getSimSlotIndex() + 1) : sir.getDisplayName());
                updateDataSwitch(sir.getSubscriptionId());
            } else if (sir == null) {
                setSummary(R.string.sim_selection_required_pref);
            }
            if (mSubscriptionManager.getActiveSubscriptionInfoCount() <= 1) {
                setEnabled(false);
            } else {
                //SPRD:add for bug 588861
                setEnabled(getActiveSubInfoList().size() > 1
                        && !WirelessUtils.isAirplaneModeOn(mContext) && !isPhoneInCall());
            }
            //SPRD:modify bug 624568
            setShouldDisableView(getActiveSubInfoList().size() == 0);
        }
    }

    private List<SubscriptionInfo> getActiveSubInfoList() {
        if (mSubscriptionManager == null) {
            return new ArrayList<SubscriptionInfo>();
        }
        /* @} */
        List<SubscriptionInfo> availableSubInfoList = mSubscriptionManager
                .getActiveSubscriptionInfoList();
        if (availableSubInfoList == null) {
            return new ArrayList<SubscriptionInfo>();
        }
        Iterator<SubscriptionInfo> iterator = availableSubInfoList.iterator();
        while (iterator.hasNext()) {
            SubscriptionInfo subInfo = iterator.next();
            int phoneId = subInfo.getSimSlotIndex();
            boolean isSimReady = mTelephonyManager
                    .getSimState(phoneId) == TelephonyManager.SIM_STATE_READY;
            if (!isSimReady) {
                iterator.remove();
            }
        }
        return availableSubInfoList;
    }

    /* SPRD: Modify for bug 603907,Click to disable the SIM card, the pop-up "confirm" POP box, screen switch, POP box disappeared @{ */
    private void showStandbyAlertDialog(final int phoneId, final boolean onOff) {
        StandbyAlertDialogFragment.show(SimSettings.this, phoneId, onOff);
    }

    private void resetAlertDialogFragment(DialogFragment dialogFragment) {
        mAlertDialogFragment = dialogFragment;
    }

    private void updatePreferencesState() {
        if (System.getInt(mContext.getContentResolver(), Global.AIRPLANE_MODE_ON, 0) != 0) {
            if(mAlertDialogFragment != null) {
                mAlertDialogFragment.dismissAllowingStateLoss();
                mAlertDialogFragment = null;
            }
        }
        getPreferenceScreen().setEnabled(
                !WirelessUtils.isAirplaneModeOn(mContext));
    }

    public static class StandbyAlertDialogFragment extends DialogFragment {
        private static final String SAVE_PHONE_ID = "phoneId";
        private static final String SAVE_ON_OFF = "onOff";
        private int mPhoneId;
        private boolean mOnOff;

        public static void show(SimSettings parent, int phoneId, boolean onOff) {
            if (!parent.isAdded())
                return;
            StandbyAlertDialogFragment dialog = new StandbyAlertDialogFragment();
            dialog.mPhoneId = phoneId;
            dialog.mOnOff = onOff;
            dialog.setTargetFragment(parent, 0);
            dialog.show(parent.getFragmentManager(), STANDBY_DIALOG_TAG);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            if (savedInstanceState != null) {
                mPhoneId = savedInstanceState.getInt(SAVE_PHONE_ID);
                mOnOff = savedInstanceState.getBoolean(SAVE_ON_OFF);
            }
            final SimSettings sft = (SimSettings) getTargetFragment();
            if (sft == null) {
                Log.d(TAG, "StandbyAlertDialogFragment getTargetFragment failure!!!");
                return super.onCreateDialog(savedInstanceState);
            }
            sft.resetAlertDialogFragment(this);
            /* @} */
            final TelephonyManager telephonyManager = TelephonyManager.from(getActivity());
            final SubscriptionManager subscriptionManager = SubscriptionManager.from(getActivity());
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.proxy_error);
            builder.setMessage(R.string.stand_by_set_changed_prompt);
            builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (mTmEx.isSimEnabled(mPhoneId) != mOnOff) {
                        mTmEx.setSimEnabled(mPhoneId, mOnOff);
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, null);
            return builder.create();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);
            mSwitchHasChanged[mPhoneId] = false;
            if (getTargetFragment() != null) {
                ((SimSettings) getTargetFragment()).updateSimSlotValues();
            }
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt(SAVE_PHONE_ID, mPhoneId);
            outState.putBoolean(SAVE_ON_OFF, mOnOff);
        }
    }
    /* @} */

    private class SimPreference extends Preference {
        private SubscriptionInfo mSubInfoRecord;
        private int mSlotId;
        Context mContext;

        /* SPRD: Modify SimPreference to add feature: Enable/Disable SIM card. @{ */
        private Switch mSwitch;
        private TelephonyManager mTm;


        public SimPreference(Context context, SubscriptionInfo subInfoRecord, int slotId) {
            super(context);

            setLayoutResource(R.layout.sim_preference);

            mContext = context;
            mTm = TelephonyManager.from(mContext);
            mSubInfoRecord = subInfoRecord;
            mSlotId = slotId;
            setKey("sim" + mSlotId);
            update();
        }

        @Override
        public void onBindViewHolder(PreferenceViewHolder holder) {
            super.onBindViewHolder(holder);
            mSwitch = (Switch) holder.findViewById(R.id.universal_switch);
            mSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // SPRD: modify for Bug618312
                    if (isPhoneInCall()) return;
                    boolean simEnabled = mTmEx.isSimEnabled(mSlotId);
                    /* SPRD:MODIFY FOR BUG601836 @{*/
                    if (simEnabled != isChecked) {
                        mIsChecked[mSlotId] = isChecked;
                        mSwitchHasChanged[mSlotId] = true;
                        showStandbyAlertDialog(mSlotId, isChecked);
                    }
                    /* @}*/
                }
            });
            updateSwitchState();
        }

        private void updateSwitchState() {
            if (mSwitch != null) {
                boolean simEnabled = mTmEx.isSimEnabled(mSlotId);
                /* SPRD:MODIFY FOR BUG601836 @{*/
                if (mSwitchHasChanged[mSlotId]) {
                    mSwitch.setChecked(mIsChecked[mSlotId]);
                    mSwitchHasChanged[mSlotId] = false;
                } else {
                    mSwitch.setChecked(simEnabled);
                }
                /* @}*/
                boolean canSetSimEnabled = (!simEnabled
                        || mTm.getSimState(mSlotId) == TelephonyManager.SIM_STATE_READY)
                        && !WirelessUtils.isAirplaneModeOn(mContext) && !isRadioBusy()
                        && !isPhoneInCall();
                mSwitch.setEnabled(canSetSimEnabled);
                mSwitch.setVisibility(View.VISIBLE);
            }
        }


        public void update() {
            // SPRD: modify for Bug494140
            if (!isAdded()) {
                return;
            }

            final Resources res = mContext.getResources();

            setTitle(String.format(mContext.getResources()
                    .getString(R.string.sim_editor_title), (mSlotId + 1)));
            if (mSubInfoRecord != null && mContext != null) {
                /* CMCC case:BC-DSDS_0061,Need to clearly distinguish between the main and gsm card@{*/
                String subInfoMessage = (SettingsOperatorHelper.getInstance(mContext).
                        isSimSlotForPrimaryCard(mSubInfoRecord)
                        ? mContext.getResources().getString(R.string.main_card_slot)
                        : mContext.getResources().getString(R.string.gsm_card_slot)) + mSubInfoRecord.getDisplayName();
                if (TextUtils.isEmpty(getPhoneNumber(mSubInfoRecord))) {
                    setSummary(subInfoMessage);
                } else {
                    setSummary(subInfoMessage + " - "
                            + PhoneNumberUtils.createTtsSpannable(getPhoneNumber(mSubInfoRecord)));
                }
                /* @} */
                //SPRD:add for bug 588862
                setEnabled(mTm.getSimState(mSlotId) == TelephonyManager.SIM_STATE_READY
                        && !isRadioBusy());
                setIcon(new BitmapDrawable(res, (mSubInfoRecord.createIconBitmap(mContext))));
            } else {
                setSummary(R.string.sim_slot_empty);
                setFragment(null);
                setEnabled(false);
            }

            updateSwitchState();
        }
        /* @} */

        private int getSlotId() {
            return mSlotId;
        }
    }

    private ContentObserver mMobileDataObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            updateCellularDataValues();
        }

    };
    /* @} */

    // Returns the line1Number. Line1number should always be read from TelephonyManager since it can
    // be overridden for display purposes.
    private String getPhoneNumber(SubscriptionInfo info) {
        final TelephonyManager tm =
            (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getLine1Number(info.getSubscriptionId());
    }

    private void log(String s) {
        Log.d(TAG, s);
    }

    /**
     * For search
     */
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
            new BaseSearchIndexProvider() {
                @Override
                public List<SearchIndexableResource> getXmlResourcesToIndex(Context context,
                        boolean enabled) {
                    ArrayList<SearchIndexableResource> result =
                            new ArrayList<SearchIndexableResource>();

                    if (Utils.showSimCardTile(context)) {
                        SearchIndexableResource sir = new SearchIndexableResource(context);
                        sir.xmlResId = R.xml.sim_settings;
                        result.add(sir);
                    }
                    return result;
                }

                /* SPRD: 597416 Smart Dual Sim should not be searchable when not in Reliance version@{*/
                @Override
                public List<String> getNonIndexableKeys(Context context) {
                    final List<String> keys = new ArrayList<String>();
                    if (!SettingsRelianceHelper.getInstance(context).shouldShowSmartDualSimSOption()) {
                        keys.add(KEY_SMART_DUAL_SIM);
                    }
                    return keys;
                }
                /* @} */
            };

    private boolean isCallStateIdle() {
        boolean callStateIdle = true;
        for (int i = 0; i < mCallState.length; i++) {
            if (TelephonyManager.CALL_STATE_IDLE != mCallState[i]) {
                callStateIdle = false;
            }
        }
        Log.d(TAG, "isCallStateIdle " + callStateIdle);
        return callStateIdle;
    }

    /* SPRD: new featrue:Smart Dual SIM @{ */
    private void refreshSmartDualSimStatus() {
        if (mAvailableSubInfoList != null
                && mAvailableSubInfoList.size() > 1
                && SettingsRelianceHelper.getInstance(mContext).shouldShowSmartDualSimSOption()) {
            log("add KEY_SMART_DUAL_SIM");
            mSimPreferenceCatergory.addPreference(mSmartDualSim);
        } else {
            log("removePreference KEY_SMART_DUAL_SIM");
            mSimPreferenceCatergory.removePreference(mSmartDualSim);
        }
    }
    /* @} */
}
