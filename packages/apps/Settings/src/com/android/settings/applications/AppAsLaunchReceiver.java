/*
 * Copyright ? 2017 Spreadtrum Communications Inc.
 */
package com.android.settings.applications;

import android.app.ActivityManagerNativeEx;
import android.app.INotificationManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import com.android.settings.R;
import com.sprd.settings.AppAsLaunchToast;

public class AppAsLaunchReceiver extends BroadcastReceiver {
    private static final String TAG = "AppAsLaunchReceiver";
    private static final String APPASLAUNCHRECEIVER= "android.settings.APPASLAUNCHRECEIVER";
    private static final String ENABLE_TAG = "enable";
    private static final String PACKAGE_NAME_TAG = "packagename";
    private static final String LABEL_NAME_TAG = "labelName";
    private static final String NOTIFICATION_ID_TAG = "notiId";
    private final static String APP_AS_LAUNCH_ENABLE = "appaslaunch_enable_";
    private int notificationId = 278;
    @Override
    public void onReceive(Context context, Intent intent) {
        if (APPASLAUNCHRECEIVER.equals(intent.getAction())) {
            boolean enable = intent.getBooleanExtra(ENABLE_TAG,true);
            String packageName = intent.getStringExtra(PACKAGE_NAME_TAG);
            String labelName = intent.getStringExtra(LABEL_NAME_TAG);
            int notiId = intent.getIntExtra(NOTIFICATION_ID_TAG, notificationId);
            // set packageName app enable or disable
            setAppAsEnable(enable ? 1 : 0, packageName);
            // save data if receiving click checkbox notifications.
            Settings.System.putInt(context.getContentResolver(),
                    APP_AS_LAUNCH_ENABLE + packageName, enable ? 1 : 0);
            String text = context.getString(R.string.app_as_close_success,
                    enable ? context
                    .getString(com.android.internal.R.string.notification_open)
                    : context
                    .getString(com.android.internal.R.string.notification_close),
                    labelName);
            // close notification
            try {
                INotificationManager inm = NotificationManager.getService();
                inm.cancelNotificationWithTag(
                        "android", null, notiId, UserHandle.myUserId());
            } catch (RuntimeException e) {
                Log.w(TAG,
                    "Error cancelling notification for heavy-weight app", e);
            } catch (RemoteException e) {
            }
            AppAsLaunchToast.makeText(context, text,
                    AppAsLaunchToast.LENGTH_SHORT).show();
        }
    }

    private void setAppAsEnable(int flag, String packageName){
        try{
            ActivityManagerNativeEx.getDefault()
                    .setAppAsEnable(flag, packageName, null);
        } catch(RemoteException e) {
            Log.e(TAG,"Oops, setAppAsEnable Get RemoteException");
        }
    }
}
