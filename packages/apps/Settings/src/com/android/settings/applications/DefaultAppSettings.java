/**
 * SPRD: Created by Spreadst, for feature 535093, support default app settings.
 */

package com.android.settings.applications;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.android.settings.R;

public class DefaultAppSettings extends PreferenceActivity {
    static final String TAG = "DefaultAppSettings";
    static final int REQUESTING_UNINSTALL = 10;

    private static final String KEY_LAUNCHER = "launcher";
    private static final String KEY_CALL = "call";
    private static final String KEY_MESSAGING = "messaging";
    private static final String KEY_CAMERA = "camera";
    private static final String KEY_GALLERY = "gallery";
    private static final String KEY_MUSIC = "music";
    private static final String KEY_EMAIL = "email";

    PreferenceGroup mPrefGroup;

    PackageManager mPm;
    ComponentName[] mAppComponentSet;
    ArrayList<DefaultAppSettingPreference> mPrefs;
    DefaultAppSettingPreference mCurrentApp = null;

    private String appName;
    private DefaultAppSettingsUtils mUtils;

    private static final HashMap<String, Integer> appNameToIDMap = new HashMap<String, Integer>();
    static {
        appNameToIDMap.put(KEY_LAUNCHER, R.string.default_app_settings_launcher);
        appNameToIDMap.put(KEY_CALL, R.string.default_app_settings_call);
        appNameToIDMap.put(KEY_MESSAGING, R.string.default_app_settings_messaging);
        appNameToIDMap.put(KEY_CAMERA, R.string.default_app_settings_camera);
        appNameToIDMap.put(KEY_GALLERY, R.string.default_app_settings_gallery);
        appNameToIDMap.put(KEY_MUSIC, R.string.default_app_settings_music);
        appNameToIDMap.put(KEY_EMAIL, R.string.default_app_settings_email);
    }

    OnClickListener mAppClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = (Integer)v.getTag();
            /* SPRD:modify for #611205 @{ */
            if(null != mPrefs && index < mPrefs.size()) {
                DefaultAppSettingPreference pref = mPrefs.get(index);
                if (!pref.isChecked) {
                    makeCurrentApp(pref);
                }
            }
            /* @} */
        }
    };

    void makeCurrentApp(DefaultAppSettingPreference newApp) {
        if (mCurrentApp != null) {
            mCurrentApp.setChecked(false);
        }
        newApp.setChecked(true);
        mCurrentApp = newApp;

        mUtils.configurePreferredActivity(appName, newApp.activityName, mAppComponentSet);
    }

    private void buildAppActivitiesList() {
        mUtils.getAppList(appName);

        mCurrentApp = null;
        mPrefGroup.removeAll();
        mPrefs = new ArrayList<DefaultAppSettingPreference>();
        mAppComponentSet = new ComponentName[mUtils.appActivities.size()];
        int prefIndex = 0;
        for (int i = 0; i < mUtils.appActivities.size(); i++) {
            final ResolveInfo candidate = mUtils.appActivities.get(i);
            final ActivityInfo info = candidate.activityInfo;
            ComponentName activityName = new ComponentName(info.packageName, info.name);
            mAppComponentSet[i] = activityName;
            try {
                Drawable icon = info.loadIcon(mPm);
                CharSequence name = info.loadLabel(mPm);
                DefaultAppSettingPreference pref = new DefaultAppSettingPreference(this, activityName, prefIndex,
                        icon, name, info, mAppClickListener);
                //Bug 634743 revised with huawei design and hide laucher of settings
                Log.d(TAG,"activityName="+activityName.getClassName());
                if(!activityName.getClassName().equals("com.android.settings.FallbackHome")){
                    mPrefs.add(pref);
                    mPrefGroup.addPreference(pref);
                    pref.setEnabled(true);
                    if (activityName.equals(mUtils.currentDefaultApp)) {
                        mCurrentApp = pref;
                    }
                    prefIndex++;
                }
            } catch (Exception e) {
                Log.v(TAG, "Problem dealing with activity " + activityName, e);
            }
        }

        if (mCurrentApp != null) {
            new Handler().post(new Runnable() {
               public void run() {
                   mCurrentApp.setChecked(true);
               }
            });
        } else if(mPrefGroup.getPreferenceCount() == 1) {
            new Handler().post(new Runnable() {
               public void run() {
                   ((DefaultAppSettingPreference)mPrefGroup.getPreference(0)).setChecked(true);
               }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.default_app_selection);

        mPm = getPackageManager();
        mUtils = new DefaultAppSettingsUtils(this);
        appName = getIntent().getStringExtra("appName");
        mPrefGroup = (PreferenceGroup) findPreference("app");
        this.getActionBar().setTitle(appNameToIDMap.get(appName));

    }

    @Override
    public void onResume() {
        super.onResume();
        buildAppActivitiesList();
    }
}
