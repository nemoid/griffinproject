/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.settings.applications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SearchIndexableResource;
import android.support.v7.preference.Preference;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.Utils;
import com.android.settings.applications.PermissionsSummaryHelper.PermissionsResultCallback;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settingslib.applications.ApplicationsState.AppEntry;
import com.android.settingslib.applications.ApplicationsState.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.app.Dialog;
/*SPRD: 551243 ADD import for CCSA4 feature @{ */
import android.os.SystemProperties;
/* @} */

public class AdvancedAppSettings extends SettingsPreferenceFragment implements
        ApplicationsState.Callbacks, Indexable {

    static final String TAG = "AdvancedAppSettings";

    private static final String KEY_APP_PERM = "manage_perms";
    private static final String KEY_APP_DOMAIN_URLS = "domain_urls";
    private static final String KEY_HIGH_POWER_APPS = "high_power_apps";
    private static final String KEY_SYSTEM_ALERT_WINDOW = "system_alert_window";
    private static final String KEY_WRITE_SETTINGS_APPS = "write_settings_apps";
    /* SPRD: Bug 597519  @{ */
    private static final String KEY_DEFAULT_HOME = "default_home";
    private static final String KEY_DEFAULT_BROWSER = "default_browser";
    private static final String KEY_DEFAULT_PHONE_APP = "default_phone_app";
    private static final String KEY_DEFAULT_EMERGENCY_APP = "default_emergency_app";
    private static final String KEY_SMS_APPLICATION = "default_sms_app";
    /* @} */
    private Session mSession;
    private Preference mAppPermsPreference;
    private Preference mAppDomainURLsPreference;
    private Preference mHighPowerPreference;
    private Preference mSystemAlertWindowPreference;
    private Preference mWriteSettingsPreference;


    /* SPRD: Bug 597519  @{ */
    private DefaultHomePreference mDefaultHomePreference;
    private DefaultBrowserPreference mDefaultBrowserPreference ;
    private DefaultPhonePreference mDefaultPhonePreference ;
    private DefaultEmergencyPreference mDefaultEmergencPreference;
    private DefaultSmsPreference mDefaultSmsPreference;
    /* @} */
    private BroadcastReceiver mPermissionReceiver;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.advanced_apps);

        Preference permissions = getPreferenceScreen().findPreference(KEY_APP_PERM);
        permissions.setIntent(new Intent(Intent.ACTION_MANAGE_PERMISSIONS));

        ApplicationsState applicationsState = ApplicationsState.getInstance(
                getActivity().getApplication());
        mSession = applicationsState.newSession(this);

        mAppPermsPreference = findPreference(KEY_APP_PERM);
        mAppDomainURLsPreference = findPreference(KEY_APP_DOMAIN_URLS);
        mHighPowerPreference = findPreference(KEY_HIGH_POWER_APPS);
        mSystemAlertWindowPreference = findPreference(KEY_SYSTEM_ALERT_WINDOW);
        mWriteSettingsPreference = findPreference(KEY_WRITE_SETTINGS_APPS);
        /* SPRD: Bug 597519  @{ */
        mDefaultHomePreference = (DefaultHomePreference) findPreference(KEY_DEFAULT_HOME);
        mDefaultBrowserPreference = (DefaultBrowserPreference)findPreference(KEY_DEFAULT_BROWSER);
        mDefaultPhonePreference = (DefaultPhonePreference)findPreference(KEY_DEFAULT_PHONE_APP);
        mDefaultEmergencPreference = (DefaultEmergencyPreference) findPreference(KEY_DEFAULT_EMERGENCY_APP);
        mDefaultSmsPreference = (DefaultSmsPreference)findPreference(KEY_SMS_APPLICATION);
        /* @} */
    }

    @Override
    public void onResume() {
        super.onResume();
        /* SPRD: Bug 597519  @{ */
        if(mDefaultHomePreference != null) {
            mDefaultHomePreference.refreshHomeOptions();
        }

        if(mDefaultBrowserPreference != null) {
            mDefaultBrowserPreference.updateDefaultBrowserPreference();
        }

        if(mDefaultPhonePreference != null) {
            mDefaultPhonePreference.loadDialerApps();
        }

        if(mDefaultEmergencPreference != null) {
            mDefaultEmergencPreference.load();
        }

        if(mDefaultSmsPreference != null) {
            mDefaultSmsPreference.loadSmsApps();
        }
        /*SPRD: 551243 disable mAppPermsPreference for CCSA4 feature @{ */
        if(mAppPermsPreference != null){
            if(SystemProperties.get("persist.support.securetest").equals("1")){
                getPreferenceScreen().removePreference(mAppPermsPreference);
            }else{
                getPreferenceScreen().addPreference(mAppPermsPreference);
            }
        }
        /* @} */
    }

    @Override
    public void onPause() {
        super.onPause();
        /* SPRD: Bug 597519  @{ */
        if (mDefaultHomePreference != null) {
            Dialog preHomeDialog = mDefaultHomePreference.getDialog();
            if(preHomeDialog != null && preHomeDialog.isShowing()){
                preHomeDialog.dismiss();
            }
        }
        if (mDefaultBrowserPreference != null) {
            Dialog preBrowserDialog = mDefaultBrowserPreference.getDialog();
            if(preBrowserDialog != null && preBrowserDialog.isShowing()){
                preBrowserDialog.dismiss();
            }
        }
        if (mDefaultPhonePreference != null) {
            Dialog prePhoneDialog = mDefaultPhonePreference.getDialog();
            if(prePhoneDialog != null && prePhoneDialog.isShowing()){
                prePhoneDialog.dismiss();
            }
        }
        if (mDefaultEmergencPreference != null) {
            Dialog preEmergencDialog = mDefaultEmergencPreference.getDialog();
            if(preEmergencDialog != null && preEmergencDialog.isShowing()){
                preEmergencDialog.dismiss();
            }
        }
        if (mDefaultSmsPreference != null) {
            Dialog preSmsDialog = mDefaultSmsPreference.getDialog();
            if(preSmsDialog != null && preSmsDialog.isShowing()){
                preSmsDialog.dismiss();
            }
        }
        /* @} */
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.APPLICATIONS_ADVANCED;
    }

    @Override
    public void onRunningStateChanged(boolean running) {
        // No-op.
    }

    @Override
    public void onPackageListChanged() {
        // No-op.
    }

    @Override
    public void onRebuildComplete(ArrayList<AppEntry> apps) {
        // No-op.
    }

    @Override
    public void onPackageIconChanged() {
        // No-op.
    }

    @Override
    public void onPackageSizeChanged(String packageName) {
        // No-op.
    }

    @Override
    public void onAllSizesComputed() {
        // No-op.
    }

    @Override
    public void onLauncherInfoChanged() {
        // No-op.
    }

    @Override
    public void onLoadEntriesCompleted() {
        // No-op.
    }

    private final PermissionsResultCallback mPermissionCallback = new PermissionsResultCallback() {
        @Override
        public void onAppWithPermissionsCountsResult(int standardGrantedPermissionAppCount,
                int standardUsedPermissionAppCount) {
            if (getActivity() == null) {
                return;
            }
            mPermissionReceiver = null;
            if (standardUsedPermissionAppCount != 0) {
                mAppPermsPreference.setSummary(getContext().getString(
                        R.string.app_permissions_summary,
                        standardGrantedPermissionAppCount,
                        standardUsedPermissionAppCount));
            } else {
                mAppPermsPreference.setSummary(null);
            }
        }
    };

    public static final Indexable.SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
            new BaseSearchIndexProvider() {
                @Override
                public List<SearchIndexableResource> getXmlResourcesToIndex(
                        Context context, boolean enabled) {
                    SearchIndexableResource sir = new SearchIndexableResource(context);
                    sir.xmlResId = R.xml.advanced_apps;
                    return Arrays.asList(sir);
                }

                @Override
                public List<String> getNonIndexableKeys(Context context) {
                    return Utils.getNonIndexable(R.xml.advanced_apps, context);
                }
            };
}
