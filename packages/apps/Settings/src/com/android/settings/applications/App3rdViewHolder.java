package com.android.settings.applications;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.settings.R;

import com.android.settings.applications.ManageApplications;
import com.android.settingslib.applications.ApplicationsState;

public class App3rdViewHolder {
    public ApplicationsState.AppEntry entry;
    public View rootView;
    public TextView appName;
    public ImageView appIcon;
    public CheckBox checkBox;

    static public App3rdViewHolder createOrRecycle(LayoutInflater inflater,
            View convertView) {
        if (convertView == null) {
            convertView = inflater
                    .inflate(R.layout.autorun_filelist_item, null);
            App3rdViewHolder holder = new App3rdViewHolder();
            holder.rootView = convertView;
            holder.appName = (TextView) convertView.findViewById(R.id.app_name);
            holder.appIcon = (ImageView) convertView
                    .findViewById(R.id.app_icon);
            holder.checkBox = (CheckBox) convertView
                    .findViewById(R.id.select_checkbox);
            convertView.setTag(holder);
            return holder;
        } else {
            return (App3rdViewHolder) convertView.getTag();
        }
    }
}
