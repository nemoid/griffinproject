/**
 * SPRD: Created by Spreadst, for feature 535093, support default app settings.
 */

package com.android.settings.applications;

import java.util.ArrayList;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;

import android.provider.Telephony.Sms.Intents;
import com.android.internal.telephony.SmsApplication;
import com.android.internal.telephony.SmsApplication.SmsApplicationData;
import android.net.Uri;
import java.util.Collection;
import java.util.List;
import com.android.settings.R;

public class DefaultAppSettingsUtils{
    static final String TAG = "DefaultAppSettingsUtils";

    PackageManager mPm;
    ComponentName currentDefaultApp;
    ArrayList<ResolveInfo> appActivities;
    Context mContext;


    public DefaultAppSettingsUtils(Context context) {
        mContext = context;
        mPm = context.getPackageManager();
    }


    public void getAppList(String app) {
        appActivities = new ArrayList<ResolveInfo>();

        if(app.equals("launcher")) {
            currentDefaultApp  = mPm.getHomeActivities(appActivities);
        } else if(app.equals("messaging")) {
            currentDefaultApp  = SmsApplication.getDefaultSmsApplication(mContext, true);

            Collection<SmsApplicationData> smsApplications =
                            SmsApplication.getApplicationCollection(mContext);
            Intent intent = new Intent(Intents.SMS_DELIVER_ACTION);
            List<ResolveInfo> smsReceivers = mPm.queryBroadcastReceivers(intent, 0);
            for (ResolveInfo resolveInfo : smsReceivers) {
                for (SmsApplicationData smsApplicationData : smsApplications) {
                    if(resolveInfo.activityInfo.packageName.equals(smsApplicationData.mPackageName))
                        appActivities.add(resolveInfo);
                }
            }
        } else {
            Intent intent;
            if (app.equals("call")) {
                intent = new Intent("android.intent.action.DIAL");
            } else if(app.equals("camera")) {
                intent = new Intent("android.media.action.IMAGE_CAPTURE");
            } else if(app.equals("gallery")) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(Uri.parse("file://"), "image/*");
            } else if(app.equals("music")) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(Uri.parse("file://"), "audio/*");
            } else if(app.equals("email")) {
                intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
            }  else {
                return;
            }

            appActivities = (ArrayList<ResolveInfo>) mPm.queryIntentActivities(intent, 0);
            for (ResolveInfo resolveInfo : appActivities) {
                if(isCurrentDefault(resolveInfo.activityInfo.packageName)) {
                    currentDefaultApp = new ComponentName(resolveInfo.activityInfo.packageName,
                            resolveInfo.activityInfo.name);
                    break;
                }
            }
        }
    }

    public void configurePreferredActivity(String appName, ComponentName componentName, ComponentName[] appComponentSet) {
        IntentFilter intentFilter = new IntentFilter();

        if (appName.equals("launcher")) {
            intentFilter.addAction(Intent.ACTION_MAIN);
            intentFilter.addCategory(Intent.CATEGORY_HOME);
            intentFilter.addCategory(Intent.CATEGORY_DEFAULT);

            mPm.replacePreferredActivity(intentFilter, IntentFilter.MATCH_CATEGORY_EMPTY,
                    appComponentSet, componentName);
        } else if (appName.equals("messaging")) {
            SmsApplication.setDefaultApplication(componentName.getPackageName(), mContext);
        } else if (appName.equals("call")) {
            intentFilter.addAction("android.intent.action.DIAL");
            intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
            intentFilter.addDataScheme("tel");

            mPm.replacePreferredActivity(intentFilter, IntentFilter.MATCH_CATEGORY_SCHEME
                     + IntentFilter.MATCH_ADJUSTMENT_NORMAL, appComponentSet, componentName);
        } else if (appName.equals("camera")) {
            intentFilter.addAction("android.media.action.IMAGE_CAPTURE");
            intentFilter.addCategory(Intent.CATEGORY_DEFAULT);

            mPm.replacePreferredActivity(intentFilter, IntentFilter.MATCH_CATEGORY_EMPTY,
                    appComponentSet, componentName);
        } else if (appName.equals("email")) {
            intentFilter.addAction(Intent.ACTION_SENDTO);
            intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
            intentFilter.addDataScheme("mailto");

            mPm.replacePreferredActivity(intentFilter, IntentFilter.MATCH_CATEGORY_SCHEME
                    + IntentFilter.MATCH_ADJUSTMENT_NORMAL, appComponentSet, componentName);
        } else {
            intentFilter.addAction("android.intent.action.VIEW");
            intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
            intentFilter.addDataScheme("file");
            intentFilter.addDataScheme("content");
            try {
                if (appName.equals("music"))
                    intentFilter.addDataType("audio/*");
                else
                    intentFilter.addDataType("image/*");
            } catch (IntentFilter.MalformedMimeTypeException e) {
                Log.w("ResolverActivity", e);
                intentFilter = null;
            }

            if (currentDefaultApp != null) {
                mPm.clearPackagePreferredActivities(currentDefaultApp.getPackageName());
            }

            mPm.addPreferredActivity(intentFilter,IntentFilter.MATCH_CATEGORY_TYPE, appComponentSet, componentName);
        }
    }


    private boolean isCurrentDefault(String packageName) {
        List<IntentFilter> intentList = new ArrayList<IntentFilter>();
        List<ComponentName> prefActList = new ArrayList<ComponentName>();
        mPm.getPreferredActivities(intentList, prefActList, packageName);
        if (prefActList != null && prefActList.size() > 0) {
            return true;
        }
        return false;
    }

    public String getDefaultApplication(String name) {
        getAppList(name);
        for (int i = 0; i < appActivities.size(); i++) {
            ActivityInfo info = appActivities.get(i).activityInfo;
            if(currentDefaultApp != null && currentDefaultApp.getPackageName().equals(info.packageName)) {
                //if (isSystem(info))
                //    return mContext.getResources().getString(R.string.default_app_settings_default);
                //else
                    return (String) info.loadLabel(mPm);
            }
        }
        return null;
    }

    private boolean isSystem(ActivityInfo info) {
        boolean isSystem;
        final Bundle meta = info.metaData;
        if (meta != null) {
            final String altHomePackage = meta.getString(ActivityManager.META_HOME_ALTERNATE);
            if (altHomePackage != null) {
                try {
                    final int match = mPm.checkSignatures(info.packageName, altHomePackage);
                    if (match >= PackageManager.SIGNATURE_MATCH) {
                        PackageInfo altInfo = mPm.getPackageInfo(altHomePackage, 0);
                        final int altFlags = altInfo.applicationInfo.flags;
                        isSystem = (altFlags & ApplicationInfo.FLAG_SYSTEM) != 0;
                    }
                } catch (Exception e) {
                    // e.g. named alternate package not found during lookup
                    Log.w(TAG, "Unable to compare/resolve alternate", e);
                }
            }
        }
        isSystem = (info.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;

        return isSystem;
    }
}
