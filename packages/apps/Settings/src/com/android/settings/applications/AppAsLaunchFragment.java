/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.applications;

import java.text.SimpleDateFormat;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.ActivityNotFoundException;
import android.app.ActivityManagerNativeEx;
import android.app.AppAs;
import android.app.Fragment;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.ListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.app.Activity;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.RemoteException;

import android.app.ActivityManager;
import com.android.settings.R;
import com.android.settings.Settings.AppAsLaunchItemDetailsActivity;
import com.android.settings.SettingsActivity;
import com.android.settings.notification.ConfigureNotificationSettings;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.search.SearchIndexableRaw;
import com.android.settings.search.Indexable.SearchIndexProvider;

import android.widget.ListView;
import android.content.pm.ApplicationInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppAsLaunchFragment extends Fragment
        implements Indexable, View.OnClickListener {
    private static final String TAG = "AppAsLaunchFragment";
    private static final String PACKAGE_SCHEME = "package";

    private final PackageReceiver mPackageReceiver = new PackageReceiver();

    private class PackageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "PackageReceiver.onReceive");
            onResume();
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int index = msg.what;
            // SPRD:modify for Bug 659322 IndexOutOfBoundsException with uninstalling app in multi-screen.
            if (mAppInfo != null && index != -1  && index < mAppInfo.size()) {
                Log.d(TAG, "handleMessage remove index = "+index);
                mAppInfo.remove(index);
                mAppAdapter.notifyDataSetChanged();
            } else if (index == -1) {
                updateSelectAll();
            }
        }
    };

    private ListView listview = null;
    private RelativeLayout mSelectLayout = null;
    private CheckBox mChkAll;
    private TextView mSelectAll;
    private PackageManager pm;
    private List<AppAs.AppAsData> mAppInfo = null;
    private AsApplicationInfoAdapter mAppAdapter = null;
    private Context mContext;
    public static LayoutInflater mInflater;
    private View mRootView;
    private View emptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.as_launch_record, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        switch (item.getItemId()) {
            case R.id.record:
                Intent intent = new Intent(this.getActivity(), AppAsRecord.class);
                startActivity(intent);
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mInflater = inflater;
        mRootView = mInflater.inflate(R.layout.app_as_launch, container,
                false);
        initialViews(container);
        return mRootView;
    }

    public void initialViews(ViewGroup contentParent) {
        if (mRootView == null) {
            Log.e(TAG, "mRootView == null ");
            return;
        }
        listview = (ListView) mRootView.findViewById(R.id.mList);
        mSelectLayout = (RelativeLayout) mRootView.findViewById( R.id.selectlayout );
        mChkAll = (CheckBox) mRootView.findViewById( R.id.chkall );
        mChkAll.setOnClickListener( this );
        mSelectAll = (TextView) mRootView.findViewById( R.id.selectall );
        mSelectLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.chkall :
                selectAllProcess(v);
                break;
            default :
                break;
        }
    }

    private void selectAllProcess(View v) {
        boolean isChecked = ((CheckBox)v).isChecked();
        for (AppAs.AppAsData data : mAppInfo) {
            setAppAsEnable(isChecked ? 1 : 0, data.getPackage());
            data.setEnabled(isChecked);
        }
        if (mAppAdapter != null)
            mAppAdapter.notifyDataSetChanged();
        mChkAll.setChecked(isChecked);
        mSelectAll.setText(isChecked
                ? R.string.cancle_select_all : R.string.select_all);
    }

    private void updateSelectAll() {
        if(mAppInfo == null || mAppInfo.size() <= 0){
            mChkAll.setChecked(false);
            mSelectAll.setText(R.string.select_all);
        } else {
            boolean checkAll = mAppAdapter.getEnabledCount() == mAppInfo.size();
            mChkAll.setChecked(checkAll);
            mSelectAll.setText(checkAll
                    ? R.string.cancle_select_all : R.string.select_all);
        }
    }

    public void setAppAsEnable(int flag, String packageName){
        try{
            ActivityManagerNativeEx.getDefault().setAppAsEnable(flag, packageName, null);
        } catch(RemoteException e) {
            Log.e(TAG,"Oops, setAppAsEnable Get RemoteException");
        }
    }

    public void onBackPressed() {
        getActivity().finish();
    }

    public void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        filter.addDataScheme(PACKAGE_SCHEME);
        mContext.registerReceiver(mPackageReceiver , filter);

        filter = new IntentFilter();
        filter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
        filter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE);
        mContext.registerReceiver(mPackageReceiver , filter);
    }

    public void onStop() {
        super.onStop();
        mContext.unregisterReceiver(mPackageReceiver);
    }

    public void onResume() {
        //load data
        emptyView = mRootView.findViewById(com.android.internal.R.id.empty);
        try{
            mAppInfo = ActivityManagerNativeEx.getDefault().getAppAsList();
        } catch(RemoteException e) {
            Log.e(TAG,"Oops, Get RemoteException");
            mAppInfo = null;
        }
        pm = mContext.getPackageManager();
        if(mAppInfo == null || mAppInfo.size() <= 0){
            listview.setAdapter(null);
            listview.setEmptyView(emptyView);
        } else {
            mAppAdapter = new AsApplicationInfoAdapter(mContext, mAppInfo, null);
            mAppAdapter.setHandler(mHandler);
            listview.setAdapter(mAppAdapter);
            emptyView.setVisibility(View.GONE);
        }
        updateSelectAll();
        listview.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
                String packageName = mAppInfo.get(arg2).getPackage();
                if (packageName != null) {
                    // start new activity to manage app as launch
                    Intent intent = new Intent();
                    intent.putExtra(Intent.EXTRA_PACKAGE_NAME, packageName);
                    /* SPRD:modify for Bug 652332 no back key in actionbar in associated start. @{ */
                    intent.putExtra(SettingsActivity.EXTRA_SHOW_FRAGMENT_AS_SUBSETTING, true);
                    intent.setClass(getActivity(), AppAsLaunchItemDetailsActivity.class);
                    /* @} */
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Log.w(TAG, "No app can handle as_launch_app");
                    }
                }
            }
        });
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        mAppAdapter = null;
    }

    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
            new AppAsLaunchSearchIndexProvider();

    private static class AppAsLaunchSearchIndexProvider extends BaseSearchIndexProvider {
        private boolean mIsOwner;

        public AppAsLaunchSearchIndexProvider() {
            mIsOwner = UserHandle.myUserId() == UserHandle.USER_OWNER;
        }
        /* @} */

        @Override
        public List<SearchIndexableRaw> getRawDataToIndex(Context context, boolean enabled) {
            final List<SearchIndexableRaw> result = new ArrayList<SearchIndexableRaw>();
            if (!mIsOwner) {
                return result;
            }
            SearchIndexableRaw data = new SearchIndexableRaw(context);
            final Resources res = context.getResources();
            // SPRD:modify for Bug 648854 can not search associated start in Settings.
            final String screenTitle = res.getString(R.string.app_as_launch);
            data.title = screenTitle;
            result.add(data);
            return result;
        }

    }
}
