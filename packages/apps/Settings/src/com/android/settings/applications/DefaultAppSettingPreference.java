/**
 * SPRD: Created by Spreadst, for feature 535093, support default app settings.
 */

package com.android.settings.applications;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.preference.Preference;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import com.android.settings.R;

public class DefaultAppSettingPreference extends Preference {
    static final String TAG = "DefaultAppSettingPreference";
    ComponentName activityName;
    String applicationName;
    int index;
    final ColorFilter grayscaleFilter;
    boolean isChecked;
    PackageManager mPm;
    OnClickListener mListener;
    boolean isSystem;

    public DefaultAppSettingPreference(Context context, ComponentName activity,
        int i, Drawable icon, CharSequence title, ActivityInfo info, OnClickListener listener) {
        super(context);
        setLayoutResource(R.layout.preference_default_app_settings);
        setIcon(icon);
        setTitle(title);
        applicationName = title.toString();
        activityName = activity;
        index = i;
        mPm = context.getPackageManager();
        mListener = listener;

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0f);
        float[] matrix = colorMatrix.getArray();
        matrix[18] = 0.5f;
        grayscaleFilter = new ColorMatrixColorFilter(colorMatrix);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        RadioButton radio = (RadioButton) view.findViewById(R.id.default_radio);
        radio.setChecked(isChecked);

        Integer indexObj = new Integer(index);
        View v = view.findViewById(R.id.default_app_pref);
        v.setOnClickListener(mListener);
        v.setTag(indexObj);
    }

    void setChecked(boolean state) {
        if (state != isChecked) {
            isChecked = state;
            notifyChanged();
        }
    }
}
