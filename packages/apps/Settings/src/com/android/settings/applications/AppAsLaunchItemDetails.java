/*
 * Copyright ? 2017 Spreadtrum Communications Inc.
 */
package com.android.settings.applications;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManagerNativeEx;
import android.app.AppAs;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;

import java.util.List;

public class AppAsLaunchItemDetails extends SettingsPreferenceFragment
        implements View.OnClickListener {
    private static final String TAG = "AppAsLaunchItemDetails";
    private static final String PACKAGE_SCHEME = "package";

    private final PackageReceiver mPackageReceiver = new PackageReceiver();

    private class PackageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "PackageReceiver.onReceive");
            onResume();
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int index = msg.what;
            // SPRD:modify for Bug 659322 IndexOutOfBoundsException with uninstalling app in multi-screen.
            if (mAppInfo != null && index != -1  && index < mAppInfo.size()) {
                Log.d(TAG, "handleMessage remove index = "+index);
                mAppInfo.remove(index);
                mAppAdapter.notifyDataSetChanged();
            } else if (index == -1) {
                updateSelectAll();
            }
        }
    };

    private ListView mListview = null;
    private RelativeLayout mSelectLayout = null;
    private CheckBox mChkAll;
    private TextView mSelectAll;
    private PackageManager pm;
    private List<AppAs.AppAsData> mAppInfo = null;
    private AsApplicationInfoAdapter mAppAdapter = null;
    private Context mContext;
    public static LayoutInflater mInflater;
    private View mRootView;
    private View mEmptyView;
    private String mBaseName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mInflater = inflater;
        mRootView = mInflater.inflate(R.layout.app_as_launch, container,
                false);
        initialViews(container);
        mEmptyView = mRootView.findViewById(com.android.internal.R.id.empty);
        return mRootView;
    }

    /* SPRD:modify for Bug 652332 no back key in actionbar in associated start. @{ */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
            return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }
    /* @} */

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.APPLICATION;
    }

    public void initialViews(ViewGroup contentParent) {
        if (mRootView == null) {
            Log.e(TAG, "mRootView == null ");
            return;
        }
        mListview = (ListView) mRootView.findViewById(R.id.mList);
        mSelectLayout = (RelativeLayout) mRootView.findViewById( R.id.selectlayout );
        mChkAll = (CheckBox) mRootView.findViewById( R.id.chkall );
        mChkAll.setOnClickListener( this );
        mSelectAll = (TextView) mRootView.findViewById( R.id.selectall );
        mSelectLayout.setVisibility(View.VISIBLE);
    }

    public void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        filter.addDataScheme(PACKAGE_SCHEME);
        mContext.registerReceiver(mPackageReceiver , filter);

        filter = new IntentFilter();
        filter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
        filter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE);
        mContext.registerReceiver(mPackageReceiver , filter);
    }

    public void onStop() {
        super.onStop();
        mContext.unregisterReceiver(mPackageReceiver);
    }

    public void onResume() {
        //load data
        mBaseName = getActivity().getIntent().getStringExtra(Intent.EXTRA_PACKAGE_NAME);
        if (mBaseName == null) {
            Log.w(TAG, "Missing mandatory argument EXTRA_PACKAGE_NAME");
            getActivity().finish();
        }
        try{
            mAppInfo = ActivityManagerNativeEx.getDefault().getAppAsLauncherList(mBaseName);
        } catch(RemoteException e) {
            Log.e(TAG,"Oops, Get RemoteException");
            mAppInfo = null;
        }
        CharSequence normalTitle = getActivity().getTitle();
        pm = getActivity().getPackageManager();
        try{
            ApplicationInfo aiInfo = pm.getApplicationInfo(mBaseName, 0);
            getActivity().setTitle(aiInfo.loadLabel(pm));
        } catch(PackageManager.NameNotFoundException e){
            getActivity().setTitle(normalTitle);
        }
        if(mAppInfo == null || mAppInfo.size() <= 0){
            Log.e(TAG,"mAppInfo == null or size <0");
            mListview.setAdapter(null);
            mListview.setEmptyView(mEmptyView);
        } else {
            mAppAdapter = new AsApplicationInfoAdapter(getActivity().getBaseContext(), mAppInfo, mBaseName);
            mAppAdapter.setHandler(mHandler);
            mListview.setAdapter(mAppAdapter);
            mEmptyView.setVisibility(View.GONE);
        }
        updateSelectAll();
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        mAppAdapter = null;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.chkall :
                selectAllProcess(v);
                break;
            default :
                break;
        }
    }

    private void selectAllProcess(View v) {
        boolean isChecked = ((CheckBox)v).isChecked();
        setAppAsEnable(isChecked ? 1 : 0, mBaseName);
		// wangxiaomei add start
		if(mAppInfo != null){
            for (AppAs.AppAsData data : mAppInfo) {
                 data.setEnabled(isChecked);
			}
        }
		if (mAppAdapter != null) {
            mAppAdapter.notifyDataSetChanged();
        }
		// wangxiaomei add end
        mChkAll.setChecked(isChecked);
        mSelectAll.setText(isChecked
                ? R.string.cancle_select_all : R.string.select_all);
    }

    private void updateSelectAll() {
        if(mAppInfo == null || mAppInfo.size() <= 0){
            mChkAll.setChecked(false);
            mSelectAll.setText(R.string.select_all);
        } else {
            boolean checkAll = mAppAdapter.getEnabledCount() == mAppInfo.size();
            mChkAll.setChecked(checkAll);
            mSelectAll.setText(checkAll
                    ? R.string.cancle_select_all : R.string.select_all);
        }
    }

    public void setAppAsEnable(int flag, String packageName){
        try{
            ActivityManagerNativeEx.getDefault().setAppAsEnable(flag, packageName, null);
        } catch(RemoteException e) {
            Log.e(TAG,"Oops, setAppAsEnable Get RemoteException");
        }
    }
}
