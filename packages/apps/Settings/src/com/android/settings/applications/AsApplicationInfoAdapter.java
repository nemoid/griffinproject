/**
 *
 */
package com.android.settings.applications;

import android.app.ActivityManager;
import android.app.ActivityManagerNativeEx;
import android.app.AppAs;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import android.os.RemoteException;
import android.util.Log;

import com.android.settings.R;

public class AsApplicationInfoAdapter extends BaseAdapter {

    private static final String TAG = "AsApplicationInfoAdapter";
    private List<AppAs.AppAsData> mlistAppInfo = null;
    private Context mContext = null;
    LayoutInflater infater = null;
    PackageManager mPm;
    String mBasePackage = null;
    private Handler mHandler = null;

    public AsApplicationInfoAdapter(Context context, List<AppAs.AppAsData> apps, String calledPackage) {
        mContext = context;
        infater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mlistAppInfo = apps ;
        mPm = mContext.getPackageManager();
        mBasePackage = calledPackage;
    }

    @Override
    public int getCount() {
        return mlistAppInfo.size();
    }
    @Override
    public Object getItem(int position) {
        return mlistAppInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setList(List<AppAs.AppAsData> list) {
        mlistAppInfo = list;
    }

    public int getEnabledCount() {
        int count = 0;
        for (AppAs.AppAsData data: mlistAppInfo) {
            if (data.getEnabled()) {
                count ++;
            }
        }
        return count;
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup arg2) {
        View view = null;
        ViewHolder holder = null;
        if (convertview == null || convertview.getTag() == null) {
            view = infater.inflate(R.layout.browse_app_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertview ;
            holder = (ViewHolder) convertview.getTag() ;
        }
        AppAs.AppAsData appAsdata = (AppAs.AppAsData)getItem(position);
        String packageName = appAsdata.getPackage();
        int launchCount = appAsdata.getCount();
        long launchTime = appAsdata.getLastTickTime();
        SimpleDateFormat formatter = new SimpleDateFormat ("HH:mm:ss ");
        Date curDate = new Date(launchTime);
        String str = formatter.format(curDate);
        ApplicationInfo mAppInfo = null;

        try{
            mAppInfo = mPm.getApplicationInfo(packageName, 0);
            holder.appIcon.setImageDrawable(mAppInfo.loadIcon(mPm));
            holder.tvAppLabel.setText(mAppInfo.loadLabel(mPm));
        } catch(PackageManager.NameNotFoundException e){
            //holder.appIcon.setImageDrawable(null);
            //holder.tvAppLabel.setText(null);
            // remove this item if package not exist.
            if (mHandler != null) {
                Message message = Message.obtain();
                message.what = position;
                mHandler.sendMessage(message);
            }

            return view;
        }

        /* SPRD:modify for Bug 649544 strings of launch time truncated in associated start with largest font. @{ */
        String count = mContext.getResources().getString(
                R.string.app_as_launch_count,
                String.valueOf(launchCount), str);
        holder.tvAppCount.setText(count);
        /* @} */

        final String pkgName = packageName;
        holder.switcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox) v;
                checkBox.toggle();
                // change checkbox checked or not.
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    appAsdata.setEnabled(false);
                } else {
                    checkBox.setChecked(true);
                    appAsdata.setEnabled(true);
                }
                //save state into xml.
                if (checkBox.isChecked()) {
                    setAppAsEnable(1, pkgName);
                } else {
                    setAppAsEnable(0, pkgName);
                }

                if (mHandler != null) {
                    Message message = Message.obtain();
                    message.what = -1;
                    mHandler.sendMessage(message);
                }
            }
        });

        holder.switcher.setChecked(appAsdata.getEnabled());
        return view;
    }

    public void setAppAsEnable(int flag, String packageName){
        try{
            ActivityManagerNativeEx.getDefault().setAppAsEnable(flag, packageName, mBasePackage);
        } catch(RemoteException e) {
            Log.e(TAG,"Oops, setAppAsEnable Get RemoteException");
        }
    }

    class ViewHolder {
        ImageView appIcon;
        TextView tvAppLabel;
        CheckBox switcher;
        TextView tvAppCount;
        // SPRD:modify for Bug 649544 strings of launch time truncated in associated start with largest font.
        //TextView tvAppTime;

        public ViewHolder(View view) {
            this.appIcon = (ImageView) view.findViewById(R.id.imgApp);
            this.tvAppLabel = (TextView) view.findViewById(R.id.tvAppLabel);
            this.switcher = (CheckBox) view.findViewById(R.id.switcher);
            this.tvAppCount = (TextView) view.findViewById(R.id.tvAppCount);
            // SPRD:modify for Bug 649544 strings of launch time truncated in associated start with largest font.
            //this.tvAppTime = (TextView) view.findViewById(R.id.tvAppTime);
        }
    }
}
