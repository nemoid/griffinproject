/**
 * SPRD: Created by Spreadst, for feature 535093, support default app settings.
 */

package com.android.settings.applications;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.preference.PreferenceActivity;

import com.android.settings.R;
import android.util.Log;

public class DefaultAppList extends PreferenceActivity {

    private static final String KEY_LAUNCHER = "launcher";
    private static final String KEY_CALL = "call";
    private static final String KEY_MESSAGING = "messaging";
    private static final String KEY_CAMERA = "camera";
    private static final String KEY_GALLERY = "gallery";
    private static final String KEY_MUSIC = "music";
    private static final String KEY_EMAIL = "email";

    private PreferenceScreen mLauncherPreference;
    private PreferenceScreen mCallPreference;
    private PreferenceScreen mMessagingPreference;
    private PreferenceScreen mCameraPreference;
    private PreferenceScreen mGalleryPreference;
    private PreferenceScreen mMusicPreference;
    private PreferenceScreen mEmailPreference;

    private final PreferenceScreen[] APP_PREFERENCE = new PreferenceScreen[] {
        mLauncherPreference, mCallPreference,
        mMessagingPreference, mCameraPreference,
        mGalleryPreference, mMusicPreference,
        mEmailPreference
    };

    private final String[] APP_KEY = new String[] {
        KEY_LAUNCHER, KEY_CALL,
        KEY_MESSAGING, KEY_CAMERA,
        KEY_GALLERY, KEY_MUSIC,
        KEY_EMAIL
    };

    Context mContext = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.default_app_settings);
        mContext = this;

        for (int i = 0; i < APP_KEY.length; i++) {
            APP_PREFERENCE[i]  = (PreferenceScreen) findPreference(APP_KEY[i]);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        DefaultAppSettingsUtils mUtils = new DefaultAppSettingsUtils(this);
        String defaultSystem = getResources().getString(R.string.default_app_settings_default);
        String application;
        for (int i = 0; i < APP_KEY.length; i++) {
            application = mUtils.getDefaultApplication(APP_KEY[i]);
            if(application == null) {
                /* SPRD:modify for Bug 647724 Summary doesn't display default with Launcher3 is default Launcher in default app settings. @{ */
                int count = 0;
                for (int j = 0; j < mUtils.appActivities.size(); j++) {
                    final ResolveInfo candidate = mUtils.appActivities.get(j);
                    final ActivityInfo info = candidate.activityInfo;
                    ComponentName activityName = new ComponentName(info.packageName, info.name);
                    if(!"com.android.settings.FallbackHome".equals(activityName.getClassName())){
                        count++;
                    }
                }
                if( count > 1)
                /* @} */
                    application = "";
                else
                    application = defaultSystem;
            }
            APP_PREFERENCE[i].setSummary(application);
        }
    }

}
