/**
 * SPRD: Created by Spreadst, for feature 535093, support default app settings.
 */

package com.android.settings.applications;

import android.app.AddonManager;
import android.content.Context;
import android.util.Log;

import com.android.settings.R;

public class SupportDefaultAppSettings{
    static SupportDefaultAppSettings mInstance;

    public static SupportDefaultAppSettings getInstance() {
        if (mInstance != null) {
            return mInstance;
        }
        mInstance = (SupportDefaultAppSettings) AddonManager.getDefault().getAddon(
                R.string.feature_default_app_settings_addon,
                SupportDefaultAppSettings.class);
        return mInstance;
    }

    public boolean isSupport(){
        return false;
    }
}