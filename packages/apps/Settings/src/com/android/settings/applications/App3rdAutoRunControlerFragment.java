/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.applications;

import android.app.Activity;
import android.app.ActionBar;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.AppOpsManagerWrapper;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.UserHandle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.Button;
import android.widget.ImageButton;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Comparator;
import java.util.Map;

import com.android.settings.R;
import com.android.settings.Utils;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.search.Indexable.SearchIndexProvider;
import com.android.settings.search.SearchIndexableRaw;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settingslib.applications.ApplicationsState.AppEntry;

public class App3rdAutoRunControlerFragment extends Fragment implements Indexable {

    static final String TAG = "App3rdAutoRunControlerFragment";
    static final boolean DEBUG = true;

    public static LayoutInflater mInflater;
    public ApplicationsAdapter mApplications;

    private ApplicationsState mApplicationsState;
    private ViewGroup mRelative;
    private CheckBox mSelectAllBox;
    private TextView mSelectText;
    private View mRootView;
    private View mListContainer;
    private ListView mListView;
    private Context mContext;
    private AppOpsManager mAppOps;

    class ApplicationsAdapter extends BaseAdapter implements
            ApplicationsState.Callbacks, AbsListView.RecyclerListener {
        private final ApplicationsState mState;
        private final ApplicationsState.Session mSession;
        private final ArrayList<View> mActive = new ArrayList<View>();
        private ArrayList<ApplicationsState.AppEntry> mEntries;
        private boolean mResumed;
        private LayoutInflater inflater = mInflater;
        App3rdViewHolder holder;

        public ApplicationsAdapter(ApplicationsState state) {
            mState = state;
            mSession = state.newSession(this);
        }

        private HashMap<Long, Long> mCheckMap = new HashMap<Long, Long>();

        public void setChecked(int position, boolean checked) {
            long id = getItemId(position);
            if (checked) {
                mCheckMap.put(id, id);
            } else {
                mCheckMap.remove(id);
            }
            mAppOps.setMode(AppOpsManagerWrapper.OP_POST_AUTORUN, mEntries
                    .get(position).info.uid,
                    mEntries.get(position).info.packageName,
                    checked ? AppOpsManager.MODE_ALLOWED
                            : AppOpsManager.MODE_IGNORED);
        }

        public boolean hasCheckedItem() {
            return mCheckMap.size() > 0;
        }

        public boolean isChecked(int position) {
            long id = getItemId(position);
            return mCheckMap.containsValue(id);
        }

        public int getCheckedCount() {
            return mCheckMap.size();
        }

        public void updateCheckedMap() {
            if (mCheckMap.size() == 0) {
                return;
            }
            List<Long> delList = new ArrayList<Long>();
            for (Long id : mCheckMap.keySet()) {
                boolean isChecked = false;
                for (int j = 0; j < mEntries.size(); j++) {
                    if (mEntries.get(j).id == id) {
                        isChecked = true;
                        break;
                    }
                }
                if (isChecked == false) {
                    delList.add(id);
                }

            }
            for (int i = 0; i < delList.size(); i++) {
                mCheckMap.remove(delList.get(i));
            }
        }

        public ArrayList<ApplicationsState.AppEntry> getCheckedAppInfo() {
            ArrayList<ApplicationsState.AppEntry> ids = new ArrayList<ApplicationsState.AppEntry>(
                    mCheckMap.size());
            int pos = 0;
            ApplicationsState.AppEntry appInfo;
            for (int i = 0; i < getCount(); i++) {
                if (isChecked(i)) {
                    appInfo = mEntries.get(i);
                    ids.add(appInfo);
                }
            }
            return ids;
        }

        public void resume() {
            if (DEBUG)
                Log.i(TAG, "Resume!  mResumed=" + mResumed);
            if (!mResumed) {
                mResumed = true;
                mSession.resume();
                rebuild(true);
            }
        }

        public void pause() {
            if (mResumed) {
                mResumed = false;
                mSession.pause();
            }
        }

        public void release() {
            mSession.release();
        }

        public void rebuild(boolean eraseold) {
            if (DEBUG)
                Log.i(TAG, "Rebuilding app list...");
            if (mEntries != null) {
                mCheckMap.clear();
                if (DEBUG)
                    Log.i(TAG, "Rebuilding app list clear mCheckMap");
            }
            ApplicationsState.AppFilter filterObj = ApplicationsState.FILTER_THIRD_PARTY;
            Comparator<AppEntry> comparatorObj = ApplicationsState.ALPHA_COMPARATOR;
            ArrayList<ApplicationsState.AppEntry> entries = mSession.rebuild(
                    filterObj, comparatorObj);

            /* SPRD:modify for # 610523 modify for monkey test fail @{ */
            Log.e(TAG, "entries==" + entries);
            /* @} */

            if (entries == null && !eraseold) {
                return;
            }
            if (entries != null) {
                mEntries = entries;
                /* SPRD:modify for # 610523 modify for monkey test fail @{ */
                Log.e(TAG, "entries.size()==" + entries.size());
                /* @} */
            } else {
                mEntries = null;
            }
            /* SPRD: bug fix 565036 the startup manager shows wrong information @{ */
            if (mEntries != null) {
                Iterator<ApplicationsState.AppEntry> it = mEntries.iterator();
                while (it.hasNext()) {
                    ApplicationsState.AppEntry entry = it.next();
                    if ((entry.info.flags&ApplicationInfo.FLAG_INSTALLED) == 0) {
                        it.remove();
                    }
                }
            }
            /* @}*/

            for (int i = 0; i < getCount(); i++) {
                // import
                Log.e(TAG,
                        "begin checkOpNoThrow ==uid" + mEntries.get(i).info.uid
                                + "packageName="
                                + mEntries.get(i).info.packageName);
                boolean enabled = (mAppOps.checkOpNoThrow(
                        AppOpsManagerWrapper.OP_POST_AUTORUN,
                        mEntries.get(i).info.uid,
                        mEntries.get(i).info.packageName) == AppOpsManager.MODE_ALLOWED);
                Log.e(TAG, "enabled=" + enabled);
                setChecked(i, enabled);
            }
            updateViews();
            notifyDataSetChanged();

            if (entries == null) {
                mListContainer.setVisibility(View.INVISIBLE);
            } else {
                mListContainer.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onRunningStateChanged(boolean running) {
        }

        @Override
        public void onRebuildComplete(ArrayList<AppEntry> apps) {
            if (DEBUG)
                Log.i(TAG, "onRebuildComplete...");
            mListContainer.setVisibility(View.VISIBLE);
            mEntries = apps;

            /* SPRD: bug fix 617086 the startup manager @{ */
            if (mEntries != null) {
                Iterator<ApplicationsState.AppEntry> it = mEntries.iterator();
                while (it.hasNext()) {
                    ApplicationsState.AppEntry entry = it.next();
                    if ((entry.info.flags&ApplicationInfo.FLAG_INSTALLED) == 0) {
                        Log.d(TAG, "onRebuildComplete, remove packageName: " + entry.info.packageName);
                        it.remove();
                    }
                }
            }
            /* @}*/

            for (int i = 0; i < getCount(); i++) {
                boolean enabled = (mAppOps.checkOpNoThrow(
                        AppOpsManagerWrapper.OP_POST_AUTORUN,
                        mEntries.get(i).info.uid,
                        mEntries.get(i).info.packageName) == AppOpsManager.MODE_ALLOWED);
                setChecked(i, enabled);
            }

            updateViews();
            notifyDataSetChanged();
        }

        @Override
        public void onPackageListChanged() {
            if (DEBUG)
                Log.i(TAG, "onPackageListChanged...");
            rebuild(false);
        }

        @Override
        public void onPackageIconChanged() {
            // We ensure icons are loaded when their item is displayed, so
            // don't care about icons loaded in the background.
        }

        @Override
        public void onPackageSizeChanged(String packageName) {
        }

        @Override
        public void onAllSizesComputed() {

        }

        public int getCount() {
            return mEntries != null ? mEntries.size() : 0;
        }

        public Object getItem(int position) {
            return mEntries.get(position);
        }

        public ApplicationsState.AppEntry getAppEntry(int position) {
            return mEntries.get(position);
        }

        public long getItemId(int position) {
            return mEntries.get(position).id;
        }

        public View getView(final int position, View convertView,
                ViewGroup parent) {
            holder = App3rdViewHolder.createOrRecycle(mInflater, convertView);
            convertView = holder.rootView;
            ApplicationsState.AppEntry entry = mEntries.get(position);
            synchronized (entry) {
                holder.entry = entry;
                if (entry.label != null) {
                    holder.appName.setText(entry.label);
                }
                mState.ensureIcon(entry);
                if (entry.icon != null) {
                    holder.appIcon.setImageDrawable(entry.icon);
                }
                holder.checkBox.setChecked(isChecked(position));
            }
            mActive.remove(convertView);
            mActive.add(convertView);
            return convertView;
        }

        @Override
        public void onMovedToScrapHeap(View view) {
            mActive.remove(view);
        }

        @Override
        public void onLauncherInfoChanged() {
            rebuild(false);
        }

        @Override
        public void onLoadEntriesCompleted() {
            // TODO Auto-generated method stub
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mApplicationsState = ApplicationsState.getInstance(getActivity()
                .getApplication());
        mApplications = new ApplicationsAdapter(mApplicationsState);
        mAppOps = (AppOpsManager) getActivity().getSystemService(
                Context.APP_OPS_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mInflater = inflater;
        mRootView = mInflater.inflate(R.layout.autorun_multi_select, container,
                false);
        initialViews(container);
        return mRootView;
    }

    /* SPRD 602632: modify the title{@ */
    @Override
    public void onConfigurationChanged(
            android.content.res.Configuration newConfig) {
        ActionBar actionBar = getActivity().getActionBar();
        if (actionBar != null && getResources() != null) {
            actionBar.setTitle(setActionText(getResources(), actionBar, newConfig));
        }
        super.onConfigurationChanged(newConfig);
    }

    public static SpannableString setActionText(Resources res, ActionBar actionBar,
            android.content.res.Configuration newConfig) {
        final SpannableString text = new SpannableString(actionBar.getTitle());
        if (newConfig.toString().contains("land")) {
            text.setSpan(new AbsoluteSizeSpan(res.getDimensionPixelSize(
                    R.dimen.startup_land_actionbar_title_size)), 0,
                    text.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            text.setSpan(new AbsoluteSizeSpan(res.getDimensionPixelSize(
                    R.dimen.startup_port_actionbar_title_size)), 0,
                    text.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return text;
    }
    /* @} */

    public void onBackPressed() {
        getActivity().finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mApplications != null) {
            mApplications.resume();
        }
        updateViews();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mApplications != null) {
            mApplications.pause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mApplications != null) {
            mApplications.release();
        }
        mRootView = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void initialViews(ViewGroup contentParent) {
        if (mRootView == null) {
            Log.d(TAG, "mRootView == null ");
            return;
        }
        mRelative = (ViewGroup) mRootView.findViewById(R.id.select);
        mSelectAllBox = (CheckBox) mRootView.findViewById(R.id.select_all);
        mSelectText = (TextView) mRootView.findViewById(R.id.select_text);
        mListContainer = mRootView.findViewById(R.id.list_container);
        mSelectAllBox.setVisibility(View.VISIBLE);

        if (mListContainer != null) {
            View emptyView = mListContainer
                    .findViewById(com.android.internal.R.id.empty);
            ListView lv = (ListView) mListContainer
                    .findViewById(android.R.id.list);
            if (emptyView != null) {
                lv.setEmptyView(emptyView);
            }
            lv.setSaveEnabled(true);
            lv.setItemsCanFocus(true);
            lv.setTextFilterEnabled(true);
            mListView = lv;
            mListView.setAdapter(mApplications);
            mListView.setRecyclerListener(mApplications);
            mListView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View view,
                        int position, long arg3) {
                    App3rdViewHolder holder = (App3rdViewHolder) view.getTag();
                    holder.checkBox.setChecked(!holder.checkBox.isChecked());
                    mApplications.setChecked(position,
                            holder.checkBox.isChecked());
                    updateViews();
                }
            });
        }
    }

    private void updateViews() {
        if (mApplications.mEntries != null
                && mApplications.mEntries.size() == 0) {
            mRelative.setVisibility(View.GONE);
        } else {
            mRelative.setVisibility(View.VISIBLE);
        }

        int selectNum = mApplications.getCheckedCount();
        int allNum = mApplications.getCount();
        if (DEBUG)
            Log.d(TAG, "selectNum =" + selectNum + " allNum = " + allNum);
        if (selectNum == allNum && allNum != 0) {
            mSelectAllBox.setChecked(true);
            mSelectText.setText(R.string.cancle_select_all);
        } else {
            mSelectAllBox.setChecked(false);
            mSelectText.setText(R.string.select_all);
        }

        mSelectAllBox.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                int allNum = mApplications.getCount();
                for (int i = 0; i < allNum; i++) {
                    mApplications.setChecked(i, mSelectAllBox.isChecked());
                }
                if (mSelectAllBox.isChecked()) {
                    mSelectText.setText(R.string.cancle_select_all);
                } else {
                    mSelectText.setText(R.string.select_all);
                }
                mApplications.notifyDataSetChanged();
            }
        });
        mApplications.notifyDataSetChanged();
    }

    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
            new App3rdAutoRunSearchIndexProvider();

    private static class App3rdAutoRunSearchIndexProvider extends BaseSearchIndexProvider {
        /* SPRD: Modified for bug 597592, some application should not be searched under guest mode @{ */
        private boolean mIsOwner;

        public App3rdAutoRunSearchIndexProvider() {
            mIsOwner = UserHandle.myUserId() == UserHandle.USER_OWNER;
        }
        /* @} */

        @Override
        public List<SearchIndexableRaw> getRawDataToIndex(Context context, boolean enabled) {
            final List<SearchIndexableRaw> result = new ArrayList<SearchIndexableRaw>();
            /* SPRD: Modified for bug 597592, some application should not be searched under guest mode @{ */
            if (!mIsOwner) {
                return result;
            }
            /* @} */
            SearchIndexableRaw data = new SearchIndexableRaw(context);
            final Resources res = context.getResources();
            final String screenTitle = res.getString(R.string.auto_run_applications);
            data.title = screenTitle;
            result.add(data);
            return result;
        }

    }

}
