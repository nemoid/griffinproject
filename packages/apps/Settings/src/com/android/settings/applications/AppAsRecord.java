/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.applications;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.content.Intent;
import android.app.Activity;
import android.app.ActivityManagerNativeEx;
import android.app.AppAs;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.os.RemoteException;

import android.app.ActivityManager;
import com.android.settings.R;
import android.widget.ListView;
import android.content.pm.ApplicationInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

public class AppAsRecord extends Activity {
    private static final String TAG = "AppAsRecord";

    private ListView listview = null;
    private PackageManager pm;
    private List<AppAs.AppAsRecord> mAppInfo = null;
    private AppAsRecordAdapter mAppAdapter = null;
    private View emptyView;

    class AppAsRecordAdapter extends BaseAdapter {
        private List<AppAs.AppAsRecord> mlistAppInfo = null;
        private Context mContext = null;
        LayoutInflater infater = null;
        PackageManager mPm;

        public AppAsRecordAdapter(final Context context, final List<AppAs.AppAsRecord> apps) {
            mContext = context;
            infater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mlistAppInfo = apps ;
            mPm = mContext.getPackageManager();
        }
        @Override
        public int getCount() {
            return mlistAppInfo.size();
        }
        @Override
        public Object getItem(int position) {
            return mlistAppInfo.get(position);
        }
        @Override
        public long getItemId(int position) {
            return 0;
        }
        @Override
        public View getView(int position, View convertview, ViewGroup arg2) {
            View view = null;
            ViewHolder holder = null;
            if (convertview == null || convertview.getTag() == null) {
                view = infater.inflate(R.layout.browse_app_item, null);
                holder = new ViewHolder(view);
                view.setTag(holder);
            } else {
                view = convertview ;
                holder = (ViewHolder) convertview.getTag() ;
            }

            String base = ((AppAs.AppAsRecord)getItem(position)).getBasePackage().toString();
            String calling = ((AppAs.AppAsRecord)getItem(position)).getCallingPackage().toString();
            int launchCount = ((AppAs.AppAsRecord)getItem(position)).getCount();
            long launchTime = ((AppAs.AppAsRecord)getItem(position)).getLastTickTime();
            boolean flag = ((AppAs.AppAsRecord)getItem(position)).getEnabled();
            SimpleDateFormat formatter = new SimpleDateFormat ("HH:mm:ss ");
            Date curDate = new Date(launchTime);
            String str = formatter.format(curDate);
            ApplicationInfo mAppInfo = null;
            ApplicationInfo mAppInfo1 = null;
            try{
                mAppInfo = mPm.getApplicationInfo(base, 0);
                mAppInfo1 = mPm.getApplicationInfo(calling, 0);
                holder.appIcon.setImageDrawable(mAppInfo.loadIcon(mPm));
                if (flag) {
                    holder.tvAppLabel.setText(mContext.getResources().getString(R.string.app_as_launch_enable, mAppInfo.loadLabel(mPm), mAppInfo1.loadLabel(mPm)));
                } else {
                    holder.tvAppLabel.setText(mContext.getResources().getString(R.string.app_as_launch_disable, mAppInfo.loadLabel(mPm), mAppInfo1.loadLabel(mPm)));
                }
            } catch(PackageManager.NameNotFoundException e){
                holder.appIcon.setImageDrawable(null);
                holder.tvAppLabel.setText(null);
            }

            /* SPRD:modify for Bug 649544 strings of launch time truncated in associated start with largest font. @{ */
            String count = mContext.getResources().getString(
                    R.string.app_as_launch_count,
                    String.valueOf(launchCount), str);
            holder.tvAppCount.setText(count);
            /* @} */
            holder.switcher.setVisibility(View.GONE);

            return view;
        }

        public void setAppAsEnable(int flag, String packageName){
            try{
                ActivityManagerNativeEx.getDefault().setAppAsEnable(flag, packageName, packageName);
            } catch(RemoteException e) {
                Log.e(TAG,"Oops, setAppAsEnable Get RemoteException");
            }
        }

        class ViewHolder {
            ImageView appIcon;
            TextView tvAppLabel;
            TextView tvAppCount;
            // SPRD:modify for Bug 649544 strings of launch time truncated in associated start with largest font.
            //TextView tvAppTime;
            CheckBox switcher;

            public ViewHolder(View view) {
                this.appIcon = (ImageView) view.findViewById(R.id.imgApp);
                this.tvAppLabel = (TextView) view.findViewById(R.id.tvAppLabel);
                this.tvAppCount = (TextView) view.findViewById(R.id.tvAppCount);
                // SPRD:modify for Bug 649544 strings of launch time truncated in associated start with largest font.
                //this.tvAppTime = (TextView) view.findViewById(R.id.tvAppTime);
                this.switcher = (CheckBox) view.findViewById(R.id.switcher);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_as_launch);
        listview = (ListView) findViewById(R.id.mList);
        emptyView = findViewById(com.android.internal.R.id.empty);
        TextView warning = (TextView) findViewById(R.id.warning);
        warning.setVisibility(View.GONE);
    }

    public void onResume() {
        //load data
        try{
            mAppInfo = ActivityManagerNativeEx.getDefault().getAppAsRecordList();
        } catch(RemoteException e) {
            Log.e(TAG,"Oops, Get RemoteException");
            mAppInfo = null;
        }
        pm = this.getPackageManager();
        if(mAppInfo == null || mAppInfo.size() <= 0){
            Log.e(TAG,"mAppInfo == null or size <0");
            listview.setAdapter(null);
            listview.setEmptyView(emptyView);
        } else {
            mAppAdapter = new AppAsRecordAdapter(getBaseContext(), mAppInfo);
            listview.setAdapter(mAppAdapter);
            emptyView.setVisibility(View.GONE);
        }
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        mAppAdapter = null;
    }
}