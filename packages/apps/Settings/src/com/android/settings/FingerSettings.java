/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.UserHandle;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.support.v7.preference.Preference;
import android.provider.Settings;
import android.util.Log;
import android.content.Context;
import android.content.res.Resources;
import com.android.settings.SettingsPreferenceFragment;
import com.android.internal.logging.MetricsProto.MetricsEvent;

import com.android.settings.R;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.search.Indexable.SearchIndexProvider;
import com.android.settings.search.SearchIndexableRaw;

import java.util.ArrayList;
import java.util.List;
/**
 * Settings activity for Finger. Currently implements the following setting:
 */
public class FingerSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener,Indexable {
		private static final String TAG = "FingerSettings";
		private static final String ALLOW_MUSIC_PREFERENCE_KEY = "pref_allowMusic";
		private static final String ALLOW_CAMERA_PREFERENCE_KEY = "pref_allowCamera";
		private static final String ALLOW_RECORDING_PREFERENCE_KEY = "pref_allowRecording";
		private static final String ALLOW_GALLERY_PREFERENCE_KEY = "pref_allowGallery";
		private static final String ALLOW_CALL_PREFERENCE_KEY = "pref_allowCall";
		private static final String ALLOW_ALARM_PREFERENCE_KEY = "pref_allowAlarm";
		
    private SwitchPreference mAllowMusicPreference;
    private SwitchPreference mAllowCameraPreference;
    private SwitchPreference mAllowRecordingPreference;
    private SwitchPreference mAllowGalleryPreference;
    private SwitchPreference mAllowCallPreference;
    private SwitchPreference mAllowAlarmPreference;
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /***
        ActionBar actionBar =  getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP, ActionBar.DISPLAY_HOME_AS_UP);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        ***/        
        addPreferencesFromResource(R.xml.fingerprint_preferences);
        mAllowMusicPreference = (SwitchPreference) findPreference(ALLOW_MUSIC_PREFERENCE_KEY);
        mAllowMusicPreference.setOnPreferenceChangeListener(this);
        
        mAllowCameraPreference = (SwitchPreference) findPreference(ALLOW_CAMERA_PREFERENCE_KEY);
        mAllowCameraPreference.setOnPreferenceChangeListener(this);
        
        mAllowRecordingPreference = (SwitchPreference) findPreference(ALLOW_RECORDING_PREFERENCE_KEY);
        mAllowRecordingPreference.setOnPreferenceChangeListener(this);
        
        mAllowGalleryPreference = (SwitchPreference) findPreference(ALLOW_GALLERY_PREFERENCE_KEY);
        mAllowGalleryPreference.setOnPreferenceChangeListener(this);
        
        mAllowCallPreference = (SwitchPreference) findPreference(ALLOW_CALL_PREFERENCE_KEY);
        mAllowCallPreference.setOnPreferenceChangeListener(this);
        
        mAllowAlarmPreference = (SwitchPreference) findPreference(ALLOW_ALARM_PREFERENCE_KEY);
        mAllowAlarmPreference.setOnPreferenceChangeListener(this);

    }
 
	 	@Override
    public void onResume() {
        super.onResume();
        updateState();
    }

    private void updateState() {
        if (mAllowMusicPreference != null) {
            int allowMusic = Settings.Global.getInt(getActivity().getContentResolver(),"allow_music", 0);
            mAllowMusicPreference.setChecked(allowMusic == 1);
        }
        if (mAllowCameraPreference != null) {
            int allowCamera = Settings.Global.getInt(getActivity().getContentResolver(),"allow_camera", 0);
            mAllowCameraPreference.setChecked(allowCamera == 1);
        }
        if (mAllowRecordingPreference != null) {
            int allowRecording = Settings.Global.getInt(getActivity().getContentResolver(),"allow_recording", 0);
            mAllowRecordingPreference.setChecked(allowRecording == 1);
        }		        
        if (mAllowGalleryPreference != null) {
            int allowGallery = Settings.Global.getInt(getActivity().getContentResolver(),"allow_gallery", 0);
            mAllowGalleryPreference.setChecked(allowGallery == 1);
        }		        
        if (mAllowCallPreference != null) {
            int allowCall = Settings.Global.getInt(getActivity().getContentResolver(),"allow_call", 0);
            mAllowCallPreference.setChecked(allowCall == 1);
        }
        if (mAllowAlarmPreference != null) {
            int allowAlarm = Settings.Global.getInt(getActivity().getContentResolver(),"allow_alarm", 0);
            mAllowAlarmPreference.setChecked(allowAlarm == 1);
        }
    }
    
    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
      	if (preference == mAllowMusicPreference) {
            boolean auto = (Boolean) newValue;
            Log.d(TAG,"==="+(auto ? 1: 0));
            Settings.Global.putInt(getActivity().getContentResolver(),"allow_music",auto ? 1: 0);
        }		        
        if (preference == mAllowCameraPreference) {
            boolean auto = (Boolean) newValue;
            Settings.Global.putInt(getActivity().getContentResolver(),"allow_camera",auto ? 1: 0);
        }
        if (preference == mAllowRecordingPreference) {
            boolean auto = (Boolean) newValue;
            Settings.Global.putInt(getActivity().getContentResolver(),"allow_recording",auto ? 1: 0);
        }		        
        if (preference == mAllowGalleryPreference) {
            boolean auto = (Boolean) newValue;
            Settings.Global.putInt(getActivity().getContentResolver(),"allow_gallery",auto ? 1: 0);
        }
        
        if (preference == mAllowCallPreference) {
            boolean auto = (Boolean) newValue;
            Settings.Global.putInt(getActivity().getContentResolver(),"allow_call",auto ? 1: 0);
        }
        if (preference == mAllowAlarmPreference) {
            boolean auto = (Boolean) newValue;
            Settings.Global.putInt(getActivity().getContentResolver(),"allow_alarm",auto ? 1: 0);
        }
        return true;
    }
    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.USER;
    }
    
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
            new FingerSettingsSearchIndexProvider();

    private static class FingerSettingsSearchIndexProvider extends BaseSearchIndexProvider {

        /* SPRD: Modified for bug 597592, some application should not be searched under guest mode @{ */
        private boolean mIsOwner;

        public FingerSettingsSearchIndexProvider() {
            mIsOwner = UserHandle.myUserId() == UserHandle.USER_OWNER;
        }
        /* @} */
        @Override
        public List<SearchIndexableRaw> getRawDataToIndex(Context context, boolean enabled) {
            final List<SearchIndexableRaw> result = new ArrayList<SearchIndexableRaw>();
            /* SPRD: Modified for bug 597592, some application should not be searched under guest mode @{ */
            if (!mIsOwner) {
                return result;
            }
            /* @} */
            SearchIndexableRaw data = new SearchIndexableRaw(context);
            final Resources res = context.getResources();
            final String screenTitle = res.getString(R.string.fingerprint);
            data.title = screenTitle;
            data.screenTitle = screenTitle;
            data.intentAction = "android.intent.action.MAIN";
            data.intentTargetPackage = "com.android.settings";
            data.intentTargetClass = "com.android.settings.FingerSettings";
            result.add(data);
            return result;
        }
    }
}
