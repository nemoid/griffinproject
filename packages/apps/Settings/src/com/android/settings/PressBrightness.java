package com.android.settings;

import android.app.AddonManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.UserHandle;
import android.support.v7.preference.ListPreference;
import android.provider.Settings;

import com.android.settings.DisplaySettings;

public class PressBrightness {
    static PressBrightness mInstance;
    public static ListPreference mTouchLightTimeoutPreference;
    public static Context context;
    public static final String ADDON_STRING = "plugin.sprd.pressbrightness.AddonPressBrightness";

    public static PressBrightness getInstance() {
        if (mInstance == null) {
            mInstance = (PressBrightness) AddonManager.getDefault().getAddon(
                    ADDON_STRING, PressBrightness.class);
        }
        return mInstance;
    }

    public boolean isSupport() {
        return false;
    }

    public void init(Context mContext, ListPreference mTouchLightPreference) {
        context = mContext;
        mTouchLightTimeoutPreference = mTouchLightPreference;
    }

    public void updateTouchLightPreferenceSummary(int value) {
    }

    public void writeTouchLightPreference(Object objValue) {
    }

    public void updateTouchLightStatus() {
    }

    public void setEnabled(boolean checked, ListPreference listPreference) {
    }
}