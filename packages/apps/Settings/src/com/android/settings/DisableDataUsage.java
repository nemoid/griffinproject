package com.android.settings;

import android.app.AddonManager;

public class DisableDataUsage {

    static DisableDataUsage mInstance;
    private static final String addonName = "plugin.sprd.disabledatausage.AddonDisableDataUsage";

    public static DisableDataUsage getInstance(){
        if(mInstance != null) return mInstance;

        mInstance = (DisableDataUsage) AddonManager.getDefault().getAddon(addonName,
                DisableDataUsage.class);
        return mInstance;
    }

    public boolean isCmcc(){
        return false;
    }
}
