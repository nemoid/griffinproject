package com.android.settings.fuelgauge;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AddonManager;
import android.content.ComponentName;
import android.util.Log;
import com.android.settings.R;

public class SprdPluginProtectedAppAddonStub {
    private static final String TAG = "SprdPluginProtectedAppAddonStub";

    private static SprdPluginProtectedAppAddonStub sInstance;
    protected boolean mHasCustomizeData = false;

    public static SprdPluginProtectedAppAddonStub getInstance() {
        if (sInstance == null) {
            sInstance = (SprdPluginProtectedAppAddonStub) AddonManager.getDefault().getAddon(
                    R.string.feature_protectedapp_addon, SprdPluginProtectedAppAddonStub.class);
            if (sInstance == null) {
                sInstance = new SprdPluginProtectedAppAddonStub();
            }
        }
        return sInstance;
    }

    protected boolean isDefault() {
        Log.d(TAG, "[isDefault]::");
        return mHasCustomizeData;
    }
}
