/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.android.settings.datausage;

import java.lang.ref.WeakReference;
import java.util.zip.Inflater;
//import com.android.settings.datausage.DataUsageSummary.DataUsageViewChangedReceive;
import android.content.Context;
import android.content.res.Resources;
import android.net.NetworkTemplate;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.telephony.SubscriptionManager;
import android.text.format.Formatter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.android.settings.applications.LinearColorBar;
import com.android.settings.R;

import com.sprd.settings.networkrate.DataNetworkRate;
/**
 * Provides a summary of a setting page in a preference.  Such as memory or data usage.
 */
public class NetworkRatePreference extends Preference implements TemplatePreference/*,DataUsageViewChangedReceive*/ {

    private static final String TAB_3G = "3g";
    private static final String TAB_4G = "4g";
    public static final String TAB_MOBILE = "mobile";
    public static final String TAB_WIFI = "wifi";
    private static final int MOBILE = 0;
    private static final int WIFI = 1;
    private static final String TAB_ETHERNET = "ethernet";
    private static final String NOLINK = "0Kbps";
    private static final int UNITCHANGE = 900;

    private Context mContext;
    private Resources mResources;
    private TextView mUpLinkRate = null;
    private TextView mDownLinkRate = null;
    private TextView mTotalRate = null;
    private String mNetWorkType = null;
    private int mCurrentSubId = -1;
    private RateHandler mRateHandler = null;

    private static final long UPDATE_UI_DELAY = 1000;
    private static final int MSG_UPDATE_RATE_HIDE = 0;
    private static final int MSG_UPDATE_RATE_SHOW = 1;
    private static final int MSG_UPDATE_RATE_INIT = 2;

    class RateData {
        public String upLinkTheoryPeak;
        public String downLinkTheoryPeak;

        public long upLinkRate;
        public long downLinkRate;

        public long totalSend;
        public long totalReceive;
        public int type = -1;
    }

    private static final String TAG = "NetworkRatePreference";
    public boolean isShown = false;

    public NetworkRatePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayoutResource(R.layout.network_realtime_rate);
        this.mContext = context;
        this.mResources = this.mContext.getResources();

        View mView =  LayoutInflater.from(context).inflate(getLayoutResource(), null);

        // uplink_spread
        mUpLinkRate = (TextView) mView.findViewById(R.id.uplink_spread);
        // downlink_spread
        mDownLinkRate = (TextView) mView.findViewById(R.id.downlink_spread);
        // total
        mTotalRate = (TextView) mView.findViewById(R.id.total_traffic);
    }

    private Handler mUpdateUIHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_RATE_SHOW: {
                    RateData rateData = (msg.obj != null && msg.obj instanceof RateData ? (RateData) msg.obj
                            : null);
                    if (rateData != null) {
                        Log.i(TAG,"mCurrentSubId:"+mCurrentSubId
                                +"\t mNetWorkType:"+mNetWorkType +"\t type:"+rateData.type);
                        if ((SubscriptionManager.getDefaultDataSubscriptionId() == mCurrentSubId && rateData.type == MOBILE)
                                || (("wifi".equalsIgnoreCase(mNetWorkType)) && rateData.type == WIFI)) {
                            showNetworkRateViews(true);
                        }
                        // update UI
                        updateNetworkRate(rateData.upLinkTheoryPeak, rateData.downLinkTheoryPeak,
                                Math.max(0, rateData.upLinkRate),
                                Math.max(0, rateData.downLinkRate));
                        updateNetworkTotal(rateData.totalSend, rateData.totalReceive);
                    } else {
                        showNetworkRateViews(false);
                    }
                }
                break;
                case MSG_UPDATE_RATE_HIDE: {
                    showNetworkRateViews(false);
                }
                break;
                case MSG_UPDATE_RATE_INIT: {
                    DataNetworkRate.NetworkTrafficCurrentRate networkTrafficCurrentRate = (msg.obj != null
                            && msg.obj instanceof DataNetworkRate.NetworkTrafficCurrentRate ? (DataNetworkRate.NetworkTrafficCurrentRate) msg.obj
                            : null);
                    if (networkTrafficCurrentRate != null) {
                        updateNetworkTotal(networkTrafficCurrentRate.upLinkRate,
                                networkTrafficCurrentRate.downLinkRate);
                    }
                }
                break;
            }
        };
    };

    class RateHandler extends Handler {

        // create rate information
        public final static int MSG_NETWORK_NETWORK_RATE_INIT = 0;
        // update rate information
        public final static int MSG_NETWORK_NETWORK_RATE_UPDATE = 1;

        private final WeakReference<Context> mContext;

        public RateHandler(Context context, Looper looper) {
            super(looper);
            mContext = new WeakReference<Context>(context);
            mDataNetworkRate = new DataNetworkRate(context);
        }

        private boolean mIsMobileDataUsage = false;

        private DataNetworkRate mDataNetworkRate = null;

        private DataNetworkRate.NetworkTrafficTheoryPeak mNetworkTrafficTheoryPeak = null;
        private DataNetworkRate.NetworkTrafficCurrentRate mNetworkTrafficCurrentRate = null;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case MSG_NETWORK_NETWORK_RATE_INIT: {
                    mIsMobileDataUsage = isMobileDataUsageView();
                    mNetworkTrafficTheoryPeak = mDataNetworkRate
                            .getCurrentNetworkTrafficTheoryPeak(mContext != null ? mContext.get()
                                    : null, mIsMobileDataUsage);
                    mNetworkTrafficCurrentRate = mDataNetworkRate.getCurrentNetworkTrafficRate(
                            mContext != null ? mContext.get() : null, mIsMobileDataUsage);
                    mUpdateUIHandler.sendMessage(Message.obtain(mUpdateUIHandler,
                            MSG_UPDATE_RATE_INIT,
                            mDataNetworkRate.getCurrentNetworkTrafficTotal(mIsMobileDataUsage)));

                    Log.i(TAG,"MSG_NETWORK_NETWORK_RATE_INIT trafficCurrentRate:"+mNetworkTrafficCurrentRate);
                    networkRateUpdate(false);
                }
                break;
                case MSG_NETWORK_NETWORK_RATE_UPDATE: {
                    DataNetworkRate.NetworkTrafficCurrentRate netWorkTrafficStatus = mDataNetworkRate
                            .getCurrentNetworkTrafficRate(mContext != null ? mContext.get() : null,
                                    mIsMobileDataUsage);
                    // read current network
                    Log.i(TAG,"netWorkTrafficStatus     :"+netWorkTrafficStatus);
                    Log.i(TAG,"networkTrafficCurrentRate:"+netWorkTrafficStatus);
                    Log.i(TAG,"networkTrafficTheoryPeak :"+netWorkTrafficStatus);
                    int type = mDataNetworkRate.networkConnectedType(
                            mContext != null ? mContext.get() : null, mIsMobileDataUsage);
                    if (mNetworkTrafficCurrentRate != null && netWorkTrafficStatus != null
                            && mNetworkTrafficTheoryPeak != null) {
                        RateData rateData = new RateData();
                        rateData.upLinkTheoryPeak = mNetworkTrafficTheoryPeak.upLinkTheoryPeak;
                        rateData.downLinkTheoryPeak = mNetworkTrafficTheoryPeak.downLinkTheoryPeak;
                        rateData.upLinkRate = netWorkTrafficStatus.upLinkRate
                                - mNetworkTrafficCurrentRate.upLinkRate;
                        rateData.downLinkRate = netWorkTrafficStatus.downLinkRate
                                - mNetworkTrafficCurrentRate.downLinkRate;
                        rateData.totalSend = netWorkTrafficStatus.upLinkRate;
                        rateData.totalReceive = netWorkTrafficStatus.downLinkRate;
                        Log.i(TAG," upLinkTheoryPeak:"+rateData.upLinkTheoryPeak
                                +"\t downLinkTheoryPeak:"+rateData.downLinkTheoryPeak
                                +"\t upLinkRate:"+rateData.upLinkRate
                                +"\t downLinkRate:"+rateData.downLinkRate);
                        rateData.type = type;
                        // update ui
                        mUpdateUIHandler.sendMessage(Message.obtain(mUpdateUIHandler,
                                MSG_UPDATE_RATE_SHOW, rateData));
                    } else {
                        mUpdateUIHandler.sendEmptyMessage(MSG_UPDATE_RATE_HIDE);
                    }
                    mNetworkTrafficCurrentRate = netWorkTrafficStatus;
                    // update current network
                    networkRateUpdate(true);
                }
                break;
            }
            super.handleMessage(msg);
        }
    }

    // hide or show views
    private void showNetworkRateViews(boolean bShow) {
        isShown = bShow;
        if (mUpLinkRate == null)
            return;
        Log.i(TAG,"isShown:"+isShown);
        /* SPRD:Modify Bug 602424 @{ */
//        if (bShow) {
//            if (!mUpLinkRate.isShown()) {
//                mUpLinkRate.setVisibility(View.VISIBLE);
//                mDownLinkRate.setVisibility(View.VISIBLE);
//                mTotalRate.setVisibility(View.VISIBLE);
//            }
//        } else {
//            Log.i(TAG,"mUpLinkRate:"+mUpLinkRate.isShown());
//            if (mUpLinkRate.isShown()) {
//                mUpLinkRate.setVisibility(View.GONE);
//                mDownLinkRate.setVisibility(View.GONE);
//                mTotalRate.setVisibility(View.GONE);
//            }
//        }
        /* @} */
        notifyChanged();
    }

    private String formatFileSize(Context context, long number, boolean shorter) {
        if (context == null) {
            return "";
        }

        float result = number;
        int suffix = R.string.byteShort;
        if (result > UNITCHANGE) {
            suffix = R.string.kilobyteShort;
            result = result / 1024;
        }
        if (result > UNITCHANGE) {
            suffix = R.string.megabyteShort;
            result = result / 1024;
        }
        if (result > UNITCHANGE) {
            suffix = R.string.gigabyteShort;
            result = result / 1024;
        }
        if (result > UNITCHANGE) {
            suffix = R.string.terabyteShort;
            result = result / 1024;
        }
        if (result > UNITCHANGE) {
            suffix = R.string.petabyteShort;
            result = result / 1024;
        }
        String value;
        if (result < 1) {
            value = String.format("%.2f", result);
        } else if (result < 10) {
            if (shorter) {
                value = String.format("%.1f", result);
            } else {
                value = String.format("%.2f", result);
            }
        } else if (result < 100) {
            if (shorter) {
                value = String.format("%.0f", result);
            } else {
                value = String.format("%.2f", result);
            }
        } else {
            value = String.format("%.0f", result);
        }
        return context.getResources().getString(R.string.fileSizeSuffix,
                value, context.getString(suffix));
    }

    // update network rate
    private void updateNetworkRate(String uplinkMax, String downlinkMax, long uplinkRate,
                                   long downlinkRate) {
        String uplinkMaxStr = (uplinkMax == null ? NOLINK : uplinkMax);
        String downlinkMaxStr = (downlinkMax == null ? NOLINK : downlinkMax);
        String uplinkStr = formatFileSize(mContext, Math.max(0, uplinkRate), false);
        String downlinkStr = formatFileSize(mContext, Math.max(0, downlinkRate), false);
        mUpLinkRate.setText(String.format(mResources.getString(R.string.uplink_rate), uplinkMaxStr,
                uplinkStr));
        mDownLinkRate.setText(String.format(mResources.getString(R.string.downlink_rate),
                downlinkMaxStr, downlinkStr));
        notifyChanged();
    }

    //Send and Receive Total
    private void updateNetworkTotal(long sendTotal, long recTotal) {
        String sendStr = formatFileSize(mContext, Math.max(0, sendTotal), false);
        String recStr = formatFileSize(mContext, Math.max(0, recTotal), false);
        mTotalRate
                .setText(String.format(mResources.getString(R.string.total_rate), sendStr, recStr));
        notifyChanged();
    }

    // NetWorkRateManager
//    @Override
//    public void onDataUsageViewChanged(String networkType) {
//        mNetWorkType = networkType;
//        mUpdateUIHandler.sendEmptyMessage(MSG_UPDATE_RATE_HIDE);
//        networkRateInit();
//    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        Log.i(TAG,"----------------onBindViewHolder------------------");
        mUpLinkRate = ((TextView) holder.findViewById(R.id.uplink_spread));
        mDownLinkRate = ((TextView) holder.findViewById(R.id.downlink_spread));
        mTotalRate = ((TextView) holder.findViewById(R.id.total_traffic));
        /* SPRD:Modify Bug 602424 @{ */
        mUpLinkRate.setVisibility(View.GONE);
        mDownLinkRate.setVisibility(View.GONE);
        mTotalRate.setVisibility(View.GONE);
        /* @} */
        if (isShown) {
            Log.i(TAG,"mUpLinkRate 1:"+mUpLinkRate.isShown());
            if (!mUpLinkRate.isShown()) {
                mUpLinkRate.setVisibility(View.VISIBLE);
                mUpLinkRate.setText(mUpLinkRate.getText());
                mDownLinkRate.setVisibility(View.VISIBLE);
                mDownLinkRate.setText(mDownLinkRate.getText());
                mTotalRate.setVisibility(View.VISIBLE);
                mTotalRate.setText(mTotalRate.getText());
                holder.setDividerAllowedAbove(true);
            }
            Log.i(TAG,"mUpLinkRate:"+mUpLinkRate.isShown()+"\t "+mUpLinkRate.getText());
            Log.i(TAG,"mDownLinkRate:"+mDownLinkRate.isShown()+"\t "+mDownLinkRate.getText());
            Log.i(TAG,"mTotalRate:"+mTotalRate.isShown()+"\t "+mTotalRate.getText());
        } else {
            Log.i(TAG,"mUpLinkRate 2:"+mUpLinkRate.isShown());
            if (mUpLinkRate.isShown()) {
                mUpLinkRate.setVisibility(View.GONE);
                mDownLinkRate.setVisibility(View.GONE);
                mTotalRate.setVisibility(View.GONE);
            }
            Log.i(TAG,"mUpLinkRate:"+mUpLinkRate.isShown()+"\t "+mUpLinkRate.getText());
            Log.i(TAG,"mDownLinkRate:"+mDownLinkRate.isShown()+"\t "+mDownLinkRate.getText());
            Log.i(TAG,"mTotalRate:"+mTotalRate.isShown()+"\t "+mTotalRate.getText());
        }
    }

    private boolean isMobileDataUsageView() {
        if (mNetWorkType == null) {
            return false;
        }
        Log.d(TAG, "mNetWorkType = " + mNetWorkType);
        if (mNetWorkType.contains("mobile")) {
            return true;
        } else {
            return false;
        }
    }

    public void networkRateInit() {
        if (mRateHandler != null) {
            if (mRateHandler.hasMessages(RateHandler.MSG_NETWORK_NETWORK_RATE_INIT)) {
                mRateHandler.removeMessages(RateHandler.MSG_NETWORK_NETWORK_RATE_INIT);
            }
            mRateHandler.sendEmptyMessage(RateHandler.MSG_NETWORK_NETWORK_RATE_INIT);
        }
    }

    private void networkRateUpdate(boolean bDelay) {
        if (mRateHandler != null) {
            if (mRateHandler.hasMessages(RateHandler.MSG_NETWORK_NETWORK_RATE_UPDATE)) {
                mRateHandler.removeMessages(RateHandler.MSG_NETWORK_NETWORK_RATE_UPDATE);
            }
            if (bDelay) {
                // sleep one second , and update ui
                if (mRateHandler == null){
                    return;
                }
                mRateHandler.sendEmptyMessageDelayed(RateHandler.MSG_NETWORK_NETWORK_RATE_UPDATE,
                        UPDATE_UI_DELAY);
            } else {
                if (mRateHandler == null){
                    return;
                }
                mRateHandler.sendEmptyMessage(RateHandler.MSG_NETWORK_NETWORK_RATE_UPDATE);
            }
        }
    }

    public void resume() {
        clean();
        HandlerThread handlerThread = new HandlerThread("measure_network_rate");
        handlerThread.setPriority(Thread.MIN_PRIORITY);
        handlerThread.start();
        mRateHandler = new RateHandler(mContext, handlerThread.getLooper());
    }

    public void clean() {
        if (mRateHandler != null) {
            mRateHandler.getLooper().quit();
            mRateHandler = null;
        }
    }

    public void setSubId(int id){
        mCurrentSubId = id;
    }

    @Override
    public void setTemplate(NetworkTemplate template, int subId,
                            NetworkServices services) {
        // TODO: Summary
    }

    public void setNetworkType(String type) {
        mNetWorkType = type;
    }
}
